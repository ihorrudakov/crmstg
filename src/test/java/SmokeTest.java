import org.openqa.selenium.NoSuchElementException;
import org.testng.annotations.Test;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import static utils.Constants.*;

public class SmokeTest extends MainTest {

    @Test
    public void checkFinanceTabOnTheDealPageByAdmin() throws InterruptedException, IOException {
        mainPageSteps.loginByAdmin();
        checkFinanceTabOnTheDealPage(CLIENT_FIN_TAB, DEAL_NUMBER_FIN_TAB, OFFICE_ADMIN, CLIENT_MANAGER_ADMIN, "Admin");
    }

    public void checkFinanceTabOnTheDealPage(String clientName, String dealNumber, String office, String responsiblePerson, String filter) throws InterruptedException, IOException {
        try {
            mainPageSteps.openRequestPage();
        } catch (NoSuchElementException e) {
            mainPageSteps.loginByAdmin();
            mainPageSteps.openRequestPage();
        }

        requestPageSteps.clickCreateRequestButton();
        Thread.sleep(3000);
        requestPageSteps.enterValues(REQUEST_DATE, responsiblePerson, REQUEST_TEXT, office, clientName, REQUEST_ADMIN_COUNTRY,
                REQUEST_ADMIN_REGION, REQUEST_ADMIN_CITY, REQUEST_STATUS, REQUEST_TYPE, REQUEST_SITE, REQUEST_DETAILS, REQUEST_REPLY_TEXT, REQUEST_NOTIFICATION,
                REQUEST_COMMENTS, REQUEST_REJECTION_REASON, REQUEST_ADMIN_CONTACT_NAME, REQUEST_ADMIN_CONTACT_PHONE, REQUEST_ADMIN_CONTACT_EMAIL, CLIENT_ADMIN_CONTACT_DOB, REQUEST_ADMIN_CONTACT_NOTES, REQUEST_DIRECTION, filter);
        requestPageSteps.clickSaveButton();
        Thread.sleep(3000);
        requestPageSteps.enterDataIntoSimpleSearchField(clientName);
        Thread.sleep(3000);
        requestPageSteps.clickGeneralSearchButton();
        Thread.sleep(3000);
        requestPageSteps.checkRequest(REQUEST_DATE, office, REQUEST_STATUS, responsiblePerson, REQUEST_TYPE, REQUEST_SITE, REQUEST_DETAILS, REQUEST_ADMIN_COUNTRY, REQUEST_ADMIN_REGION, REQUEST_ADMIN_CITY,
                clientName, REQUEST_DIRECTION, REQUEST_TEXT, REQUEST_REPLY_TEXT, REQUEST_COMMENTS, REQUEST_NOTIFICATION, REQUEST_REJECTION_REASON, filter);
        Thread.sleep(2000);
        requestPageSteps.clickCreateDealButton();
        dealPageSteps.enterValues(dealNumber, DEAL_PROVIDER, "Test payer for Fin Tab", DEAL_START_DATE, DEAL_END_DATE, DEAL_RATIO, DEAL_COMMENT, DEAL_STATUS, DEAL_DOCUMENT,
                DEAL_DOC_QUANTITY, DEAL_DOC_MEANING, DEAL_DOC_NUMBER, DEAL_DOC_END_DATE, DEAL_OKPD2, DEAL_TNVED, DEAL_ASSIGNED_CASH_SUM, DEAL_ASSIGNED_NO_CASH_SUM,
                DEAL_ASSIGNED_NO_CASH_CURRENCY, DEAL_PAID_CASH_SUM, DEAL_NO_PAID_CASH_SUM, DEAL_PAID_NO_CASH_ACCOUNT_NUMBER, DEAL_ORDER_NUMBER, DEAL_COURIER,
                DEAL_AGENT_CASH_SUM, DEAL_AGENT_NO_CASH_SUM, DEAL_AGENT_COMMENT, DEAL_COST_CASH_SUM, DEAL_COST_NO_CASH_SUM, DEAL_COST_NO_CASH_SUM_CURRENCY,
                DEAL_INCOME_INVOICE_NUMBER, DEAL_OFFICE_RATIO, DEAL_TRANSFER, DEAL_TAXPERCENT, filter);
        dealPageSteps.clickSaveButton();
        Thread.sleep(15000);
        mainPageSteps.openBankTransferPage();
        Thread.sleep(3000);
        bankTransferPageSteps.clickCreateBankTransferByDealButton();
        Thread.sleep(3000);
        bankTransferPageSteps.enterDataIntoCreateBankTransferLinkByDeal(office, BANK_TRANSFER_INCOME_FOR_DEAL_CLIENT, BANK_TRANSFER_OPERATION_TYPE_INCOME, BANK_TRANSFER_OPERATION_CODE_FOR_DEAL, dealNumber,
                BANK_TRANSFER_DEAL_COST, BANK_TRANSFER_DEAL_PAID, BANK_TRANSFER_DEAL_SERVICE, BANK_TRANSFER_DEAL_CONTENT, BANK_TRANSFER_DEAL_QUANTITY, BANK_TRANSFER_DEAL_PRICE,
                BANK_TRANSFER_DEAL_VAR_PERCENT, BANK_TRANSFER_DEAL_VAR, BANK_TRANSFER_DEAL_SUMM, "Test payer for Fin Tab", BANK_TRANSFER_PROVIDER_ID, BANK_TRANSFER_ORGANIZATION_ID, BANK_TRANSFER_BILL_NUMBER, BANK_TRANSFER_BILL_DATE,
                BANK_TRANSFER_BILL_STATUS, BANK_TRANSFER_BILL_DESCRIPTION, BANK_TRANSFER_STATUS, filter);
        Thread.sleep(3000);
        bankTransferPageSteps.clickExpandPaymentLink();
        Thread.sleep(3000);
        bankTransferPageSteps.enterDataIntoPaymentBlock(BANK_TRANSFER_CHARGE_NUMBER, BANK_TRANSFER_CHARGE_DATE_PAYMENT, BANK_TRANSFER_CHARGE_DATE, BANK_TRANSFER_CHARGE_SUM, BANK_TRANSFER_CHARGE_COMMISSION);
        Thread.sleep(3000);
        bankTransferPageSteps.clickSaveButton();
        Thread.sleep(4000);
        bankTransferPageSteps.enterGeneralSearchQuery(dealNumber);
        Thread.sleep(2000);
        bankTransferPageSteps.clickGeneralSearchButton();
        Thread.sleep(2000);
        bankTransferPageSteps.clickEditButton();
        Thread.sleep(2000);
        bankTransferPageSteps.changeBillNumber(BANK_TRANSFER_BILL_NUMBER);
        Thread.sleep(2000);
        bankTransferPageSteps.clickSaveUpdateButton();

        mainPageSteps.openCardTransferPage();
        Thread.sleep(3000);
        cardTransferPageSteps.clickCreateCardTransferLinkByDealButton();
        Thread.sleep(3000);
        cardTransferPageSteps.enterDataIntoCreateCardTransferLinkByDeal(office, CARD_TRANSFER_INCOME_FOR_DEAL, CARD_TRANSFER_OPERATION_TYPE, CARD_TRANSFER_OPERATION_CODE_FOR_DEAL, dealNumber,
                CARD_TRANSFER_DEAL_COST, CARD_TRANSFER_DEAL_PAID, clientName, CARD_TRANSFER_PROVIDER, CARD_TRANSFER_BILL_STATUS, CARD_TRANSFER_BILL_DESCRIPTION, CARD_TRANSFER_STATUS, filter);
        Thread.sleep(3000);
        cardTransferPageSteps.clickExpandPaymentLink();
        Thread.sleep(3000);
        cardTransferPageSteps.enterDataIntoPaymentBlock(CARD_TRANSFER_CHARGE_BANK, CARD_TRANSFER_CHARGE_DATE_PAYMENT, CARD_TRANSFER_CHARGE_SUM, CARD_TRANSFER_CHARGE_COMMISSION);
        Thread.sleep(3000);
        cardTransferPageSteps.clickSaveButton();
        Thread.sleep(4000);


        mainPageSteps.openCashTransferPage();
        cashTransferPageSteps.clickCreateCashTransferLinkByDealButton();
        cashTransferPageSteps.enterDataIntoCreateCashTransferLinkByDeal(office, CASH_TRANSFER_INCOME_FOR_DEAL, CASH_TRANSFER_OPERATION_TYPE, CASH_TRANSFER_OPERATION_CODE_FOR_DEAL,
                dealNumber, CASH_TRANSFER_DEAL_COST, CASH_TRANSFER_DEAL_PAID, clientName, CASH_TRANSFER_PROVIDER, CASH_TRANSFER_WORKER, CASH_TRANSFER_DESCRIPTION,
                CASH_TRANSFER_STATUS, CASH_TRANSFER_OPERATION_DATE, CASH_TRANSFER_OPERATION_SUMMARY, filter);
        Thread.sleep(3000);
        cashTransferPageSteps.clickSaveButton();
        Thread.sleep(4000);
        
        mainPageSteps.openDealPage();
        dealPageSteps.enterValuesIntoSearchField(dealNumber);
        dealPageSteps.clickGeneralSearchButton();
        Thread.sleep(3000);
        dealPageSteps.clickExpandButton();
        Thread.sleep(2000);
        dealPageSteps.checkPaidInfo("Payer", BANK_TRANSFER_BILL_NUMBER, BANK_TRANSFER_DEAL_PAID, BANK_TRANSFER_DEAL_PAID, CASH_TRANSFER_DEAL_PAID);
        Thread.sleep(2000);
        dealPageSteps.openFinancialTab();
        Thread.sleep(1000);
        dealPageSteps.checkFinTab(BANK_TRANSFER_INCOME_FOR_DEAL_CLIENT,"", BANK_TRANSFER_BILL_NUMBER, BANK_TRANSFER_BILL_STATUS, BANK_TRANSFER_DEAL_PAID,
                BANK_TRANSFER_CHARGE_NUMBER, BANK_TRANSFER_CHARGE_DATE_PAYMENT, BANK_TRANSFER_CHARGE_DATE, BANK_TRANSFER_CHARGE_SUM, BANK_TRANSFER_CHARGE_COMMISSION,
                CARD_TRANSFER_TYPE_DEAL, CARD_TRANSFER_INCOME_FOR_DEAL, CARD_TRANSFER_BILL_STATUS, CARD_TRANSFER_CHARGE_BANK, CARD_TRANSFER_CHARGE_SUM, CARD_TRANSFER_CHARGE_COMMISSION,
                CASH_TRANSFER_INCOME_FOR_DEAL, CASH_TRANSFER_OPERATION_CODE_FOR_DEAL, CASH_TRANSFER_DESCRIPTION, CASH_TRANSFER_DEAL_PAID);
        Thread.sleep(3000);

        mainPageSteps.openBankTransferPage();
        Thread.sleep(2000);
        bankTransferPageSteps.enterGeneralSearchQuery(dealNumber);
        Thread.sleep(2000);
        bankTransferPageSteps.clickGeneralSearchButton();
        Thread.sleep(2000);
        bankTransferPageSteps.clickEditButton();
        Thread.sleep(2000);
        bankTransferPageSteps.changePayerToProvider(BANK_TRANSFER_INCOME_FOR_DEAL_PROVIDER, BANK_TRANSFER_PROVIDER_ID);
        Thread.sleep(2000);
        bankTransferPageSteps.clickSaveUpdateButton();

        mainPageSteps.openDealPage();
        dealPageSteps.enterValuesIntoSearchField(dealNumber);
        dealPageSteps.clickGeneralSearchButton();
        Thread.sleep(3000);
        dealPageSteps.clickExpandButton();
        Thread.sleep(2000);
        dealPageSteps.checkPaidInfo("Provider", BANK_TRANSFER_BILL_NUMBER, BANK_TRANSFER_DEAL_PAID, BANK_TRANSFER_BILL_NUMBER, CASH_TRANSFER_DEAL_PAID);
        Thread.sleep(2000);
        dealPageSteps.openFinancialTab();
        Thread.sleep(1000);
        dealPageSteps.checkFinTab(BANK_TRANSFER_INCOME_FOR_DEAL_PROVIDER,"", BANK_TRANSFER_BILL_NUMBER, BANK_TRANSFER_BILL_STATUS, BANK_TRANSFER_DEAL_PAID,
                BANK_TRANSFER_CHARGE_NUMBER, BANK_TRANSFER_CHARGE_DATE_PAYMENT, BANK_TRANSFER_CHARGE_DATE, BANK_TRANSFER_CHARGE_SUM, BANK_TRANSFER_CHARGE_COMMISSION,
                CARD_TRANSFER_TYPE_DEAL, CARD_TRANSFER_INCOME_FOR_DEAL, CARD_TRANSFER_BILL_STATUS, CARD_TRANSFER_CHARGE_BANK, CARD_TRANSFER_CHARGE_SUM, CARD_TRANSFER_CHARGE_COMMISSION,
                CASH_TRANSFER_INCOME_FOR_DEAL, CASH_TRANSFER_OPERATION_CODE_FOR_DEAL, CASH_TRANSFER_DESCRIPTION, CASH_TRANSFER_DEAL_PAID);
        Thread.sleep(3000);


        mainPageSteps.openCashTransferPage();
        cashTransferPageSteps.enterGeneralSearchQuery(dealNumber);
        cashTransferPageSteps.clickGeneralSearchButton();
        Thread.sleep(3000);
        cashTransferPageSteps.clickDeleteButton();
        Thread.sleep(3000);
        mainPageSteps.openCardTransferPage();
        cardTransferPageSteps.openFilters();
        cardTransferPageSteps.enterDataIntoSearchFields(CARD_TRANSFER_TYPE_DEAL, CARD_TRANSFER_OFFICE, CARD_TRANSFER_DATE_FROM, CARD_TRANSFER_DATE_TO, CARD_TRANSFER_INCOME_FOR_DEAL, CARD_TRANSFER_OPERATION_TYPE,
                "", "", "", "", "", "", "", "", "");
        cardTransferPageSteps.clickSearchButton();
        Thread.sleep(3000);
        cardTransferPageSteps.clickDeleteButton();
        Thread.sleep(3000);
        mainPageSteps.openBankTransferPage();
        bankTransferPageSteps.enterGeneralSearchQuery(dealNumber);
        bankTransferPageSteps.clickGeneralSearchButton();
        Thread.sleep(3000);
        bankTransferPageSteps.clickDeleteButton();
        Thread.sleep(3000);
        mainPageSteps.openDealPage();
        dealPageSteps.enterValuesIntoSearchField(dealNumber);
        dealPageSteps.clickGeneralSearchButton();
        Thread.sleep(3000);
        dealPageSteps.clickDeleteButton();
        Thread.sleep(3000);
        mainPageSteps.openRequestPage();
        requestPageSteps.clickOpenFilters();
        requestPageSteps.enterDataIntoSimpleSearchField(clientName);
        requestPageSteps.clickSearchButton();
        Thread.sleep(3000);
        requestPageSteps.deleteCreatedRequest();
    }

    @Test
    public void checkCreateDeleteCloseDocumentBySenior() throws InterruptedException, IOException {
        mainPageSteps.loginBySenior();
        try {
            mainPageSteps.openRequestPage();
        } catch (NoSuchElementException e) {
            mainPageSteps.loginBySenior();
            mainPageSteps.openRequestPage();
        }
        requestPageSteps.clickCreateRequestButton();
        Thread.sleep(3000);
        requestPageSteps.enterValues(REQUEST_DATE, CLOSE_DOCUMENT_RESPONSIBLE_PERSON, REQUEST_TEXT, CLOSE_DOCUMENT_OFFICE, CLOSE_DOCUMENT_CLIENT_NAME, REQUEST_ADMIN_COUNTRY,
                REQUEST_ADMIN_REGION, REQUEST_ADMIN_CITY, REQUEST_STATUS, REQUEST_TYPE, REQUEST_SITE, REQUEST_DETAILS, REQUEST_REPLY_TEXT, REQUEST_NOTIFICATION,
                REQUEST_COMMENTS,REQUEST_REJECTION_REASON, REQUEST_ADMIN_CONTACT_NAME, REQUEST_ADMIN_CONTACT_PHONE, REQUEST_ADMIN_CONTACT_EMAIL, CLIENT_ADMIN_CONTACT_DOB, REQUEST_ADMIN_CONTACT_NOTES, REQUEST_DIRECTION, "Senior");
        Thread.sleep(3000);
        requestPageSteps.clickSaveButton();
        Thread.sleep(3000);
        requestPageSteps.enterDataIntoSimpleSearchField(CLOSE_DOCUMENT_CLIENT_NAME);
        Thread.sleep(3000);
        requestPageSteps.clickGeneralSearchButton();
        Thread.sleep(3000);
        requestPageSteps.clickCreateDealButton();
        Thread.sleep(3000);
        dealPageSteps.enterValues(CLOSE_DOCUMENT_DEAL_NUMBER_INCOME, DEAL_PROVIDER, "Payer for CLSD", DEAL_START_DATE, DEAL_END_DATE, DEAL_RATIO, DEAL_COMMENT, DEAL_STATUS, DEAL_DOCUMENT,
                DEAL_DOC_QUANTITY, DEAL_DOC_MEANING, DEAL_DOC_NUMBER, DEAL_DOC_END_DATE, DEAL_OKPD2, DEAL_TNVED, DEAL_ASSIGNED_CASH_SUM, DEAL_ASSIGNED_NO_CASH_SUM,
                DEAL_ASSIGNED_NO_CASH_CURRENCY, DEAL_PAID_CASH_SUM, DEAL_NO_PAID_CASH_SUM, DEAL_PAID_NO_CASH_ACCOUNT_NUMBER, DEAL_ORDER_NUMBER, DEAL_COURIER,
                DEAL_AGENT_CASH_SUM, DEAL_AGENT_NO_CASH_SUM, DEAL_AGENT_COMMENT, DEAL_COST_CASH_SUM, DEAL_COST_NO_CASH_SUM, DEAL_COST_NO_CASH_SUM_CURRENCY,
                DEAL_INCOME_INVOICE_NUMBER, DEAL_OFFICE_RATIO, DEAL_TRANSFER, DEAL_TAXPERCENT, "Senior");
        dealPageSteps.clickSaveButton();
        Thread.sleep(15000);
        mainPageSteps.openBankTransferPage();
        Thread.sleep(3000);
        bankTransferPageSteps.clickCreateBankTransferByDealButton();
        Thread.sleep(3000);
        bankTransferPageSteps.enterDataIntoCreateBankTransferLinkByDeal(CLOSE_DOCUMENT_OFFICE, BANK_TRANSFER_INCOME_FOR_DEAL_CLIENT, BANK_TRANSFER_OPERATION_TYPE_INCOME, BANK_TRANSFER_OPERATION_CODE_FOR_DEAL, CLOSE_DOCUMENT_DEAL_NUMBER_INCOME,
                BANK_TRANSFER_DEAL_COST, BANK_TRANSFER_DEAL_PAID, BANK_TRANSFER_DEAL_SERVICE, BANK_TRANSFER_DEAL_CONTENT, BANK_TRANSFER_DEAL_QUANTITY, BANK_TRANSFER_DEAL_PRICE,
                BANK_TRANSFER_DEAL_VAR_PERCENT, BANK_TRANSFER_DEAL_VAR, BANK_TRANSFER_DEAL_SUMM, "Payer for CLSD", BANK_TRANSFER_PROVIDER_ID, BANK_TRANSFER_ORGANIZATION_ID,
                BANK_TRANSFER_BILL_NUMBER_FOR_CD, BANK_TRANSFER_BILL_DATE, BANK_TRANSFER_BILL_STATUS, BANK_TRANSFER_BILL_DESCRIPTION, BANK_TRANSFER_STATUS, "Senior");
        Thread.sleep(3000);
        bankTransferPageSteps.clickExpandPaymentLink();
        Thread.sleep(3000);
        bankTransferPageSteps.enterDataIntoPaymentBlock(BANK_TRANSFER_CHARGE_NUMBER, BANK_TRANSFER_CHARGE_DATE_PAYMENT, BANK_TRANSFER_CHARGE_DATE, BANK_TRANSFER_CHARGE_SUM, BANK_TRANSFER_CHARGE_COMMISSION);
        Thread.sleep(3000);
        bankTransferPageSteps.clickSaveButton();
        Thread.sleep(3000);
        mainPageSteps.openBankTransferPage();
        bankTransferPageSteps.enterDataIntoGeneralSearchField(CLOSE_DOCUMENT_DEAL_NUMBER_INCOME);
        Thread.sleep(3000);
        bankTransferPageSteps.clickGeneralSearchButton();
        Thread.sleep(2000);
        bankTransferPageSteps.clickEditButton();
        Thread.sleep(6000);
        bankTransferPageSteps.changeBillNumber(BANK_TRANSFER_BILL_NUMBER_FOR_CD);
        bankTransferPageSteps.clickSaveUpdateButton();
        Thread.sleep(3000);
        mainPageSteps.openCloseDocumentPage();
        closeDocumentPageSteps.clickCreateCloseDocumentForClientButton();
        closeDocumentPageSteps.enterData(BANK_TRANSFER_BILL_NUMBER_FOR_CD, CLOSE_DOCUMENT_ACT_NUMBER, CLOSE_DOCUMENT_ACT_DATE, "", "", "", "",
                CLOSE_DOCUMENT_NDS_SUM, CLOSE_DOCUMENT_TOTAL_SUM, CLOSE_DOCUMENT_ACT_INVOICE_NUMBER, CLOSE_DOCUMENT_DATE_INVOICE_NUMBER, CLOSE_DOCUMENT_COMMENT, "", "Client");
        Thread.sleep(3000);
        closeDocumentPageSteps.clickSaveButton();
        //enter search query
        closeDocumentPageSteps.openSearchFilter();
        closeDocumentPageSteps.enterSearchData("Реализация", "", "", "", "",
                BANK_TRANSFER_BILL_NUMBER_FOR_CD, "", "", "");
        closeDocumentPageSteps.clickSearchButton();
        Thread.sleep(3000);
        closeDocumentPageSteps.clickEditButton();
        Thread.sleep(3000);
        closeDocumentPageSteps.checkChargeData(BANK_TRANSFER_CHARGE_NUMBER, BANK_TRANSFER_CHARGE_DATE, BANK_TRANSFER_CHARGE_SUM);
        Thread.sleep(3000);
        closeDocumentPageSteps.enterUpdatedData(BANK_TRANSFER_BILL_NUMBER_FOR_CD, CLOSE_DOCUMENT_ACT_NUMBER, CLOSE_DOCUMENT_ACT_DATE, "", "", "", "",
                CLOSE_DOCUMENT_NDS_SUM, CLOSE_DOCUMENT_TOTAL_SUM, CLOSE_DOCUMENT_ACT_INVOICE_NUMBER, CLOSE_DOCUMENT_DATE_INVOICE_NUMBER, CLOSE_DOCUMENT_COMMENT, "", "Client");
        Thread.sleep(3000);
        closeDocumentPageSteps.clickUpdateButton();
        //enter search query
        Thread.sleep(5000);
        closeDocumentPageSteps.openSearchFilter();
        Thread.sleep(5000);
        closeDocumentPageSteps.enterSearchData("Реализация", "", "", "", "",
                BANK_TRANSFER_BILL_NUMBER_FOR_CD, "", "", "");
        Thread.sleep(5000);
        closeDocumentPageSteps.clickSearchButton();
        Thread.sleep(5000);
        closeDocumentPageSteps.clickDeleteButton();
        Thread.sleep(5000);
        mainPageSteps.openBankTransferPage();
        Thread.sleep(3000);
        bankTransferPageSteps.clickOpenFiltersButton();
        bankTransferPageSteps.enterDataIntoSearchFields(BANK_TRANSFER_TYPE_DEAL_SEARCH, CLOSE_DOCUMENT_OFFICE, BANK_TRANSFER_INCOME_FOR_DEAL_CLIENT, BANK_TRANSFER_OPERATION_TYPE_INCOME,
                BANK_TRANSFER_OPERATION_CODE_FOR_DEAL, BANK_TRANSFER_DATE_FROM, BANK_TRANSFER_DATE_TO, BANK_TRANSFER_BILL_NUMBER_FOR_CD, "",
                BANK_TRANSFER_BILL_STATUS, "Да", "Да", "Да", "Да", CLOSE_DOCUMENT_DEAL_NUMBER_INCOME, "", "","", BANK_TRANSFER_BILL_DESCRIPTION,
                BANK_TRANSFER_ORGANIZATION_ID, "", "", "", "", "", "", "", "", "",
                "", BANK_TRANSFER_CHARGE_NUMBER, BANK_TRANSFER_CHARGE_DATE_PAYMENT, BANK_TRANSFER_CHARGE_DATE, "");
        Thread.sleep(3000);
        bankTransferPageSteps.clickSearchButton();
        Thread.sleep(3000);
        bankTransferPageSteps.clickDeleteButton();
        mainPageSteps.openDealPage();
        dealPageSteps.enterValuesIntoSearchField(CLOSE_DOCUMENT_DEAL_NUMBER_INCOME);
        Thread.sleep(3000);
        dealPageSteps.clickGeneralSearchButton();
        Thread.sleep(2000);
        dealPageSteps.clickDeleteButton();
        Thread.sleep(2000);
        mainPageSteps.openRequestPage();
        requestPageSteps.enterDataIntoSimpleSearchField(CLOSE_DOCUMENT_CLIENT_NAME);
        Thread.sleep(3000);
        requestPageSteps.clickGeneralSearchButton();
        Thread.sleep(2000);
        requestPageSteps.deleteCreatedRequest();
        Thread.sleep(2000);
    }

    /*@Test(alwaysRun = true, priority = 1)
    public void checkDataBase() throws SQLException, InterruptedException {
        clientsWithEmptyContactInfo();
        leadWithEmptyContactInfo();
        billByDealWithEmptyDeal();
        billByDealWithEmptyService();
        billByDealWithEmptySumm();
        closeDocWithEmptyBill();
        closeDocClientWithEmptyService();
        closeDocClientWithBankTransferWithDifferentServices();
        billByDealCardWithEmptyDeal();
        cardBillWithEmptySumm();
        billByDealCashWithEmptyDeal();
        cashBillWithEmptySumm();

    }*/

    @Test(alwaysRun = true)
    public void clientsWithEmptyContactInfo() throws SQLException {
        System.out.println("\nАктивные клиенты, в которых отсутствует контактная информация:");
        Statement st = conn.createStatement();
        ResultSet rs = st.executeQuery("SELECT client.id, client.name, client_contact.name as fio, client_contact.email, client_contact.phone FROM client join client_contact on client.id = client_contact.clientId\n" +
                "where client_contact.email = '' and client_contact.phone = '' and client.status != 0 and client_contact.status !=0");
        System.out.printf("%-10s%-25s%-25s%-10s%-10s%n", "ClientID","ClientName","FIO", "E-mail", "Phone");
        System.out.printf("---------------------------------------------------------------------------\n");
        int i = 0;
        while (rs.next()) {
            System.out.printf("%-10s%-25s%-25s%-10s%-10s%n", rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5));
            i++;
        }
        System.out.println("\n -----");
        if (i>0) {
            throw new AssertionError();
        }
    }

    @Test(alwaysRun = true)
    public void leadWithEmptyContactInfo() throws SQLException {
        System.out.println("\nАктивные заявки, в которых отсутствует контактная информация клиента:");
        Statement st = conn.createStatement();
        ResultSet rs = st.executeQuery("SELECT lead.id as leadId, client.id as clinetId, client.name, client_contact.name as fio, client_contact.email, client_contact.phone FROM lead " +
                "join client on lead.clientId = client.id " +
                "join client_contact on client.id = client_contact.clientId " +
                "where client_contact.email = '' and client_contact.phone = '' and lead.status != 0  and client_contact.status != 0");
        System.out.printf("%-8s%-10s%-25s%-25s%-10s%-10s%n", "LeadID","ClientID","ClientName","FIO", "E-mail", "Phone");
        System.out.printf("-----------------------------------------------------------------------------------\n");
        int i = 0;
        while (rs.next()) {
            System.out.printf("%-8s%-10s%-25s%-25s%-10s%-10s%n", rs.getInt(1), rs.getInt(2), rs.getString(3), rs.getString(4), rs.getString(5), rs.getString(6));
            i++;
        }
        System.out.println("\n -----");
        if (i>0) {
            throw new AssertionError();
        }
    }

    @Test(alwaysRun = true)
    public void billByDealWithEmptyDeal() throws SQLException {
        System.out.println("\nАктивные счета по делу на 'Приходы расходы Банк', в которых отсутствует дело:");
        Statement st = conn.createStatement();
        ResultSet rs = st.executeQuery("SELECT bank_transfer.id, bank_transfer.billNumber, bank_transfer_deal.id, bank_transfer_deal.dealId FROM bank_transfer_deal " +
                "join bank_transfer on bank_transfer_deal.bankTransferId = bank_transfer.id where bank_transfer_deal.status != 0 and bank_transfer_deal.dealId = 0");
        System.out.printf("%-20s%-15s%-20s%-10s%n", "BankTransferID","BillNumber","BankTransferDealID","DealID");
        System.out.printf("-------------------------------------------------------------\n");
        int i = 0;
        while (rs.next()) {
            System.out.printf("%-20s%-15s%-20s%-10s%n", rs.getInt(1), rs.getInt(2), rs.getInt(3), rs.getInt(4));
            i++;
        }
        System.out.println("\n -----");
        if (i>0) {
            throw new AssertionError();
        }
    }

    @Test(alwaysRun = true)
    public void billByDealWithEmptyService() throws SQLException {
        System.out.println("\nАктивные счета по делу на 'Приходы расходы Банк', в которых отсутствует номенклатура:");
        Statement st = conn.createStatement();
        ResultSet rs = st.executeQuery("SELECT bank_transfer.id, bank_transfer.billNumber, bank_transfer_deal.id, bank_transfer_deal.dealId, deal_service.serviceId FROM bank_transfer_deal " +
                "join bank_transfer on bank_transfer_deal.bankTransferId = bank_transfer.id " +
                "join deal_service on bank_transfer.id = deal_service.bankTransferId " +
                "where deal_service.serviceId = 0 and bank_transfer_deal.status != 0");
        System.out.printf("%-20s%-35s%-20s%-15s%-10s%n", "BankTransferID","BillNumber","BankTransferDealID","DealID", "DealServiceID");
        System.out.printf("--------------------------------------------------------------------------------------------------------\n");
        int i = 0;
        while (rs.next()) {
            System.out.printf("%-20s%-35s%-20s%-15s%-10s%n", rs.getInt(1), rs.getString(2), rs.getInt(3), rs.getInt(4), rs.getInt(5));
            i++;
        }
        System.out.println("\n -----");
        if (i>0) {
            throw new AssertionError();
        }
    }

    @Test(alwaysRun = true)
    public void billByDealWithEmptySumm() throws SQLException {
        System.out.println("\nАктивные счета по делу на 'Приходы расходы Банк', в которых отсутствует сумма:");
        Statement st = conn.createStatement();
        ResultSet rs = st.executeQuery("SELECT bank_transfer.id, bank_transfer.billNumber, bank_transfer.summary FROM bank_transfer " +
                "where bank_transfer.summary is null and bank_transfer.status != 0");
        System.out.printf("%-20s%-70s%-10s%n", "BankTransferID","BillNumber", "BillSummary");
        System.out.printf("--------------------------------------------------------------------------------------------------------\n");
        int i = 0;
        while (rs.next()) {
            System.out.printf("%-20s%-70s%-10s%n", rs.getInt(1), rs.getString(2), rs.getInt(3));
            i++;
        }
        System.out.println("\n -----");
        if (i>0) {
            throw new AssertionError();
        }
    }

    @Test(alwaysRun = true)
    public void closeDocWithEmptyBill() throws SQLException {
        System.out.println("\nАктивные закрывающие документы, в которых отсутствует счет:");
        Statement st = conn.createStatement();
        ResultSet rs = st.executeQuery("SELECT bank_closing.id, bank_closing.actNumber, bank_closing_transfer.bankTransferId FROM bank_closing " +
                "join bank_closing_transfer on bank_closing.id = bank_closing_transfer.bankClosingId " +
                "where bank_closing_transfer.bankTransferId is null " +
                "or bank_closing_transfer.bankTransferId = '' and bank_closing_transfer.status != 0");
        System.out.printf("%-25s%-25s%-25s%n", "BankClosingID","BankClosingActNumber","BankClosingBankTransfer");
        System.out.printf("-----------------------------------------------------------------------------\n");
        int i = 0;
        while (rs.next()) {
            System.out.printf("%-25s%-25s%-25s%n", rs.getInt(1), rs.getString(2), rs.getInt(3));
            i++;
        }
        System.out.println("\n -----");
        if (i>0) {
            throw new AssertionError();
        }
    }

    @Test(alwaysRun = true)
    public void closeDocClientWithEmptyService() throws SQLException {
        System.out.println("\nАктивные закрывающие документы (Реализация), в которых отсутствует номенклатура:");
        Statement st = conn.createStatement();
        ResultSet rs = st.executeQuery("SELECT bank_closing.id, bank_closing.actNumber, bank_closing_deal_service.serviceId FROM bank_closing_deal_service " +
                "join bank_closing on bank_closing_deal_service.bankClosingId = bank_closing.id " +
                "where bank_closing.typeId='client' and bank_closing_deal_service.serviceId = 0 and bank_closing.status != 0 and bank_closing_deal_service.status != 0;");
        System.out.printf("%-25s%-25s%-25s%n", "BankClosingID","BankClosingActNumber","BankClosingServiceID");
        System.out.printf("-----------------------------------------------------------------------------\n");
        int i = 0;
        while (rs.next()) {
            System.out.printf("%-25s%-25s%-25s%n", rs.getString(1), rs.getString(2), rs.getString(3));
            i++;
        }
        System.out.println("\n -----");
        if (i>0) {
            throw new AssertionError();
        }
    }

    @Test(alwaysRun = true)
    public void closeDocClientWithBankTransferWithDifferentServices() throws SQLException, InterruptedException {
        System.out.println("\nАктивные закрывающие документы оформленные на счета по делу, в счетах которых не совпадает номенклатура:");
        Statement st = conn.createStatement();
        ResultSet rs = st.executeQuery("SELECT bcds.bankClosingId, bcds.bankTransferId " +
                "FROM crm_stage.bank_closing_deal_service bcds " +
                "LEFT JOIN crm_stage.bank_closing bc ON (bcds.bankClosingId = bc.id AND bc.status <> 0) " +
                "LEFT JOIN crm_stage.bank_closing_transfer bct ON (bcds.bankClosingId = bct.bankClosingId AND bcds.bankTransferId = bct.bankTransferId AND bct.status <> 0) " +
                "WHERE " +
                "bct.id IS NULL " +
                "AND bcds.status <> 0 " +
                "AND bc.id IS NOT NULL");
        System.out.printf("%-25s%-25s%n", "BankClosingID", "BankTransferID");
        System.out.printf("------------------------------------------------------------------\n");
        int i = 0;
        Thread.sleep(3000);
        while (rs.next()) {
            System.out.printf("%-25s%-25s%n", rs.getInt(1), rs.getInt(2));
            i++;
        }
        System.out.println("\n -----");
        if (i>0) {
            throw new AssertionError();
        }
    }

    @Test(alwaysRun = true)
    public void billByDealCardWithEmptyDeal() throws SQLException {
        System.out.println("\nАктивные счета по делу на 'Приходы расходы Карта', в которых отсутствует дело:");
        Statement st = conn.createStatement();
        ResultSet rs = st.executeQuery("SELECT card_transfer.id, card_transfer.description, card_transfer_deal.dealId FROM card_transfer_deal " +
                "join card_transfer on card_transfer_deal.cardTransferId = card_transfer.id where card_transfer_deal.status != 0 and (card_transfer_deal.dealId = 0 or card_transfer_deal.dealId is null)");
        System.out.printf("%-20s%-30s%-10s%n", "CardTransferID","CardTransferDescription","DealID");
        System.out.printf("-------------------------------------------------------------\n");
        int i = 0;
        while (rs.next()) {
            System.out.printf("%-20s%-30s%-10s%n", rs.getInt(1), rs.getString(2), rs.getInt(3));
            i++;
        }
        System.out.println("\n -----");
        if (i>0) {
            throw new AssertionError();
        }
    }

    @Test(alwaysRun = true)
    public void cardBillWithEmptySumm() throws SQLException {
        System.out.println("\nАктивные счета по делу на 'Приходы расходы Карта', в которых отсутствует сумма:");
        Statement st = conn.createStatement();
        ResultSet rs = st.executeQuery("SELECT id, description, summary FROM card_transfer where summary is null and status != 0");
        System.out.printf("%-20s%-35s%-15s%n", "CardTransferID","CardDescription","CardSummary");
        System.out.printf("--------------------------------------------------------------------\n");
        int i = 0;
        while (rs.next()) {
            System.out.printf("%-20s%-35s%-15s%n", rs.getInt(1), rs.getString(2), rs.getInt(3));
            i++;
        }
        System.out.println("\n -----");
        if (i>0) {
            throw new AssertionError();
        }
    }

    @Test(alwaysRun = true)
    public void billByDealCashWithEmptyDeal() throws SQLException {
        System.out.println("\nАктивные счета по делу на 'Приходы расходы Кэш', в которых отсутствует дело:");
        Statement st = conn.createStatement();
        ResultSet rs = st.executeQuery("SELECT cash_transfer.id, cash_transfer.description, cash_transfer_deal.dealId, cash_transfer.typeId FROM cash_transfer_deal " +
                "join cash_transfer on cash_transfer_deal.cashTransferId = cash_transfer.id " +
                "where cash_transfer_deal.status != 0 and (cash_transfer_deal.dealId = 0 or cash_transfer_deal.dealId is null)");
        System.out.printf("%-20s%-30s%-30s%-10s%n", "CashTransferID", "CashTransferDescription", "DealID", "TypeID");
        System.out.printf("-------------------------------------------------------------------------------------------\n");
        int i = 0;
        while (rs.next()) {
            System.out.printf("%-20s%-30s%-30s%-10s%n", rs.getInt(1), rs.getString(2), rs.getInt(3), rs.getString(4));
            i++;
        }
        System.out.println("\n -----");
        if (i>0) {
            throw new AssertionError();
        }
    }

    @Test(alwaysRun = true)
    public void cashBillWithEmptySumm() throws SQLException {
        System.out.println("\nАктивные счета по делу на 'Приходы расходы Кэш', в которых отсутствует сумма:");
        Statement st = conn.createStatement();
        ResultSet rs = st.executeQuery("SELECT id, description, operationSum FROM cash_transfer where operationSum is null and status != 0");
        System.out.printf("%-20s%-100s%-15s%n", "CashTransferID","CashDescription","CashSummary");
        System.out.printf("--------------------------------------------------------------------\n");
        int i = 0;
        while (rs.next()) {
            System.out.printf("%-20s%-100s%-15s%n", rs.getInt(1), rs.getString(2), rs.getInt(3));
            i++;
        }
        System.out.println("\n -----");
        if (i>0) {
            throw new AssertionError();
        }
    }

    @Test(alwaysRun = true)
    public void checkReportByOfficeDeal() throws SQLException, InterruptedException, IOException {
        dataBaseSteps.getReportData(conn);
        mainPageSteps.loginByAdmin();
        mainPageSteps.openReportPage();
        reportPageSteps.openByOfficeDealTab();
        dataBaseSteps.selectOfficeAndEmployee();
        reportPageSteps.clickSearchButton();
        dataBaseSteps.checkOfficeDealReport();
    }

    @Test(alwaysRun = true)
    public void checkLeadChart() throws InterruptedException, IOException {
        mainPageSteps.loginByAdmin();
        mainPageSteps.openReportPage();
        reportPageSteps.openLeadTab();
        reportPageSteps.selectLeadFilters("По статусам", "Круговая диаграмма", "Все клиенты");
        reportPageSteps.clickBuildButton();
        reportPageSteps.checkChart();
    }

    @Test(alwaysRun = true)
    public void checkOfficeSalesReport() throws SQLException, IOException, InterruptedException {
        dataBaseSteps.getSummOfOfficeSales(conn);
        mainPageSteps.loginByAdmin();
        mainPageSteps.openReportPage();
        reportPageSteps.openOfficeReportTab();
        reportPageSteps.clickBuildButton();
        dataBaseSteps.checkSummOfOfficeSales();
    }

    @Test(alwaysRun = true)
    public void checkServicePrice() throws SQLException, IOException, InterruptedException {
        System.out.println("\nНоменклатура в группе 'Сертификация EAC Audit', в которых отсутствует цена:");
        Statement st = conn.createStatement();
        ResultSet rs = st.executeQuery("SELECT name, fullName, price FROM service where groupId = 17 and price is null and status != 0");
        System.out.printf("%-30s%-30s%-15s%n", "ServiceName","ServiceFullName","Price");
        System.out.printf("--------------------------------------------------------------------\n");
        int i = 0;
        while (rs.next()) {
            System.out.printf("%-30s%-30s%-15s%n", rs.getString(1), rs.getString(2), rs.getInt(3));
            i++;
        }
        System.out.println("\n -----");
        if (i>0) {
            throw new AssertionError();
        }
    }
}
