import com.informix.jdbcx.IfxConnectionPoolDataSource;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.*;
import steps.*;
import utils.Constants;

import java.io.PrintStream;
import java.io.UnsupportedEncodingException;
import java.sql.*;
import java.util.Arrays;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

public abstract class MainTest {

    WebDriver driver;

    String dbHost = "62.109.21.167";
    int dbPort = 3306;
    String dbuserName = "crm_readonly";
    String dbpassword = "R2zITNGrszwdI3Qu";

    //String dbuserName = "igor_readonly";
    //String dbpassword = "CL1ESSZMzH2EKoP9";

    String url = "jdbc:mysql://188.120.233.103:3306/crm_stage?verifyServerCertificate=false&useSSL=true&requireSSL=true";
    String driverName = "com.mysql.jdbc.Driver";
    Connection conn = null;

    public void createDBconnection() {
        try {
            //mysql database connectivity
            Class.forName(driverName).newInstance();
            conn = DriverManager.getConnection(url, dbuserName, dbpassword);

            System.out.println("Database connection established");
            System.out.println("DONE");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void closeDBconnection() throws SQLException {
        if (conn != null && !conn.isClosed()){
            System.out.println("Closing Database Connection");
            conn.close();
        }
    }

    BankTransferPageSteps bankTransferPageSteps;
    CardTransferPageSteps cardTransferPageSteps;
    CashTransferPageSteps cashTransferPageSteps;
    ClientPageSteps clientPageSteps;
    RequestPageSteps requestPageSteps;
    DealPageSteps dealPageSteps;
    MainPageSteps mainPageSteps;
    PayerPageSteps payerPageSteps;
    CloseDocumentPageSteps closeDocumentPageSteps;
    DataBaseSteps dataBaseSteps;
    ReportPageSteps reportPageSteps;

    @BeforeTest
    public void before() throws UnsupportedEncodingException {
        createDBconnection();
    }

    @BeforeMethod
    public void setUp() {
        //System.setProperty("webdriver.chrome.driver", "src//main//resources//chromedriver.exe");
        System.setProperty("webdriver.chrome.driver", "/usr/local/share/chromedriver");

        ChromeOptions options = new ChromeOptions();
        options.addArguments("--no-sandbox");
        options.addArguments("--headless");
        options.addArguments("--disable-dev-shm-usage");
        options.addArguments("test-type");

        String[] switches = {"--ignore-certificate-errors"};

        DesiredCapabilities capabilities = DesiredCapabilities.chrome();
        capabilities.setCapability("chrome.switches", Arrays.asList(switches));
        capabilities.setCapability (CapabilityType.ACCEPT_SSL_CERTS, true);
        capabilities.setCapability(ChromeOptions.CAPABILITY, options);

        driver = new ChromeDriver(capabilities);
        driver.manage().window().setSize(new Dimension(1980,1024));
        driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
        bankTransferPageSteps = new BankTransferPageSteps(driver);
        cardTransferPageSteps = new CardTransferPageSteps(driver);
        cashTransferPageSteps = new CashTransferPageSteps(driver);
        clientPageSteps = new ClientPageSteps(driver);
        requestPageSteps = new RequestPageSteps(driver);
        dealPageSteps = new DealPageSteps(driver);
        mainPageSteps = new MainPageSteps(driver);
        payerPageSteps = new PayerPageSteps(driver);
        closeDocumentPageSteps = new CloseDocumentPageSteps(driver);
        dataBaseSteps = new DataBaseSteps(driver);
        reportPageSteps = new ReportPageSteps(driver);
    }

    @AfterMethod
    public void tearDown() {
        driver.manage().deleteAllCookies();
        driver.close();
    }

    @AfterTest
    public void after() throws SQLException {
        closeDBconnection();
    }
}
