package utils;

/**
 * Created by Ihor_Rudakov on 6/8/2017.
 */
public class Constants {

    //AUTH DATA
    //http://site4.tmweb.ru/sso/auth/my-self?email=administrator@admin.com&role=admin&authKey=p7RQ8Pk_gxK2JFYvIQd7uoo0J7BGa3MR

    public static final String ADMINISTRATOR_EMAIL = "administrator@admin.com";
    public static final String ADMINISTRATOR_ROLE = "admin";
    public static final String ADMINISTRATOR_AUTH_KEY = "2UvjM8l07wASNWAN2RgMFVqTH14rePtK";

    //site4.tmweb.ru/sso/auth/my-self?email=supervisor@supervisor.com&role=supervisor&authKey=hYi0DmINbUBzbqbREEbObUQQJhEJlI4z
    public static final String SUPERVISOR = "Supervisor Kesha";
    public static final String SUPERVISOR_EMAIL = "supervisor@supervisor.com";
    public static final String SUPERVISOR_ROLE = "supervisor";
    public static final String SUPERVISOR_AUTH_KEY = "OAAoogkkgTJMXJlF14Y32p2Ow6ggHzmM";

    //site4.tmweb.ru/sso/auth/my-self?email=marketer@marketer.com&role=marketer&authKey=dfGpZZxyt1OKkM8nVeaPHoqwC9A3Wzs4
    public static final String MARKETER = "Marketer John";
    public static final String MARKETER_EMAIL = "marketer@marketer.com";
    public static final String MARKETER_ROLE = "marketer";
    public static final String MARKETER_AUTH_KEY = "mjka1-vZGboIzTlh1A64RSXPm-xzrHu8";

    //site4.tmweb.ru/sso/auth/my-self?email=account_fin_dep@accfindep.com&role=account_fin_dep&authKey=LyjsF0h3qF2N1yUxqpZRix6jaeREGdNa
    public static final String ACCOUNT_DEP = "Accountant Fin Dep Sergey";
    public static final String ACCOUNT_DEP_EMAIL = "account_fin_dep@accfindep.com";
    public static final String ACCOUNT_DEP_ROLE = "account_fin_dep";
    public static final String ACCOUNT_DEP_AUTH_KEY = "G5SdfIleppl_-_fnTUyqPJqtWeQRbxe4";

    //site4.tmweb.ru/sso/auth/my-self?email=account_office@accoffice.com&role=account_office&authKey=Tl5logRWjPNrQJ1XpGHIMLNSRh7sNDAf
    public static final String ACCOUNT_OFFICE = "Accountant Office Nick";
    public static final String ACCOUNT_OFFICE_EMAIL = "account_office@accoffice.com";
    public static final String ACCOUNT_OFFICE_ROLE = "account_office";
    public static final String ACCOUNT_OFFICE_AUTH_KEY = "0dFLEpTcxyFNzcP1Qr7U6iGC7vUrr4kC";

    //site4.tmweb.ru/sso/auth/my-self?email=senior@senior.com&role=senior&authKey=W_rbkzQIitK2U89nNX6Kgfmusv-FGHFP
    public static final String SENIOR_MANAGER = "Senior Andrey";
    public static final String SENIOR_MANAGER_EMAIL = "senior@senior.com";
    public static final String SENIOR_MANAGER_ROLE = "senior";
    public static final String SENIOR_MANAGER_AUTH_KEY = "EBDLzLc7OQbh4AE7cGGnY8p2MyLxtIZ5";

    //site4.tmweb.ru/sso/auth/my-self?email=manager@manager.com&role=manager&authKey=OPsoTo8XHzgRMPgtvqYvziCI8MUmOrfH
    public static final String MANAGER = "Manager Alex";
    public static final String MANAGER_EMAIL = "manager@manager.com";
    public static final String MANAGER_ROLE = "manager";
    public static final String MANAGER_AUTH_KEY = "Gu0LMyTGTIOWkkoxqckduPmg3mcTv8Ih";

    public static final String CRM_BASE_URL = "https://crm.stage.gortest.ru/sso/auth/my-self";

    public static final String PROPERTY_FILE_PATH = "C:\\Users\\Admin\\IdeaProjects\\TRTS_DEMO\\src\\main\\resources\\dataConfig.properties";


    //CREATE USER DATA

    public static final String ROLE_ADMIN = "Администратор";
    public static final String ROLE_ACCOUNT_FIN_DEP = "Бухгалтер офиса";
    public static final String ROLE_SUPERVISOR = "Старший Менеджер";
    public static final String ROLE_SENIOR = "Менеджер";
    public static final String ROLE_ACCOUNT_OFFICE = "Менеджер";
    public static final String ROLE_MANAGER = "Менеджер";
    public static final String ROLE_COWORKER = "Сотрудник";
    public static final String ROLE_MARKETER = "Маркетолог";

    //--ADMIN
    public static final String EMAIL_ADMIN = "expert@expert.expert";
    public static final String FIO_ADMIN = "Expert Expert";
    public static final String PASS_ADMIN = "Qwerty12345";

    public static final String EMAIL_ADMIN_NEW = "fringe@fringe.fringe";
    public static final String FIO_ADMIN_NEW = "Fringe Expertovich";
    public static final String PASS_ADMIN_NEW = "Qwerty54321";

    public static final String DOB_ADMIN = "30-07-1991";
    public static final String DOB_ADMIN_NEW = "31-08-1990";

    public static final String STATUS_ACTIVE = "Активный";
    public static final String STATUS_DELETED = "Удаленный";
    public static final String STATUS_MATERNITY = "В декрете";

    public static final String PHONE_ADMIN = "380974355674";
    public static final String PHONE_ADMIN_NEW = "380966151547";

    public static final String CITY_ADMIN = "Москва";
    public static final String CITY_ADMIN_NEW = "Тула";

    public static final String OFFICE_ADMIN = "Тестовый офис";
    public static final String OFFICE_NOT = "Нет";
    public static final String OFFICE_FINANCIAL = "Финансовый отдел";

    public static final String HEAD = "Администратор";
    public static final String HEAD_NULL = "NULL";

    //--SUPERVISOR
    public static final String EMAIL_SUPERVISOR = "expert_supervisor@supervisor.com";
    public static final String FIO_SUPERVISOR = "Expert Supervisor";
    public static final String PASS_SUPERVISOR = "Qwerty12345";

    public static final String EMAIL_SUPERVISOR_NEW = "fringe_supervisor@supervisor.com";
    public static final String FIO_SUPERVISOR_NEW = "Fringe Supervisor";
    public static final String PASS_SUPERVISOR_NEW = "Qwerty54321";

    public static final String DOB_SUPERVISOR = "30-05-1990";
    public static final String DOB_SUPERVISOR_NEW = "13-08-1995";

    public static final String PHONE_SUPERVISOR = "380971194555";
    public static final String PHONE_SUPERVISOR_NEW = "380961195543";

    public static final String CITY_SUPERVISOR = "Пермь";
    public static final String CITY_SUPERVISOR_NEW = "Псков";

    public static final String OFFICE_SUPERVISOR = "Тестовый офис";
    public static final String HEAD_SUPERVISOR = "NULL";

    //--SENIOR
    public static final String EMAIL_SENIOR = "expert_senior@senior.com";
    public static final String FIO_SENIOR = "Expert Senior";
    public static final String PASS_SENIOR = "Qwerty12345";

    public static final String EMAIL_SENIOR_NEW = "fringe_senior@senior.com";
    public static final String FIO_SENIOR_NEW = "Fringe Senior";
    public static final String PASS_SENIOR_NEW = "Qwerty54321";

    public static final String DOB_SENIOR = "30-05-1986";
    public static final String DOB_SENIOR_NEW = "13-08-1988";

    public static final String PHONE_SENIOR = "380971091115";
    public static final String PHONE_SENIOR_NEW = "380961053312";

    public static final String CITY_SENIOR = "Белгород";
    public static final String CITY_SENIOR_NEW = "Тверь";

    public static final String OFFICE_SENIOR = "NULL";
    public static final String HEAD_SENIOR = "NULL";

    //--ACCOUNTANT OFFICE
    public static final String EMAIL_ACCOUNT_OFFICE = "expert_account_office@account.com";
    public static final String FIO_ACCOUNT_OFFICE = "Expert Account Office";
    public static final String PASS_ACCOUNT_OFFICE = "Qwerty12345";

    public static final String EMAIL_ACCOUNT_OFFICE_NEW = "fringe_account_office@account.com";
    public static final String FIO_ACCOUNT_OFFICE_NEW = "Fringe Account Office";
    public static final String PASS_ACCOUNT_OFFICE_NEW = "Qwerty54321";

    public static final String DOB_ACCOUNT_OFFICE = "28-02-1991";
    public static final String DOB_ACCOUNT_OFFICE_NEW = "13-10-1995";

    public static final String PHONE_ACCOUNT_OFFICE = "380978091736";
    public static final String PHONE_ACCOUNT_OFFICE_NEW = "380966457643";

    public static final String CITY_ACCOUNT_OFFICE = "Уфа";
    public static final String CITY_ACCOUNT_OFFICE_NEW = "Омск";

    public static final String OFFICE_ACCOUNT_OFFICE = "NULL";
    public static final String HEAD_ACCOUNT_OFFICE = "NULL";

    //--ACCOUNTANT FINANCIAL DEPARTMENT
    public static final String EMAIL_ACCOUNT_FIN_DEP = "expert_accountant_fin_dep@account.com";
    public static final String FIO_ACCOUNT_FIN_DEP = "Expert Account Fin Dep";
    public static final String PASS_ACCOUNT_FIN_DEP = "Qwerty12345";

    public static final String EMAIL_ACCOUNT_FIN_DEP_NEW = "fringe_accountant_fin_dep@account.com";
    public static final String FIO_ACCOUNT_FIN_DEP_NEW = "Fringe Account Fin Dep";
    public static final String PASS_ACCOUNT_FIN_DEP_NEW = "Qwerty54321";

    public static final String DOB_ACCOUNT_FIN_DEP = "13-05-1996";
    public static final String DOB_ACCOUNT_FIN_DEP_NEW = "23-08-1995";

    public static final String PHONE_ACCOUNT_FIN_DEP = "38097435544";
    public static final String PHONE_ACCOUNT_FIN_DEP_NEW = "380963344667";

    public static final String CITY_ACCOUNT_FIN_DEP = "Самара";
    public static final String CITY_ACCOUNT_FIN_DEP_NEW = "Воронеж";

    public static final String OFFICE_ACCOUNT_FIN_DEP = "Тестовый офис";
    public static final String HEAD_ACCOUNT_FIN_DEP = "NULL";

    //--MANAGER
    public static final String EMAIL_MANAGER = "manager_with_coach@manager.com";
    public static final String FIO_MANAGER = "Manager with coach";
    public static final String PASS_MANAGER = "Qwerty54321";

    public static final String EMAIL_MANAGER_NEW = "new_manager_with_coach@manager.com";
    public static final String FIO_MANAGER_NEW = "New Manager with coach";
    public static final String PASS_MANAGER_NEW = "Qwerty12345";

    public static final String DOB_MANAGER = "13-05-1996";
    public static final String DOB_MANAGER_NEW = "23-08-1995";

    public static final String PHONE_MANAGER = "38097437777";
    public static final String PHONE_MANAGER_NEW = "380963347777";

    public static final String CITY_MANAGER = "Самара";
    public static final String CITY_MANAGER_NEW = "Воронеж";

    public static final String OFFICE_MANAGER = "Тестовый офис";
    public static final String HEAD_MANAGER = "NULL";


    //CREATE OFFICE DATA

    public static final String TITLE = "AutoOffice";
    public static final String OFFICE_ADDRESS = "Test address";
    public static final String OFFICE_SITE = "testsite.com";
    public static final String PREFIX = "AT";
    public static final String KOEF = "20";
    public static final String REMAINDER = "10000";

    public static final String DESCRIPTION = "Automated Test Office Description";

    public static final String OFFICE_PHONE = "380634556788";
    public static final String OFFICE_NOTE = "Automated Test Office Note";


    //__________________________________________________________\\

    public static final String TITLE_NEW = "AutoOfficeNew";
    public static final String PREFIX_NEW = "TA";
    public static final String KOEF_NEW = "30";
    public static final String REMAINDER_NEW = "20000";

    public static final String DESCRIPTION_NEW = "Automated Test Office Description (New generation)";

    public static final String OFFICE_PHONE_NEW = "380634776777";
    public static final String OFFICE_NOTE_NEW = "Automated Test Office Note NEW!!!";


    //CREATE PROVIDER DATA

    public static final String PROVIDER_TYPE = "Аренда";
    public static final String PROVIDER_DIRECTION = "Аттракционы";
    public static final String PROVIDER_TITLE = "Sebastopol Seafront";
    public static final String PROVIDER_STATUS = "Работаем";
    public static final String PROVIDER_CONDITION = "Предоплата за 6 месяцев";
    public static final String PROVIDER_CONTACT_NAME = "Alexey Sergeevich";
    public static final String PROVIDER_EMAIL = "alexey@alex.com";
    public static final String PROVIDER_PHONE = "380971105312";
    public static final String PROVIDER_DOB = "Add";
    public static final String PROVIDER_OTHER_CONTACTS = "alex.ru";
    public static final String PROVIDER_CONTACT_NOTES = "Single";

    public static final String PROVIDER_LEGAL_NAME = "Primorskiy Bulvar";
    public static final String PROVIDER_LEGAL_TYPE = "Государственный орган";
    public static final String PROVIDER_LEGAL_INN = "1234567812";
    public static final String PROVIDER_LEGAL_ORGANIZATION_ID = "ООО \"Тест СПб\"";
    public static final String PROVIDER_LEGAL_OSNUSN = "ОСНО";
    public static final String PROVIDER_LEGAL_CONTRACT_NUMBER = "12345";

    public static final String PROVIDER_CONTRACT_DATE_FROM = "23-08-2017";
    public static final String PROVIDER_CONTRACT_CURRENT_DATE = "24-08-2017";
    public static final String PROVIDER_CONTRACT_DATE_TO = "25-08-2019";
    public static final String PROVIDER_CONTRACT_PERIOD = "2 года";
    public static final String PROVIDER_CONTRACT_STORAGE = "У заказчика";
    public static final String PROVIDER_WHO_LET_CANAL = "Городской совет";
    public static final String PROVIDER_COMMENT = "Обязательная регистрация в фискальной службе";

    //_________________________________________________________\\

    public static final String PROVIDER_TYPE_NEW = "Сертификация";
    public static final String PROVIDER_DIRECTION_NEW = "Лицензия МЧС";
    public static final String PROVIDER_TITLE_NEW = "Sebastopol";
    public static final String PROVIDER_STATUS_NEW = "Постоянный";
    public static final String PROVIDER_CONDITION_NEW = "Предоплата не требуется";
    public static final String PROVIDER_CONTACT_NAME_NEW = "Pavel Sergeevich";
    public static final String PROVIDER_EMAIL_NEW = "pavel@pvl.com";
    public static final String PROVIDER_PHONE_NEW = "380970111547";
    public static final String PROVIDER_DOB_NEW = "Addon";
    public static final String PROVIDER_OTHER_CONTACTS_NEW = "pavel.ru";
    public static final String PROVIDER_CONTACT_NOTES_NEW = "Married";

    public static final String PROVIDER_LEGAL_NAME_NEW = "Park Pobedy";
    public static final String PROVIDER_LEGAL_TYPE_NEW = "Обособленное подразделение";
    public static final String PROVIDER_LEGAL_INN_NEW = "8765432112";
    public static final String PROVIDER_LEGAL_ORGANIZATION_ID_NEW = "ООО \"Интертест\"";
    public static final String PROVIDER_LEGAL_OSNUSN_NEW = "Нерезидент";
    public static final String PROVIDER_LEGAL_CONTRACT_NUMBER_NEW = "54321";

    public static final String PROVIDER_CONTRACT_DATE_FROM_NEW = "04-12-2017";
    public static final String PROVIDER_CONTRACT_CURRENT_DATE_NEW = "04-12-2017";
    public static final String PROVIDER_CONTRACT_DATE_TO_NEW = "05-12-2017";
    public static final String PROVIDER_CONTRACT_PERIOD_NEW = "5 лет";
    public static final String PROVIDER_CONTRACT_STORAGE_NEW = "Оригинал у заказчика, копия у контрагента";
    public static final String PROVIDER_WHO_LET_CANAL_NEW = "Союз моряков Украинского Севастополя";
    public static final String PROVIDER_COMMENT_NEW = "Особых разрешений не требуется";


    //CREATE CLIENTS DATA

    public static final String CLIENT_FIN_TAB = "Test client for Fin Tab";

    public static final String CLIENT_MANAGER_ADMIN = "Администратор";
    public static final String CLIENT_MANAGER_SENIOR = "Senior Andrey";
    public static final String CLIENT_MANAGER_SUPERVISOR = "Supervisor Kesha";
    public static final String CLIENT_MANAGER_ACCOUNTANT = "Accountant Office Nick";
    public static final String CLIENT_MANAGER_MANAGER = "Manager Alex";

    //--ADMIN
    public static final String CLIENT_ADMIN_NAME = "Admin Client";
    public static final String CLIENT_ADMIN_NAME_FOR_REQUEST = "Admin Client For Request";
    public static final String CLIENT_ADMIN_NAME_FOR_DEAL = "Admin Client For Deal";
    public static final String CLIENT_ADMIN_TYPE = "VIP";

    public static final String CLIENT_ADMIN_OFFICE = "Тестовый офис";

    public static final String CLIENT_ADMIN_COUNTRY = "Россия";
    public static final String CLIENT_ADMIN_REGION = "Москва и Московская обл.";
    public static final String CLIENT_ADMIN_CITY = "Москва";
    public static final String CLIENT_ADMIN_ADDRESS = "ул. Петровская, 5";
    public static final String CLIENT_ADMIN_SITE = "admin.ru";

    public static final String CLIENT_ADMIN_CONTACT_NAME = "Admin Adminovich";
    public static final String CLIENT_ADMIN_CONTACT_EMAIL = "admin@admin.com";
    public static final String CLIENT_ADMIN_CONTACT_PHONE = "79873267677";
    public static final String CLIENT_ADMIN_CONTACT_DOB = "DOB";
    public static final String CLIENT_ADMIN_CONTACT_NOTES = "Звонить до 18:00";

    public static final String CLIENT_ADMIN_PAYER_NAME = "Городской совет";
    public static final String CLIENT_ADMIN_PAYER_INN = "57892375823582370";
    public static final String CLIENT_ADMIN_PAYER_NOTES = "Предлагать договор";

    public static final String CLIENT_ADMIN_DESCRIPTION = "Тестовые комментарии к новому клиенту";
    public static final String CLIENT_ADMIN_DIRECTION = "аттракционы";
    public static final String CLIENT_STATUS = "Активный";

    //---

    public static final String CLIENT_ADMIN_NAME_NEW = "Admin Client New";
    public static final String CLIENT_ADMIN_TYPE_NEW = "Обычный";

    public static final String CLIENT_ADMIN_OFFICE_NEW = "Тестовый офис";

    public static final String CLIENT_ADMIN_COUNTRY_NEW = "Россия";
    public static final String CLIENT_ADMIN_REGION_NEW = "Москва и Московская обл.";
    public static final String CLIENT_ADMIN_CITY_NEW = "Москва";
    public static final String CLIENT_ADMIN_ADDRESS_NEW = "ул. Константиновская, 5";
    public static final String CLIENT_ADMIN_SITE_NEW = "admin.com";

    public static final String CLIENT_ADMIN_CONTACT_NAME_NEW = "Admin Adminov";
    public static final String CLIENT_ADMIN_CONTACT_EMAIL_NEW = "admin@admin.ru";
    public static final String CLIENT_ADMIN_CONTACT_PHONE_NEW = "79873261217";
    public static final String CLIENT_ADMIN_CONTACT_DOB_NEW = "DOP";
    public static final String CLIENT_ADMIN_CONTACT_NOTES_NEW = "Звонить до 17:00";

    public static final String CLIENT_ADMIN_PAYER_NAME_NEW = "Городской суд";
    public static final String CLIENT_ADMIN_PAYER_INN_NEW = "57892375829982370";
    public static final String CLIENT_ADMIN_PAYER_NOTES_NEW = "Согласиться на предоплату";

    public static final String CLIENT_ADMIN_DESCRIPTION_NEW = "Тестовые комментарии к ранее созданному клиенту";
    public static final String CLIENT_ADMIN_DIRECTION_NEW = "услуги";

    //--SUPERVISOR
    public static final String CLIENT_SUPERVISOR_NAME = "Supervisor Client";
    public static final String CLIENT_SUPERVISOR_NAME_FOR_REQUEST = "Supervisor Client For Request";
    public static final String CLIENT_SUPERVISOR_NAME_FOR_DEAL = "Supervisor Client For Deal";
    public static final String CLIENT_SUPERVISOR_TYPE = "Обычный";

    public static final String CLIENT_SUPERVISOR_OFFICE = "Тестовый офис";
    public static final String CLIENT_SUPERVISOR_OFFICE_PREV = "Ростест СПб";

    public static final String CLIENT_SUPERVISOR_COUNTRY = "Россия";
    public static final String CLIENT_SUPERVISOR_REGION = "Москва и Московская обл.";
    public static final String CLIENT_SUPERVISOR_CITY = "Люберцы";
    public static final String CLIENT_SUPERVISOR_ADDRESS = "ул. Садовая, 18";
    public static final String CLIENT_SUPERVISOR_SITE = "supervisor.ru";

    public static final String CLIENT_SUPERVISOR_CONTACT_NAME = "Supervisor Supervisorovich";
    public static final String CLIENT_SUPERVISOR_CONTACT_EMAIL = "super@super.com";
    public static final String CLIENT_SUPERVISOR_CONTACT_PHONE = "79874557544";
    public static final String CLIENT_SUPERVISOR_CONTACT_DOB = "DOB";
    public static final String CLIENT_SUPERVISOR_CONTACT_NOTES = "Звонить после 19:00";

    public static final String CLIENT_SUPERVISOR_PAYER_NAME = "СОШ №4";
    public static final String CLIENT_SUPERVISOR_PAYER_INN = "57892375823586845";
    public static final String CLIENT_SUPERVISOR_PAYER_NOTES = "Предлагать услуги";

    public static final String CLIENT_SUPERVISOR_DIRECTION = "двери";

    //---

    public static final String CLIENT_SUPERVISOR_NAME_NEW = "Supervisor Client New";
    public static final String CLIENT_SUPERVISOR_TYPE_NEW = "VIP";

    public static final String CLIENT_SUPERVISOR_OFFICE_NEW = "Тестовый офис";

    public static final String CLIENT_SUPERVISOR_COUNTRY_NEW = "Россия";
    public static final String CLIENT_SUPERVISOR_REGION_NEW = "Москва и Московская обл.";
    public static final String CLIENT_SUPERVISOR_CITY_NEW = "Люберцы";
    public static final String CLIENT_SUPERVISOR_ADDRESS_NEW = "ул. Современная, 18";
    public static final String CLIENT_SUPERVISOR_SITE_NEW = "supervisor.com";

    public static final String CLIENT_SUPERVISOR_CONTACT_NAME_NEW = "Supervisor Supervisorov";
    public static final String CLIENT_SUPERVISOR_CONTACT_EMAIL_NEW = "super@super.ru";
    public static final String CLIENT_SUPERVISOR_CONTACT_PHONE_NEW = "79874557221";
    public static final String CLIENT_SUPERVISOR_CONTACT_DOB_NEW = "DOP";
    public static final String CLIENT_SUPERVISOR_CONTACT_NOTES_NEW = "Звонить после 20:00";

    public static final String CLIENT_SUPERVISOR_PAYER_NAME_NEW = "СОШ №17";
    public static final String CLIENT_SUPERVISOR_PAYER_INN_NEW = "57800175823586845";
    public static final String CLIENT_SUPERVISOR_PAYER_NOTES_NEW = "Предлагать товар";

    public static final String CLIENT_SUPERVISOR_DIRECTION_NEW = "уголь";

    //--ACCOUNTANT OFFICE
    public static final String CLIENT_ACCOUNT_OFFICE_NAME = "Accountant Office Client";
    public static final String CLIENT_ACCOUNT_OFFICE_NAME_FOR_REQUEST = "Accountant Office Client For Request";
    public static final String CLIENT_ACCOUNT_OFFICE_NAME_FOR_DEAL = "Accountant Office Client For Deal";
    public static final String CLIENT_ACCOUNT_OFFICE_NAME_FOR_TASK = "Accountant Office Client For Task";
    public static final String CLIENT_ACCOUNT_OFFICE_TYPE = "Проблемный";

    public static final String CLIENT_ACCOUNT_OFFICE_OFFICE = "Тестовый офис";

    public static final String CLIENT_ACCOUNT_OFFICE_COUNTRY = "Россия";
    public static final String CLIENT_ACCOUNT_OFFICE_REGION = "Тверская обл.";
    public static final String CLIENT_ACCOUNT_OFFICE_CITY = "Тверь";
    public static final String CLIENT_ACCOUNT_OFFICE_ADDRESS = "ул. Ленина, 54";
    public static final String CLIENT_ACCOUNT_OFFICE_SITE = "account-office.ru";

    public static final String CLIENT_ACCOUNT_OFFICE_CONTACT_NAME = "Accountant Officevich";
    public static final String CLIENT_ACCOUNT_OFFICE_CONTACT_EMAIL = "account@account.com";
    public static final String CLIENT_ACCOUNT_OFFICE_CONTACT_PHONE = "79873267001";
    public static final String CLIENT_ACCOUNT_OFFICE_CONTACT_DOB = "DOB";
    public static final String CLIENT_ACCOUNT_OFFICE_CONTACT_NOTES = "Писать на e-mail";

    public static final String CLIENT_ACCOUNT_OFFICE_PAYER_NAME = "КП Спектр";
    public static final String CLIENT_ACCOUNT_OFFICE_PAYER_INN = "57892375823580022";
    public static final String CLIENT_ACCOUNT_OFFICE_PAYER_NOTES = "Исключительно с постоплатой";

    public static final String CLIENT_ACCOUNT_OFFICE_DIRECTION = "арматура";

    //---

    public static final String CLIENT_ACCOUNT_OFFICE_NAME_NEW = "Accountant Office Client New";
    public static final String CLIENT_ACCOUNT_OFFICE_TYPE_NEW = "VIP";

    public static final String CLIENT_ACCOUNT_OFFICE_OFFICE_NEW = "Тестовый офис";

    public static final String CLIENT_ACCOUNT_OFFICE_COUNTRY_NEW = "Россия";
    public static final String CLIENT_ACCOUNT_OFFICE_REGION_NEW = "Тверская обл.";
    public static final String CLIENT_ACCOUNT_OFFICE_CITY_NEW = "Тверь";
    public static final String CLIENT_ACCOUNT_OFFICE_ADDRESS_NEW = "ул. Щорса, 2";
    public static final String CLIENT_ACCOUNT_OFFICE_SITE_NEW = "account-office.com";

    public static final String CLIENT_ACCOUNT_OFFICE_CONTACT_NAME_NEW = "Accountant Officevichen";
    public static final String CLIENT_ACCOUNT_OFFICE_CONTACT_EMAIL_NEW = "account@account.ru";
    public static final String CLIENT_ACCOUNT_OFFICE_CONTACT_PHONE_NEW = "79872267001";
    public static final String CLIENT_ACCOUNT_OFFICE_CONTACT_DOB_NEW = "DOP";
    public static final String CLIENT_ACCOUNT_OFFICE_CONTACT_NOTES_NEW = "Писать на e-mail с указанием темы письма";

    public static final String CLIENT_ACCOUNT_OFFICE_PAYER_NAME_NEW = "КП Спектрус";
    public static final String CLIENT_ACCOUNT_OFFICE_PAYER_INN_NEW = "57892300023580022";
    public static final String CLIENT_ACCOUNT_OFFICE_PAYER_NOTES_NEW = "Исключительно с БО";

    public static final String CLIENT_ACCOUNT_OFFICE_DIRECTION_NEW = "топливо";

    //--SENIOR MANAGER
    public static final String CLIENT_SENIOR_NAME = "Senior Client";
    public static final String CLIENT_SENIOR_NAME_FOR_REQUEST = "Senior Client For Request";
    public static final String CLIENT_SENIOR_NAME_FOR_DEAL = "Senior Client For Deal";
    public static final String CLIENT_SENIOR_TYPE = "Обычный";

    public static final String CLIENT_SENIOR_OFFICE = "Тестовый офис";

    public static final String CLIENT_SENIOR_COUNTRY = "Россия";
    public static final String CLIENT_SENIOR_REGION = "Белгородская обл.";
    public static final String CLIENT_SENIOR_CITY = "Белгород";
    public static final String CLIENT_SENIOR_ADDRESS = "ул. Короленко, 33";
    public static final String CLIENT_SENIOR_SITE = "senior.ru";

    public static final String CLIENT_SENIOR_CONTACT_NAME = "Senior Seniorovich";
    public static final String CLIENT_SENIOR_CONTACT_EMAIL = "senior@senior.com";
    public static final String CLIENT_SENIOR_CONTACT_PHONE = "79873267221";
    public static final String CLIENT_SENIOR_CONTACT_DOB = "DOB";
    public static final String CLIENT_SENIOR_CONTACT_NOTES = "Звонить после 9:00";

    public static final String CLIENT_SENIOR_PAYER_NAME = "ГП ТрансАэро";
    public static final String CLIENT_SENIOR_PAYER_INN = "57892375823585501";
    public static final String CLIENT_SENIOR_PAYER_NOTES = "Исключить предоплату";

    public static final String CLIENT_SENIOR_DIRECTION = "НТД (нормативно-техническая документация)";

    //---

    public static final String CLIENT_SENIOR_NAME_NEW = "Senior Client NEW";
    public static final String CLIENT_SENIOR_TYPE_NEW = "Обычный";

    public static final String CLIENT_SENIOR_OFFICE_NEW = "Тестовый офис";

    public static final String CLIENT_SENIOR_COUNTRY_NEW = "Россия";
    public static final String CLIENT_SENIOR_REGION_NEW = "Белгородская обл.";
    public static final String CLIENT_SENIOR_CITY_NEW = "Белгород";
    public static final String CLIENT_SENIOR_ADDRESS_NEW = "ул. Северная, 33";
    public static final String CLIENT_SENIOR_SITE_NEW = "senior.com";

    public static final String CLIENT_SENIOR_CONTACT_NAME_NEW = "Senior Senioroviches";
    public static final String CLIENT_SENIOR_CONTACT_EMAIL_NEW = "senior@senior.ru";
    public static final String CLIENT_SENIOR_CONTACT_PHONE_NEW = "79870167221";
    public static final String CLIENT_SENIOR_CONTACT_DOB_NEW = "DOP";
    public static final String CLIENT_SENIOR_CONTACT_NOTES_NEW = "Звонить после 8:00";

    public static final String CLIENT_SENIOR_PAYER_NAME_NEW = "ГП ТрансАэроМир";
    public static final String CLIENT_SENIOR_PAYER_INN_NEW = "57892375333085501";
    public static final String CLIENT_SENIOR_PAYER_NOTES_NEW = "Исключить предоплату без договора";

    public static final String CLIENT_SENIOR_DIRECTION_NEW = "сизы";

    //--MANAGER
    public static final String CLIENT_MANAGER_NAME = "Manager Client";
    public static final String CLIENT_MANAGER_NAME_FOR_REQUEST = "Manager Client For Request";
    public static final String CLIENT_MANAGER_NAME_FOR_DEAL = "Manager Client For Deal";
    public static final String CLIENT_MANAGER_TYPE = "VIP";

    public static final String CLIENT_MANAGER_OFFICE = "Тестовый офис";

    public static final String CLIENT_MANAGER_COUNTRY = "Россия";
    public static final String CLIENT_MANAGER_REGION = "Ставропольский край";
    public static final String CLIENT_MANAGER_CITY = "Ставрополь";
    public static final String CLIENT_MANAGER_ADDRESS = "ул. Киевская, 198";
    public static final String CLIENT_MANAGER_SITE = "manager.ru";

    public static final String CLIENT_MANAGER_CONTACT_NAME = "Manager Managerovich";
    public static final String CLIENT_MANAGER_CONTACT_EMAIL = "manager@manager.com";
    public static final String CLIENT_MANAGER_CONTACT_PHONE = "79873267110";
    public static final String CLIENT_MANAGER_CONTACT_DOB = "DOB";
    public static final String CLIENT_MANAGER_CONTACT_NOTES = "Звонить в любое время";

    public static final String CLIENT_MANAGER_PAYER_NAME = "НПК Союз";
    public static final String CLIENT_MANAGER_PAYER_INN = "57892375823581101";
    public static final String CLIENT_MANAGER_PAYER_NOTES = "Безналичный расчет";

    public static final String CLIENT_MANAGER_DIRECTION = "бытовая химия";

    //---

    public static final String CLIENT_MANAGER_NAME_NEW = "Manager Client New";
    public static final String CLIENT_MANAGER_TYPE_NEW = "Обычный";

    public static final String CLIENT_MANAGER_OFFICE_NEW = "Тестовый офис";

    public static final String CLIENT_MANAGER_COUNTRY_NEW = "Россия";
    public static final String CLIENT_MANAGER_REGION_NEW = "Ставропольский край";
    public static final String CLIENT_MANAGER_CITY_NEW = "Ставрополь";
    public static final String CLIENT_MANAGER_ADDRESS_NEW = "ул. Западная, 198";
    public static final String CLIENT_MANAGER_SITE_NEW = "manager.com";

    public static final String CLIENT_MANAGER_CONTACT_NAME_NEW = "Manager Manageroviches";
    public static final String CLIENT_MANAGER_CONTACT_EMAIL_NEW = "manager@manager.ru";
    public static final String CLIENT_MANAGER_CONTACT_PHONE_NEW = "79873888110";
    public static final String CLIENT_MANAGER_CONTACT_DOB_NEW = "DOP";
    public static final String CLIENT_MANAGER_CONTACT_NOTES_NEW = "Звонить в любое время, кроме ночи";

    public static final String CLIENT_MANAGER_PAYER_NAME_NEW = "НПК Союз Металлургов";
    public static final String CLIENT_MANAGER_PAYER_INN_NEW = "57892370023581101";
    public static final String CLIENT_MANAGER_PAYER_NOTES_NEW = "Безналичный расчет";

    public static final String CLIENT_MANAGER_DIRECTION_NEW = "табак";

    //CREATE REQUEST DATA

    public static final String REQUEST_DATE_FROM = "25-08-2016";
    public static final String REQUEST_DATE = "31-12-2022";
    public static final String REQUEST_DATE_TO = "01-12-2022";
    public static final String REQUEST_RESPONSIBLE_PERSON = "Администратор";
    public static final String REQUEST_TEXT = "Содержание запроса";

    public static final String REQUEST_OFFICE = "Тестовый офис";

    public static final String REQUEST_STATUS = "Новый";
    public static final String REQUEST_STATUS_DECLINE = "Отказ";
    public static final String REQUEST_TYPE = "Viber";
    public static final String REQUEST_SITE = "testurl.ru";
    public static final String REQUEST_DETAILS = "Очень детально";

    public static final String REQUEST_REPLY_TEXT = "Заключить контракт на 3 года";
    public static final String REQUEST_NOTIFICATION = "30-09-2017";
    public static final String REQUEST_COMMENTS = "Можно без предоплаты, заранее оговорить все возможные проблемы и форс-мажорные ситуации";
    public static final String REQUEST_REJECTION_REASON = "Готовы заключить контракт только на 1 год с опцией преждевременного расторжения";

    public static final String REQUEST_DIRECTION = "бытовая химия";

    //---

    public static final String REQUEST_DATE_FROM_NEW = "24-08-2016";
    public static final String REQUEST_DATE_NEW = "30-08-2022";
    public static final String REQUEST_DATE_TO_NEW = "03-12-2022";
    public static final String REQUEST_RESPONSIBLE_PERSON_NEW = "Администратор";
    public static final String REQUEST_TEXT_NEW = "Новое содержание запроса";

    public static final String REQUEST_OFFICE_NEW = "Тестовый офис";

    public static final String REQUEST_STATUS_NEW = "Новый";
    public static final String REQUEST_TYPE_NEW = "Skype";
    public static final String REQUEST_SITE_NEW = "testurl.com";
    public static final String REQUEST_DETAILS_NEW = "Детали запроса";

    public static final String REQUEST_REPLY_TEXT_NEW = "Заключить контракт на 2 года";
    public static final String REQUEST_NOTIFICATION_NEW = "26-09-2022";
    public static final String REQUEST_COMMENTS_NEW = "Можно без предоплаты, но заранее оговорить все возможные форс-мажорные ситуации";
    public static final String REQUEST_REJECTION_REASON_NEW = "Готовы заключить контракт только на 2 года с опцией преждевременного расторжения";

    public static final String REQUEST_DIRECTION_NEW = "дорожные знаки";

    //_______________________________________________________\\

    //--ADMIN

    public static final String REQUEST_ADMIN_CLIENT = "Admin Client from Request";
    public static final String REQUEST_ADMIN_CLIENT_BANK = "Admin Client for Bank TR";
    public static final String REQUEST_ADMIN_CLIENT_CARD = "Admin Client for Card TR";
    public static final String REQUEST_ADMIN_CLIENT_CASH = "Admin Client for Cash TR";
    public static final String REQUEST_ADMIN_CLIENT_TASK = "Admin Client for Task";
    public static final String REQUEST_ADMIN_COUNTRY = "Россия";
    public static final String REQUEST_ADMIN_REGION = "Москва и Московская обл.";
    public static final String REQUEST_ADMIN_CITY = "Москва";

    public static final String REQUEST_ADMIN_CONTACT_NAME = "Клиент от админа с заявок";
    public static final String REQUEST_ADMIN_CONTACT_PHONE = "380574325566";
    public static final String REQUEST_ADMIN_CONTACT_EMAIL = "client@admin.ru";
    public static final String REQUEST_ADMIN_CONTACT_DOB = "Add";
    public static final String REQUEST_ADMIN_CONTACT_NOTES = "Пишите круглосуточно";

    public static final String REQUEST_ADMIN_CLIENT_NEW = "Admin Client from Request page New";
    public static final String REQUEST_ADMIN_COUNTRY_NEW = "Россия";
    public static final String REQUEST_ADMIN_REGION_NEW = "Москва и Московская обл.";
    public static final String REQUEST_ADMIN_CITY_NEW = "Москва";

    public static final String REQUEST_ADMIN_CONTACT_NAME_NEW = "Клиент от админа на заявках обновленный";
    public static final String REQUEST_ADMIN_CONTACT_PHONE_NEW = "380574534441";
    public static final String REQUEST_ADMIN_CONTACT_EMAIL_NEW = "client@admin.ru";
    public static final String REQUEST_ADMIN_CONTACT_DOB_NEW = "Add N";
    public static final String REQUEST_ADMIN_CONTACT_NOTES_NEW = "Звонить после 6:00";

    //--SUPERVISOR

    public static final String REQUEST_SUPERVISOR_CLIENT = "Supervisor Client from Request";
    public static final String REQUEST_SUPERVISOR_CLIENT_BANK = "Supervisor Client for Bank TR";
    public static final String REQUEST_SUPERVISOR_CLIENT_CARD = "Supervisor Client for Card TR";
    public static final String REQUEST_SUPERVISOR_CLIENT_CASH = "Supervisor Client for Cash TR";
    public static final String REQUEST_SUPERVISOR_CLIENT_TASK = "Supervisor Client for Task";
    public static final String REQUEST_SUPERVISOR_COUNTRY = "Россия";
    public static final String REQUEST_SUPERVISOR_REGION = "Белгородская обл.";
    public static final String REQUEST_SUPERVISOR_CITY = "Белгород";

    public static final String REQUEST_SUPERVISOR_CONTACT_NAME = "Клиент от супервайзера с заявок";
    public static final String REQUEST_SUPERVISOR_CONTACT_PHONE = "380574325665";
    public static final String REQUEST_SUPERVISOR_CONTACT_EMAIL = "supervisor@admin.ru";
    public static final String REQUEST_SUPERVISOR_CONTACT_NOTES = "Пишите в любое время";

    public static final String REQUEST_SUPERVISOR_CLIENT_NEW = "Supervisor Client from Request New";
    public static final String REQUEST_SUPERVISOR_COUNTRY_NEW = "Россия";
    public static final String REQUEST_SUPERVISOR_REGION_NEW = "Москва и Московская обл.";
    public static final String REQUEST_SUPERVISOR_CITY_NEW = "Москва";

    public static final String REQUEST_SUPERVISOR_CONTACT_NAME_NEW = "Клиент от супервайзера на заявках обновленный";
    public static final String REQUEST_SUPERVISOR_CONTACT_PHONE_NEW = "380574325331";
    public static final String REQUEST_SUPERVISOR_CONTACT_EMAIL_NEW = "supervisor@admin.com";
    public static final String REQUEST_SUPERVISOR_CONTACT_NOTES_NEW = "Пишите круглосуточно";

    //--ACCOUNTANT OFFICE

    public static final String REQUEST_ACCOUNTANT_CLIENT = "Accountant Client from Request";
    public static final String REQUEST_ACCOUNTANT_CLIENT_BANK = "Accountant Client for Bank TR";
    public static final String REQUEST_ACCOUNTANT_CLIENT_CARD = "Accountant Client for Card TR";
    public static final String REQUEST_ACCOUNTANT_CLIENT_CASH = "Accountant Client for Cash TR";
    public static final String REQUEST_ACCOUNTANT_CLIENT_TASK = "Accountant Client for Task";
    public static final String REQUEST_ACCOUNTANT_COUNTRY = "Россия";
    public static final String REQUEST_ACCOUNTANT_REGION = "Москва и Московская обл.";
    public static final String REQUEST_ACCOUNTANT_CITY = "Москва";

    public static final String REQUEST_ACCOUNTANT_CONTACT_NAME = "Клиент от бухгалтера с заявок";
    public static final String REQUEST_ACCOUNTANT_CONTACT_PHONE = "380574325022";
    public static final String REQUEST_ACCOUNTANT_CONTACT_EMAIL = "client@accountant.ru";
    public static final String REQUEST_ACCOUNTANT_CONTACT_NOTES = "Пишите всегда";

    public static final String REQUEST_ACCOUNTANT_CLIENT_NEW = "Accountant Client from Request New";
    public static final String REQUEST_ACCOUNTANT_COUNTRY_NEW = "Россия";
    public static final String REQUEST_ACCOUNTANT_REGION_NEW = "Москва и Московская обл.";
    public static final String REQUEST_ACCOUNTANT_CITY_NEW = "Москва";

    public static final String REQUEST_ACCOUNTANT_CONTACT_NAME_NEW = "Клиент от бухгалтера на заявках обновленный";
    public static final String REQUEST_ACCOUNTANT_CONTACT_PHONE_NEW = "380574388866";
    public static final String REQUEST_ACCOUNTANT_CONTACT_EMAIL_NEW = "client@accountant.ru";
    public static final String REQUEST_ACCOUNTANT_CONTACT_NOTES_NEW = "Звоните круглосуточно";

    //--SENIOR

    public static final String REQUEST_SENIOR_CLIENT = "Senior Client from Request";
    public static final String REQUEST_SENIOR_CLIENT_BANK = "Senior Client for Bank TR";
    public static final String REQUEST_SENIOR_CLIENT_CARD = "Senior Client for Card TR";
    public static final String REQUEST_SENIOR_CLIENT_CASH = "Senior Client for Cash TR";
    public static final String REQUEST_SENIOR_CLIENT_TASK = "Senior Client for Task";
    public static final String REQUEST_SENIOR_COUNTRY = "Россия";
    public static final String REQUEST_SENIOR_REGION = "Белгородская обл.";
    public static final String REQUEST_SENIOR_CITY = "Белгород";

    public static final String REQUEST_SENIOR_CONTACT_NAME = "Клиент от старшего менеджера с заявок";
    public static final String REQUEST_SENIOR_CONTACT_PHONE = "380574325665";
    public static final String REQUEST_SENIOR_CONTACT_EMAIL = "client@senior.ru";
    public static final String REQUEST_SENIOR_CONTACT_NOTES = "Пишите после 9:00";

    public static final String REQUEST_SENIOR_CLIENT_NEW = "Admin Client from Request New";
    public static final String REQUEST_SENIOR_COUNTRY_NEW = "Россия";
    public static final String REQUEST_SENIOR_REGION_NEW = "Белгородская обл.";
    public static final String REQUEST_SENIOR_CITY_NEW = "Белгород";

    public static final String REQUEST_SENIOR_CONTACT_NAME_NEW = "Клиент от старшего менеджера на заявках обновленный";
    public static final String REQUEST_SENIOR_CONTACT_PHONE_NEW = "380574320046";
    public static final String REQUEST_SENIOR_CONTACT_EMAIL_NEW = "client@senior.ru";
    public static final String REQUEST_SENIOR_CONTACT_NOTES_NEW = "Звоните до 17:00";

    //--MANAGER

    public static final String REQUEST_MANAGER_CLIENT = "Manager Client from Request";
    public static final String REQUEST_MANAGER_CLIENT_BANK = "Manager Client for Bank TR";
    public static final String REQUEST_MANAGER_CLIENT_CARD = "Manager Client for Card TR";
    public static final String REQUEST_MANAGER_CLIENT_CASH = "Manager Client for Cash TR";
    public static final String REQUEST_MANAGER_CLIENT_TASK = "Manager Client for Task";
    public static final String REQUEST_MANAGER_COUNTRY = "Россия";
    public static final String REQUEST_MANAGER_REGION = "Москва и Московская обл.";
    public static final String REQUEST_MANAGER_CITY = "Москва";

    public static final String REQUEST_MANAGER_CONTACT_NAME = "Клиент от менеджера с заявок";
    public static final String REQUEST_MANAGER_CONTACT_PHONE = "380571105566";
    public static final String REQUEST_MANAGER_CONTACT_EMAIL = "client@manager.ru";
    public static final String REQUEST_MANAGER_CONTACT_NOTES = "Пишите на корпоративную электронную почту";

    public static final String REQUEST_MANAGER_CLIENT_NEW = "Manager Client from Request";
    public static final String REQUEST_MANAGER_COUNTRY_NEW = "Россия";
    public static final String REQUEST_MANAGER_REGION_NEW = "Москва и Московская обл.";
    public static final String REQUEST_MANAGER_CITY_NEW = "Москва";

    public static final String REQUEST_MANAGER_CONTACT_NAME_NEW = "Клиент от менеджера на заявках";
    public static final String REQUEST_MANAGER_CONTACT_PHONE_NEW = "380574325333";
    public static final String REQUEST_MANAGER_CONTACT_EMAIL_NEW = "client@manager.ru";
    public static final String REQUEST_MANAGER_CONTACT_NOTES_NEW = "Звоните до 23:00";


    //CREATE DEAL DATA

    public static final String DEAL_NUMBER_FIN_TAB = "FT1";
    public static final String DEAL_NUMBER_FOR_CLOSED = "CLS1";

    public static final String DEAL_NUMBER_FOR_LAYOUT_DEAL = "LAYDEAL";

    //--ADMIN

    public static final String DEAL_NUMBER_ADMIN = "AT101";
    public static final String DEAL_NUMBER_ADMIN_NEW = "AT111";
    public static final String DEAL_NUMBER_ADMIN_BNKT = "BNKT101";
    public static final String DEAL_NUMBER_ADMIN_CRDT = "CRDTA101";
    public static final String DEAL_NUMBER_ADMIN_CSHT = "CSHT101";

    //--SUPERVISOR

    public static final String DEAL_NUMBER_SUPERVISOR = "AT202";
    public static final String DEAL_NUMBER_SUPERVISOR_NEW = "AT222";
    public static final String DEAL_NUMBER_SUPERVISOR_BNKT = "BNKT202";
    public static final String DEAL_NUMBER_SUPERVISOR_CRDT = "CRDTA202";
    public static final String DEAL_NUMBER_SUPERVISOR_CSHT = "CSHT202";

    //--ACCOUNTANT OFFICE

    public static final String DEAL_NUMBER_ACCOUNTANT = "AT303";
    public static final String DEAL_NUMBER_ACCOUNTANT_NEW = "AT333";
    public static final String DEAL_NUMBER_ACCOUNTANT_BNKT = "BNKT303";
    public static final String DEAL_NUMBER_ACCOUNTANT_CRDT = "CRDTA303";
    public static final String DEAL_NUMBER_ACCOUNTANT_CSHT = "CSHT303";

    //--SENIOR

    public static final String DEAL_NUMBER_SENIOR = "AT404";
    public static final String DEAL_NUMBER_SENIOR_NEW = "AT444";
    public static final String DEAL_NUMBER_SENIOR_BNKT = "BNKT404";
    public static final String DEAL_NUMBER_SENIOR_CRDT = "CRDTA404";
    public static final String DEAL_NUMBER_SENIOR_CSHT = "CSHT404";

    //--MANAGER

    public static final String DEAL_NUMBER_MANAGER = "AT505";
    public static final String DEAL_NUMBER_MANAGER_NEW = "AT555";
    public static final String DEAL_NUMBER_MANAGER_BNKT = "BNKT505";
    public static final String DEAL_NUMBER_MANAGER_CRDT = "CRDTA505";
    public static final String DEAL_NUMBER_MANAGER_CSHT = "CSHT505";

    //--DEAL CORRECT DATA


    public static final String DEAL_CORRECT_AGENT_CASH_SUM = "170";
    public static final String DEAL_CORRECT_AGENT_NO_CASH_SUM = "190";
    public static final String DEAL_CORRECT_NUMBER = "CRRCT1";

    public static final String DEAL_CORRECT_ASSIGNED_CASH_SUM = "145";
    public static final String DEAL_CORRECT_ASSIGNED_NO_CASH_SUM = "150";
    public static final String DEAL_CORRECT_PAID_CASH_SUM = "135";
    public static final String DEAL_CORRECT_NO_PAID_CASH_SUM = "140";
    public static final String DEAL_CORRECT_COST_CASH_SUM = "110";
    public static final String DEAL_CORRECT_COST_NO_CASH_SUM = "115";

    public static final String DEAL_CORRECT = "Корректировки";


    //--

    public static final String DEAL_NUMBER = "AT404";
    public static final String DEAL_PROVIDER = "Веста";
    public static final String DEAL_PAYER = "Нет";
    public static final String DEAL_START_DATE = "01-09-2017";
    public static final String DEAL_END_DATE = "01-09-2022";
    public static final String DEAL_RATIO = "0.9";
    public static final String DEAL_COMMENT = "Очень важное дело";
    public static final String DEAL_STATUS = "Частичная оплата";
    public static final String DEAL_STATUS_CLOSE = "Закрыто";
    public static final String DEAL_DOCUMENT = "Карантинный серт";
    public static final String DEAL_DOC_QUANTITY = "2";
    public static final String DEAL_DOC_MEANING = "Официальное оформление";
    public static final String DEAL_DOC_NUMBER = "5111";
    public static final String DEAL_DOC_END_DATE = "02-09-2022";
    public static final String DEAL_OKPD2 = "Вещества химические и продукты химические";
    public static final String DEAL_TNVED = "I. Химические элементы";
    public static final String DEAL_ASSIGNED_CASH_SUM = "45";
    public static final String DEAL_ASSIGNED_NO_CASH_SUM = "50";
    public static final String DEAL_ASSIGNED_NO_CASH_CURRENCY = "$";
    public static final String DEAL_PAID_CASH_SUM = "35";
    public static final String DEAL_NO_PAID_CASH_SUM = "40";
    public static final String DEAL_PAID_NO_CASH_ACCOUNT_NUMBER = "4444";
    public static final String DEAL_ORDER_NUMBER = "3232323254545454";
    public static final String DEAL_COURIER = "Пони";
    public static final String DEAL_AGENT_CASH_SUM = "20";
    public static final String DEAL_AGENT_NO_CASH_SUM = "25";
    public static final String DEAL_AGENT_COMMENT = "Поэтапное повышение оплаты";
    public static final String DEAL_COST_CASH_SUM = "10";
    public static final String DEAL_COST_NO_CASH_SUM = "15";
    public static final String DEAL_COST_NO_CASH_SUM_CURRENCY = "$";
    public static final String DEAL_INCOME_INVOICE_NUMBER = "5432234554322345";
    public static final String DEAL_OFFICE_RATIO = "1.5";
    public static final String DEAL_TRANSFER = "0.03";
    public static final String DEAL_TAXPERCENT = "0.05";

    //_____________________________________________________________________\\

    public static final String DEAL_NUMBER_NEW = "AT505";
    public static final String DEAL_PROVIDER_NEW = "Веста";
    public static final String DEAL_PAYER_NEW = "Нет";
    public static final String DEAL_START_DATE_NEW = "02-09-2017";
    public static final String DEAL_END_DATE_NEW = "02-09-2019";
    public static final String DEAL_RATIO_NEW = "0.8";
    public static final String DEAL_COMMENT_NEW = "Не очень важное дело";
    public static final String DEAL_STATUS_NEW = "Счет оплачен";
    public static final String DEAL_DOCUMENT_NEW = "Аутсорсинг сертификации";
    public static final String DEAL_DOC_QUANTITY_NEW = "3";
    public static final String DEAL_DOC_MEANING_NEW = "Официальное оформление после прохождения обучения";
    public static final String DEAL_DOC_NUMBER_NEW = "5222";
    public static final String DEAL_DOC_END_DATE_NEW = "03-09-2018";
    public static final String DEAL_OKPD2_NEW = "Щепа технологическая";
    public static final String DEAL_TNVED_NEW = "I. Химические элементы";
    public static final String DEAL_ASSIGNED_CASH_SUM_NEW = "55";
    public static final String DEAL_ASSIGNED_NO_CASH_SUM_NEW = "60";
    public static final String DEAL_ASSIGNED_NO_CASH_CURRENCY_NEW = "$";
    public static final String DEAL_PAID_CASH_SUM_NEW = "45";
    public static final String DEAL_NO_PAID_CASH_SUM_NEW = "50";
    public static final String DEAL_PAID_NO_CASH_ACCOUNT_NUMBER_NEW = "3333";
    public static final String DEAL_ORDER_NUMBER_NEW = "1234567887654321";
    public static final String DEAL_COURIER_NEW = "Пони";
    public static final String DEAL_AGENT_CASH_SUM_NEW = "30";
    public static final String DEAL_AGENT_NO_CASH_SUM_NEW = "35";
    public static final String DEAL_AGENT_COMMENT_NEW = "Поэтапное повышение оплаты за права";
    public static final String DEAL_COST_CASH_SUM_NEW = "15";
    public static final String DEAL_COST_NO_CASH_SUM_NEW = "20";
    public static final String DEAL_COST_NO_CASH_SUM_CURRENCY_NEW = "$";
    public static final String DEAL_INCOME_INVOICE_NUMBER_NEW = "6767898954543232";
    public static final String DEAL_OFFICE_RATIO_NEW = "1.9";
    public static final String DEAL_TRANSFER_NEW = "0.05";
    public static final String DEAL_TAXPERCENT_NEW = "0.09";

    //BANK TRANSFER DATA

    public static final String BANK_TRANSFER_TYPE_DEAL = "Счет клиенту";
    public static final String BANK_TRANSFER_TYPE_DEAL_SEARCH = "Счет по делу";
    public static final String BANK_TRANSFER_TYPE_ANOTHER = "Другой счет";
    public static final String BANK_TRANSFER_TYPE_FINANCE = "Финансовый счет";
    public static final String BANK_TRANSFER_TYPE_MOVEMENT = "Неопознанное движение";

    public static final String BANK_TRANSFER_DATE_FROM = "01-06-2017";
    public static final String BANK_TRANSFER_DATE_TO = "31-12-2022";

    public static final String BANK_TRANSFER_OFFICE = "Тестовый офис";
    public static final String BANK_TRANSFER_INCOME_FOR_DEAL_PROVIDER = "Счет от поставщика";
    public static final String BANK_TRANSFER_INCOME_FOR_DEAL_CLIENT = "Счет клиенту";
    public static final String BANK_TRANSFER_INCOME_FOR_ANOTHER = "Счет на офисный расход";
    public static final String BANK_TRANSFER_INCOME_FOR_MOVEMENT = "Неопознанный приход BANK";
    public static final String BANK_TRANSFER_OPERATION_TYPE_INCOME = "банковский приход";
    public static final String BANK_TRANSFER_OPERATION_TYPE_EXPENSE = "банковский расход";
    public static final String BANK_TRANSFER_OPERATION_CODE_FOR_DEAL = "РД";
    public static final String BANK_TRANSFER_OPERATION_CODE_FOR_ANOTHER = "РР";
    public static final String BANK_TRANSFER_OPERATION_CODE_FOR_MOVEMENT = "НП";
    public static final String BANK_TRANSFER_DEAL_NUMBER = "241";////////////////////////////////////////////////////////////
    public static final String BANK_TRANSFER_DEAL_COST = "100";
    public static final String BANK_TRANSFER_DEAL_PAID = "235";

    public static final String BANK_TRANSFER_DEAL_SERVICE = "Лиц алкоголь";
    public static final String BANK_TRANSFER_DEAL_CONTENT = "Тестовое содержание";
    public static final String BANK_TRANSFER_DEAL_QUANTITY = "3";
    public static final String BANK_TRANSFER_DEAL_PRICE = "1600";
    public static final String BANK_TRANSFER_DEAL_VAR_PERCENT = "20";
    public static final String BANK_TRANSFER_DEAL_VAR = "400";
    public static final String BANK_TRANSFER_DEAL_SUMM = "4800";

    public static final String BANK_TRANSFER_PAYER_ID = "ООО \"КОФЕ-СИТИ\"";
    public static final String BANK_TRANSFER_PROVIDER_ID = "Веста";
    public static final String BANK_TRANSFER_ORGANIZATION_ID = "Отсутствует";
    public static final String BANK_TRANSFER_BILL_NUMBER = "434434";
    public static final String BANK_TRANSFER_BILL_NUMBER_FOR_CD = "343343";
    public static final String BANK_TRANSFER_BILL_DATE = "10-09-2022";
    public static final String BANK_TRANSFER_BILL_STATUS = "Оплачен частично";
    public static final String BANK_TRANSFER_BILL_DESCRIPTION = "Примечание к счету";
    public static final String BANK_TRANSFER_STATUS = "Активно";

    public static final String BANK_TRANSFER_CHARGE_NUMBER = "5432112345";
    public static final String BANK_TRANSFER_CHARGE_DATE_PAYMENT = "27-06-2022";
    public static final String BANK_TRANSFER_CHARGE_DATE = "30-06-2022";
    public static final String BANK_TRANSFER_CHARGE_SUM = "500";
    public static final String BANK_TRANSFER_CHARGE_COMMISSION = "2%";

    public static final String BANK_TRANSFER_ACT_DATE_CLOSE = "05-07-2017";
    public static final String BANK_TRANSFER_ACT_NUMBER = "4444";
    public static final String BANK_TRANSFER_ACT_INVOICE_NUMBER = "98989898988998";

    //____________________________________________________________________

    public static final String BANK_TRANSFER_OFFICE_NEW = "Тестовый офис";
    public static final String BANK_TRANSFER_INCOME_NEW = "Счет от поставщика";
    public static final String BANK_TRANSFER_OPERATION_TYPE_NEW = "банковский расход";
    public static final String BANK_TRANSFER_OPERATION_CODE_NEW = "АГ";
    public static final String BANK_TRANSFER_DEAL_NUMBER_NEW = "241";////////////////////////////////////////////////////////////
    public static final String BANK_TRANSFER_DEAL_COST_NEW = "30000";
    public static final String BANK_TRANSFER_DEAL_PAID_NEW = "235000";
    public static final String BANK_TRANSFER_PAYER_ID_NEW = "нет";
    public static final String BANK_TRANSFER_PROVIDER_ID_NEW = "";
    public static final String BANK_TRANSFER_ORGANIZATION_ID_NEW = "ОО \"Белотест\"";
    public static final String BANK_TRANSFER_BILL_NUMBER_NEW = "545545";
    public static final String BANK_TRANSFER_BILL_DATE_NEW = "11-09-2017";
    public static final String BANK_TRANSFER_BILL_STATUS_NEW = "Аннулирован";
    public static final String BANK_TRANSFER_BILL_DESCRIPTION_NEW = "Измененное замечание к счету";
    public static final String BANK_TRANSFER_STATUS_NEW = "Активно";

    public static final String BANK_TRANSFER_CHARGE_NUMBER_NEW = "462624646346";
    public static final String BANK_TRANSFER_CHARGE_DATE_PAYMENT_NEW = "28-06-2017";
    public static final String BANK_TRANSFER_CHARGE_DATE_NEW = "02-07-2017";
    public static final String BANK_TRANSFER_CHARGE_SUM_NEW = "60000";
    public static final String BANK_TRANSFER_CHARGE_COMMISSION_NEW = "4%";

    public static final String BANK_TRANSFER_ACT_DATE_CLOSE_NEW = "06-07-2017";
    public static final String BANK_TRANSFER_ACT_NUMBER_NEW = "55566";
    public static final String BANK_TRANSFER_ACT_INVOICE_NUMBER_NEW = "4623634654725244";

    public static final String BANK_TRANSFER_SUMMARY = "100";
    public static final String BANK_TRANSFER_CURRENCY = "R";
    public static final String BANK_TRANSFER_LEGAL_NAME = "ООО Метинвест";
    public static final String BANK_TRANSFER_LEGAL_INN = "123456789012";
    public static final String BANK_TRANSFER_LEGAL_KPP = "123456789";

    //CARD TRANSFER DATA

    public static final String CARD_TRANSFER_TYPE_DEAL = "Заявка по карте по делу";
    public static final String CARD_TRANSFER_TYPE_ANOTHER = "Другая заявка по карте";
    public static final String CARD_TRANSFER_TYPE_MOVEMENT = "Неопознанное";

    public static final String CARD_TRANSFER_DATE_FROM = "01-01-2017";
    public static final String CARD_TRANSFER_DATE_TO = "31-12-2022";

    public static final String CARD_TRANSFER_OFFICE = "Тестовый офис";
    public static final String CARD_TRANSFER_INCOME_FOR_DEAL = "Агентские клиенту через карту";
    public static final String CARD_TRANSFER_INCOME_FOR_ANOTHER = "Другой приход по карте";
    public static final String CARD_TRANSFER_INCOME_FOR_MOVEMENT = "Неопознанный приход по карте";
    public static final String CARD_TRANSFER_OPERATION_TYPE = "Приход по карте";
    public static final String CARD_TRANSFER_OPERATION_CODE_FOR_DEAL = "ПД";
    public static final String CARD_TRANSFER_OPERATION_CODE_FOR_ANOTHER = "ПР";
    public static final String CARD_TRANSFER_OPERATION_CODE_FOR_MOVEMENT = "НП";
    public static final String CARD_TRANSFER_DEAL_NUMBER = "A404";////////////////////////////////////////////////////////////
    public static final String CARD_TRANSFER_DEAL_COST = "1000";
    public static final String CARD_TRANSFER_DEAL_PAID = "2350";
    public static final String CARD_TRANSFER_PROVIDER = "";
    public static final String CARD_TRANSFER_BILL_STATUS = "Оплачен частично";
    public static final String CARD_TRANSFER_BILL_DESCRIPTION = "Примечание к счету";
    public static final String CARD_TRANSFER_STATUS = "Активно";

    public static final String CARD_TRANSFER_CHARGE_BANK = "Сбербанк";
    public static final String CARD_TRANSFER_CHARGE_DATE_PAYMENT = "29-06-2022";
    public static final String CARD_TRANSFER_CHARGE_SUM = "500";
    public static final String CARD_TRANSFER_CHARGE_COMMISSION = "2";

    public static final String CARD_TRANSFER_SUMMARY = "3890";
    public static final String CARD_TRANSFER_CURRENCY = "$";

    public static final String CARD_TRANSFER_NAME = "Спонсорская помощь";

    //CASH TRANSFER DATA

    public static final String CASH_TRANSFER_TYPE_DEAL = "cash_by_deal";
    public static final String CASH_TRANSFER_TYPE_ANOTHER = "cash_without_deal";
    public static final String CASH_TRANSFER_TYPE_MOVEMENT = "cash_other_office";
    public static final String CASH_TRANSFER_TYPE_MOVEMENT_MONEY = "cash_other_office_money";

    public static final String CASH_TRANSFER_DATE_FROM = "01-01-2017";
    public static final String CASH_TRANSFER_DATE_TO = "31-12-2022";

    public static final String CASH_TRANSFER_OFFICE = "Тестовый офис";
    public static final String CASH_TRANSFER_INCOME_FOR_DEAL = "Приход по делу";
    public static final String CASH_TRANSFER_INCOME_FOR_ANOTHER = "Другой приход";
    public static final String CASH_TRANSFER_INCOME_FOR_MOVEMENT = "Заявка в другой офис - расход по делу";
    public static final String CASH_TRANSFER_INCOME_FOR_MOVEMENT_MONEY = "Другой приход";
    public static final String CASH_TRANSFER_OPERATION_TYPE = "Наличный приход";
    public static final String CASH_TRANSFER_OPERATION_TYPE_FOR_MOVEMENT_MONEY = "Наличный расход";
    public static final String CASH_TRANSFER_OPERATION_CODE_FOR_DEAL = "ПД";
    public static final String CASH_TRANSFER_OPERATION_CODE_FOR_ANOTHER = "ПР";
    public static final String CASH_TRANSFER_OPERATION_CODE_FOR_MOVEMENT = "АГ";
    public static final String CASH_TRANSFER_OPERATION_CODE_FOR_MOVEMENT2 = "РО";
    public static final String CASH_TRANSFER_DEAL_COST = "10000";
    public static final String CASH_TRANSFER_DEAL_PAID = "23500";
    public static final String CASH_TRANSFER_MOVEMENT_DATE = "01-09-2022";
    public static final String CASH_TRANSFER_CLIENT = "НПО Энергосталь";
    public static final String CASH_TRANSFER_PROVIDER = "Веста";
    public static final String CASH_TRANSFER_WORKER = "Kesha Parrot";
    public static final String CASH_TRANSFER_DESCRIPTION = "Примечание к счету на наличку";
    public static final String CASH_TRANSFER_STATUS = "Активно";
    public static final String CASH_TRANSFER_OPERATION_DATE = "";
    public static final String CASH_TRANSFER_OPERATION_SUMMARY = "10000";

    //_______________________________________________________________________________


    public static final String CASH_TRANSFER_OFFICE_NEW = "Тестовый офис";
    public static final String CASH_TRANSFER_INCOME_FOR_DEAL_NEW = "Возврат по делу";
    public static final String CASH_TRANSFER_INCOME_FOR_ANOTHER_NEW = "Другой приход";
    public static final String CASH_TRANSFER_INCOME_FOR_MOVEMENT_NEW = "Заявка в другой офис - приход по делу";
    public static final String CASH_TRANSFER_INCOME_FOR_MOVEMENT_MONEY_NEW = "";
    public static final String CASH_TRANSFER_OPERATION_TYPE_NEW = "Наличный расход";
    public static final String CASH_TRANSFER_OPERATION_CODE_FOR_DEAL_NEW = "РД";
    public static final String CASH_TRANSFER_OPERATION_CODE_FOR_ANOTHER_NEW = "ПР";
    public static final String CASH_TRANSFER_OPERATION_CODE_FOR_MOVEMENT_NEW = "АГ";
    public static final String CASH_TRANSFER_OPERATION_CODE_FOR_MOVEMENT2_NEW = "РО";
    public static final String CASH_TRANSFER_DEAL_COST_NEW = "10000";
    public static final String CASH_TRANSFER_DEAL_PAID_NEW = "23500";
    public static final String CASH_TRANSFER_MOVEMENT_DATE_NEW = "01-09-2017";
    public static final String CASH_TRANSFER_CLIENT_NEW = "НПО Энергосталь";
    public static final String CASH_TRANSFER_PROVIDER_NEW = "Веста";
    public static final String CASH_TRANSFER_WORKER_NEW = "Kesha Parrot";
    public static final String CASH_TRANSFER_DESCRIPTION_NEW = "Примечание к счету на наличку";
    public static final String CASH_TRANSFER_STATUS_NEW = "Активно";
    public static final String CASH_TRANSFER_OPERATION_DATE_NEW = "";
    public static final String CASH_TRANSFER_OPERATION_SUMMARY_NEW = "10000";

    public static final String CASH_TRANSFER_CHARGE_NUMBER_LIST = "Сбербанк";
    public static final String CASH_TRANSFER_CHARGE_DATE_LIST = "Сбербанк";
    public static final String CASH_TRANSFER_CHARGE_SUM_LIST = "Сбербанк";

    public static final String CASH_TRANSFER_CHARGE_BANK = "Сбербанк";
    public static final String CASH_TRANSFER_CHARGE_DATE_PAYMENT = "29-06-2017";
    public static final String CASH_TRANSFER_CHARGE_SUM = "500";
    public static final String CASH_TRANSFER_CHARGE_COMMISSION = "2%";

    public static final String CASH_TRANSFER_SUMMARY = "3890";
    public static final String CASH_TRANSFER_CURRENCY = "$";

    public static final String CASH_TRANSFER_NAME = "Спонсорская помощь";

    //SALARY TRANSFER DATA

    public static final String SALARY_TRANSFER_DATE_FROM = "01-01-2017";
    public static final String SALARY_TRANSFER_DATE_TO = "31-12-2018";

    public static final String SALARY_TRANSFER_OFFICE = "Тестовый офис";
    public static final String SALARY_TRANSFER_INCOME = "Зарплата сотруднику";
    public static final String SALARY_TRANSFER_OPERATION_TYPE = "Наличный расход";
    public static final String SALARY_TRANSFER_OPERATION_CODE = "ЗПК";
    public static final String SALARY_TRANSFER_WORKER = "Supervisor Kesha";
    public static final String SALARY_TRANSFER_PERIOD = "2";
    public static final String SALARY_TRANSFER_SUMMARY = "9800";
    public static final String SALARY_TRANSFER_TAX = "1100";
    public static final String SALARY_TRANSFER_DESCRIPTION = "Пол ставки";
    public static final String SALARY_TRANSFER_OPERATION_SUM = "9800";

    //____________________________________________________________________________

    public static final String SALARY_TRANSFER_OFFICE_NEW = "Тестовый офис";
    public static final String SALARY_TRANSFER_INCOME_NEW = "Возврат от сотрудника";
    public static final String SALARY_TRANSFER_OPERATION_TYPE_NEW = "Наличный приход";
    public static final String SALARY_TRANSFER_OPERATION_CODE_NEW = "ШТ";
    public static final String SALARY_TRANSFER_WORKER_NEW = "Senior Andrey";
    public static final String SALARY_TRANSFER_PERIOD_NEW = "1";
    public static final String SALARY_TRANSFER_SUMMARY_NEW = "800";
    public static final String SALARY_TRANSFER_TAX_NEW = "80";
    public static final String SALARY_TRANSFER_DESCRIPTION_NEW = "Профсоюзный взнос";
    public static final String SALARY_TRANSFER_OPERATION_SUM_NEW = "800";

    //BANK DATA

    public static final String GROUP_BANK_NAME = "Test Group";
    public static final String BANK_BIC = "745235214";
    public static final String BANK_SWIFT = "30071004543";
    public static final String BANK_NAME = "Test Bank";
    public static final String BANK_CORR = "453235623623623";
    public static final String BANK_CITY = "Москва";
    public static final String BANK_PHONE = "79786547784";
    public static final String BANK_ADDRESS = "ул. Большая Морская, 52";
    public static final String BANK_STATUS_1 = "1";
    public static final String BANK_STATUS_0 = "0";
    public static final String BANK_STATUS_ACTIVE = "Активнo";
    public static final String BANK_STATUS_DELETED = "Удаленo";

    //_______________________________________________________________________________

    public static final String GROUP_BANK_NAME_NEW = "Test Group New";
    public static final String BANK_BIC_NEW = "7452352143253";
    public static final String BANK_SWIFT_NEW = "300710043253";
    public static final String BANK_NAME_NEW = "Test Bank New";
    public static final String BANK_CORR_NEW = "4532356211222222";
    public static final String BANK_COUNTRY_NEW = "Украина";
    public static final String BANK_PHONE_NEW = "797858932852300";
    public static final String BANK_ADDRESS_NEW = "пл. Ушакова, 52";

    //BANK ACCOUNT DATA

    public static final String BANK_ACCOUNT_CURRENCY = "RUB";
    public static final String BANK_ACCOUNT_CURRENCY_R = "R";
    public static final String BANK_ACCOUNT_NUMBER = "5236262623535623";
    public static final String BANK_ACCOUNT_CORR = "46346626246236";
    public static final String BANK_ACCOUNT_BANK = "Test Bank";
    public static final String BANK_ACCOUNT_OWNER_TYPE = "Организация";
    public static final String BANK_ACCOUNT_ITEM = "Тест СПб";

    //________________________________________________________________________________

    public static final String BANK_ACCOUNT_CURRENCY_NEW = "R";
    public static final String BANK_ACCOUNT_NUMBER_NEW = "57237230829358";
    public static final String BANK_ACCOUNT_CORR_NEW = "57238562356792";
    public static final String BANK_ACCOUNT_BANK_NEW = "Test Bank";
    public static final String BANK_ACCOUNT_OWNER_TYPE_NEW = "Организация";
    public static final String BANK_ACCOUNT_ITEM_NEW = "Тест СПб";

    //TASK DATA

    public static final String TASK_TIME_ADMIN = "2017-07-14";
    public static final String TASK_TYPE_ADMIN = "Заявки";
    public static final String TASK_TYPE_SENIOR = "Дела";
    public static final String TASK_TYPE_ACCOUNT_OFFICE = "Клиенты";
    public static final String TASK_STATUS_ADMIN = "Активная";
    public static final String TASK_NAME = "Тестовое задание";
    public static final String TASK_DESCRIPTION = "Описание тестового задания";

    public static final String TASK_DATE = "2017-11-12";
    public static final String TASK_TIME = "07:30";
    public static final String TASK_NAME_ADMIN = "Тестовое задание с заявок (Admin)";
    public static final String TASK_NAME_SENIOR = "Тестовое задание с заявок (Senior)";
    public static final String TASK_NAME_ACCOUNTANT_OFFICE = "Тестовое задание с заявок (Accountant)";
    public static final String TASK_DESCRIPTION_ADMIN = "Описание тестового задания с заявок (Admin)";
    public static final String TASK_DESCRIPTION_SENIOR = "Описание тестового задания с заявок (Senior)";
    public static final String TASK_DESCRIPTION_ACCOUNTANT_OFFICE = "Описание тестового задания с заявок (Accountant)";

    //PAYER DATA

    public static final String PAYER_CLIENT = "Client for payer";
    public static final String PAYER_INN = "123456780987";
    public static final String PAYER_KPP = "112233445566";
    public static final String PAYER_NAME = "Дружковкагорэлектротранс";
    public static final String PAYER_FULL_NAME = "КП 'Дружковский трамвай'";
    public static final String PAYER_CONTRAGENT_TYPE = "Юридическое лицо";
    public static final String PAYER_LEGAL_ADDRESS = "ул. Дружбы, 63";
    public static final String PAYER_REAL_ADDRESS = "ул. Дружбы, 63";

    public static final String PAYER_OSNUSN = "УСН";
    public static final String PAYER_COUNTRY = "Украина";
    public static final String PAYER_OKPO = "8578467684";
    public static final String PAYER_OGRN = "048547478";
    public static final String PAYER_TAXNUMBER = "14006";
    public static final String PAYER_DOCNUMBER = "169783";
    public static final String PAYER_DOC_DATE_RECEIVED = "2017-10-20";

    public static final String PAYER_DESCRIPTION = "Описание плательщика";
    public static final String PAYER_STATUS = "Активнo";

    public static final String PAYER_RESPPOSITION = "Менеджер";
    public static final String PAYER_RESPNAME = "Плательщик админа";
    public static final String PAYER_RESPDOC = "Документик";

    public static final String PAYER_INTEGRATION_INN = "7707083893";

    //CONTRACT DATA

    public static final String CONTRACT_LEGAL_NAME = "ООО \"Тест СПб\"";
    public static final String CONTRACT_NUMBER = "123321";
    public static final String CONTRACT_DATE = "2017-12-10";
    public static final String CONTRACT_VALIDITY = "1 год";
    public static final String CONTRACT_NAME = "Тестовый договор";
    public static final String CONTRACT_CURRENCY = "R";
    public static final String CONTRACT_TYPE_PAYER = "С покупателем";
    public static final String CONTRACT_TYPE_PROVIDER = "С поставщиком";

    public static final String CONTRACT_PAYER = "ул. Дружбы, 63";
    public static final String CONTRACT_PROVIDER = "ул. Дружбы, 63";

    public static final String CONTRACT_ORIGINAL = "У плательщика";
    public static final String CONTRACT_NOTE = "Заметка";
    public static final String CONTRACT_STATUS = "Активнo";

    //ORGANIZATION DATA

    public static final String ORGANIZATION_NAME = "Энергосталь";
    public static final String ORGANIZATION_FULL_NAME = "НПО Энергосталь";
    public static final String ORGANIZATION_CONTRAGENT_TYPE = "Обособленное подразделение";
    public static final String ORGANIZATION_LEGAL_ADDRESS = "ул. Бабатунде, 13";
    public static final String ORGANIZATION_REAL_ADDRESS = "ул. Бабатунде, 13";
    public static final String ORGANIZATION_PREFIX = "ЭН";
    public static final String ORGANIZATION_OSNUSN = "УСН";
    public static final String ORGANIZATION_COUNTRY = "Украина";
    public static final String ORGANIZATION_INN = "93799929379992";
    public static final String ORGANIZATION_KPP = "444";
    public static final String ORGANIZATION_OKPO = "ОКПО";
    public static final String ORGANIZATION_OGRN = "7489272889";
    public static final String ORGANIZATION_TAXNUMBER = "5439876";
    public static final String ORGANIZATION_DOCNUMBER = "НК57283572";
    public static final String ORGANIZATION_DOC_DATE_RECEIVED = "2017-12-01";
    public static final String ORGANIZATION_NOTE = "Некие примечания";
    public static final String ORGANIZATION_ACTIVITY_TYPE = "Продажи";
    public static final String ORGANIZATION_EXTRA_ACTIVITY_TYPE = "Аренды";
    public static final String ORGANIZATION_TAXFACE = "ФСКН";
    public static final String ORGANIZATION_DATETAX = "2017-12-01";

    public static final String ORGANIZATION_FOND = "Тестовый фонд";
    public static final String ORGANIZATION_FOND_NUMBER = "12232414";
    public static final String ORGANIZATION_FOND_DATE = "2022-12-01";

    public static final String ORGANIZATION_POZITION = "Генеральный директор";
    public static final String ORGANIZATION_RESPPERSON_NAME = "Николай Сидоров";
    public static final String ORGANIZATION_RESPPERSON_DOC = "Документ";

    public static final String ORGANIZATION_DESCR_ACTIVITY = "Экологический мониторинг";
    public static final String ORGANIZATION_RELATED_COMPANIES = "НПО Инкор";
    public static final String ORGANIZATION_RELATED_COMPANIES_BY_LEADER = "Таковые отсутствуют";

    public static final String ORGANIZATION_STATUS = "Активнo";

    //CLOSE DOCUMENT DATA

    public static final String CLOSE_DOCUMENT_ACT_NUMBER = "53412";
    public static final String CLOSE_DOCUMENT_ACT_DATE = "29-12-2022";
    public static final String CLOSE_DOCUMENT_NDS_SUM = "";
    public static final String CLOSE_DOCUMENT_TOTAL_SUM = "";
    public static final String CLOSE_DOCUMENT_ACT_INVOICE_NUMBER = "901111109";
    public static final String CLOSE_DOCUMENT_DATE_INVOICE_NUMBER = "28-12-2022";
    public static final String CLOSE_DOCUMENT_COMMENT = "Comment";

    public static final String CLOSE_DOCUMENT_RESPONSIBLE_PERSON = "Администратор";
    public static final String CLOSE_DOCUMENT_OFFICE = "Тестовый офис";
    public static final String CLOSE_DOCUMENT_CLIENT_NAME = "Client for close document";
    public static final String CLOSE_DOCUMENT_DEAL_NUMBER_EXPENSE = "CLSD1";
    public static final String CLOSE_DOCUMENT_DEAL_NUMBER_INCOME = "CLSD2";

    //ADDRESS DATA

    public static final String ADDRESS_COUNTRY = "Россия";
    public static final String ADDRESS_ZIP = "123456";
    public static final String ADDRESS_CITY = "Москва";
    public static final String ADDRESS_STREET = "ул. Южная";
    public static final String ADDRESS_HOUSE_TYPE = "Домовладение";
    public static final String ADDRESS_HOUSE_NUMBER = "7";
    public static final String ADDRESS_BUILDING_TYPE = "Корпус";
    public static final String ADDRESS_BUILDING_NUMBER = "2а";
    public static final String ADDRESS_APARTMENT_TYPE = "Офис";
    public static final String ADDRESS_APARTMENT_NUMBER = "203";
    public static final String ADDRESS_COMMENT = "Комментарий к адресу";

    //FREE REQUEST DATA

    public static final String FREE_REQUEST_OFFICE = "Тестовый офис";

    public static final String FREE_REQUEST_COUNTRY = "Россия";
    public static final String FREE_REQUEST_REGION = "Москва и Московская обл.";
    public static final String FREE_REQUEST_CITY = "Москва";
    public static final String FREE_REQUEST_ADDRESS = "ул. Алчевских, 5";
    public static final String FREE_REQUEST_SITE = "admin.ru";

    public static final String FREE_REQUEST_RESPONSIBLE_PERSON = "Администратор";

    public static final String FREE_REQUEST_DESCRIPTION = "Тестовые комментарии к новому клиенту";
    public static final String FREE_REQUEST_DIRECTION = "аттракционы";
    public static final String FREE_REQUEST_STATUS = "Активный";

    public static final String FREE_REQUEST_STATUS_CAN_NOT_REPRODUCE = "Сделать не можем";
    public static final String FREE_REQUEST_STATUS_IMPOSSIBLE = "Выполнить невозможно";
    public static final String FREE_REQUEST_STATUS_DEAD = "Мертвая";

    //--ADMIN
    public static final String FREE_REQUEST_CLIENT_ADMIN = "Client For Free Request (Admin)";

    public static final String FREE_REQUEST_NAME_ADMIN = "Client name Admin";
    public static final String FREE_REQUEST_EMAIL_ADMIN = "admin@name.com";
    public static final String FREE_REQUEST_PHONE_ADMIN = "79873267666";

    //--SUPERVISOR
    public static final String FREE_REQUEST_CLIENT_SUPERVISOR = "Client For Free Request (Supervisor)";

    public static final String FREE_REQUEST_NAME_SUPERVISOR = "Client name Supervisor";
    public static final String FREE_REQUEST_EMAIL_SUPERVISOR = "admin@name.com";
    public static final String FREE_REQUEST_PHONE_SUPERVISOR = "79873267666";

    //--SENIOR
    public static final String FREE_REQUEST_CLIENT_SENIOR = "Client For Free Request (Senior)";

    public static final String FREE_REQUEST_NAME_SENIOR = "Client name Senior";
    public static final String FREE_REQUEST_EMAIL_SENIOR = "admin@name.com";
    public static final String FREE_REQUEST_PHONE_SENIOR = "79873267666";

    //--MANAGER
    public static final String FREE_REQUEST_CLIENT_MANAGER = "Client For Free Request (Manager)";

    public static final String FREE_REQUEST_NAME_MANAGER = "Client name Manager";
    public static final String FREE_REQUEST_EMAIL_MANAGER = "admin@name.com";
    public static final String FREE_REQUEST_PHONE_MANAGER = "79873267666";


    //LAYOUT TEST DATA
    public static final String LAYOUT_NAME_INVOICE_FOR_PAYMENT = "Test Layout (invoice for payment)";
    public static final String LAYOUT_NAME_INVOICE = "Test Layout (invoice)";
    public static final String LAYOUT_NAME_ACT = "Test Layout (act)";
    public static final String LAYOUT_ENTITY_TYPE_BANK = "Приходы и расходы банк";
    public static final String LAYOUT_ENTITY_TYPE_CLOSE_DOC = "Закрывающие документы";
    public static final String LAYOUT_TYPE_INVOICE = "Счет-фактура";
    public static final String LAYOUT_TYPE_ACT = "Акт";
    public static final String LAYOUT_TYPE_INVOICE_FOR_PAYMENT = "Счет на оплату";
    public static final String LAYOUT_STATUS = "Активный";

    //LAYOUT ADDITIONAL DATA
    public static final String CLIENT_FOR_LAYOUT = "Клиент для макета";
    public static final String PAYER_FOR_LAYOUT = "Плательщик для макета";
    public static final String PROVIDER_FOR_LAYOUT = "Поставщик для макета";
    public static final String PROVIDER_LEGAL_FOR_LAYOUT = "ООО \"Для макета\"";


    //DB CONNECTION

    public static final String PROPERTY_PATH = "src/main/resources/dataConfig.properties";
    public static final String MYSQL_DRIVER = "mysqlDriver";
    public static final String CONNECTION_TO_DB = "connectionToDB";
    public static final String LOGIN = "login";
    public static final String PASSWORD = "password";

    public static final String SELECT_QUERY = "select * from employees where firstname = ?";
}
