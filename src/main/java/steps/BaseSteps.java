package steps;

import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;

import java.util.ArrayList;
import java.util.List;

public abstract class BaseSteps {

    WebDriver driver;

    public BaseSteps (WebDriver driver) {
        this.driver = driver;
    }

    public WebElement getElementByXpath(final String xpath) {
        return driver.findElement(By.xpath(xpath));
    }

    public WebElement getElementByID(final String id) {
        return driver.findElement(By.id(id));
    }

    public void clearAndType(final WebElement element, final String text) {
        element.clear();
        element.sendKeys(text);
    }

    public void click(WebElement element) {
        element.click();
    }

    public void pressEnter(WebElement element) {
        element.sendKeys(Keys.ENTER);
    }

    public void moveToElement(final WebElement element) {
        Actions actions = new Actions(driver);
        actions.moveToElement(element).build().perform();
    }

    public void acceptAlert() throws InterruptedException {
        try {
            Alert alert = driver.switchTo().alert();
            alert.accept();
        } catch (Exception e) {
            Thread.sleep(2000);
            click(getElementByXpath("//div[@class='modal-dialog']//button[2]"));
            Thread.sleep(1000);
        }
    }

    /*public void uploadFile(String path, String type) {
        WebElement fileInput = driver.findElement(By.xpath(path));
        if (type.equals("image")){
            fileInput.sendKeys("C:\\Users\\Admin\\IdeaProjects\\CRM\\image.jpg");
        } else {
            fileInput.sendKeys("C:\\Users\\Admin\\IdeaProjects\\CRM\\document.docx");
        }
    }*/

    public String getTextValue(String path) {
        return getElementByXpath(path).getText();
    }

    public void scrollToTop(String value) {
        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, " + value + ")");
    }

    public void scrollToBot(String value) {
        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, " + value + ")");
    }

    public static ArrayList<String> getArrayOfValues(ArrayList<String> list, List<WebElement> webElements) throws InterruptedException {
        //Thread.sleep(500);
        for (WebElement elem : webElements) {
            //Thread.sleep(500);
            list.add(elem.getText());
            //Thread.sleep(500);
        }
        return list;
    }

    public static double getIncome(String assignedCashSum, String assignedNoCashSum, String paidCashSum, String paidNoCashSum,
                                   String agentCashSum, String agentNoCashSum, String costCashSum, String costNoCashSum,
                                   String officeRatio, String transfer, String taxpercent, String ratio) {
        double assCash = Double.parseDouble(assignedCashSum);
        double assNoCash = Double.parseDouble(assignedNoCashSum);
        double paidCash = Double.parseDouble(paidCashSum);
        double paidNoCash = Double.parseDouble(paidNoCashSum);
        double agentCash = Double.parseDouble(agentCashSum);
        double agentNoCash = Double.parseDouble(agentNoCashSum);
        double costCash = Double.parseDouble(costCashSum);
        double costNoCash = Double.parseDouble(costNoCashSum);
        double officeK = Double.parseDouble(officeRatio);
        double transferK = Double.parseDouble(transfer);
        double taxK = Double.parseDouble(taxpercent);
        double dealK = Double.parseDouble(ratio);

        System.out.println(paidCash + "    " + paidNoCash + "    " + agentCash + "    " + agentNoCash + "    " + costCash + "    " + costNoCash + "    " +
                officeK + "    " + transferK + "    " + taxK + "    " + dealK);

        return (paidCash - (costCash+agentCash) + (paidNoCash-costNoCash-agentNoCash)*officeK - (costNoCash+agentNoCash)*transferK - paidNoCash*taxK)*dealK;
    }

    public List<WebElement> getListOfElementsByXpath(final String xpath) {
        return driver.findElements(By.xpath(xpath));
    }
}
