package steps;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.CookieStore;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.conn.ssl.X509HostnameVerifier;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.client.LaxRedirectStrategy;
import org.apache.http.impl.conn.SingleClientConnManager;
import org.apache.http.message.BasicNameValuePair;
import org.openqa.selenium.Cookie;
import org.openqa.selenium.WebDriver;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import java.util.logging.Logger;
import static utils.Constants.*;

public class MainPageSteps extends BaseSteps {
    WebDriver driver;

    protected String cashValue = "";

    public MainPageSteps(WebDriver driver) {
        super(driver);
        this.driver = driver;
    }

    private String getHash(String email, String role, String authKey) {
        String stringForHash = "";

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy|MM|dd/HH-mm");
        dateFormat.setTimeZone(TimeZone.getTimeZone("GMT+3"));
        String[] dateTime = dateFormat.format(new Date()).split("/");
        //System.out.println("date: " + dateTime[0]);
        //System.out.println("time: " + dateTime[1]);

        stringForHash = email + "--" + dateTime[0] + "--" + role + "--" + dateTime[1] + "--" + authKey;
        //System.out.println(stringForHash);
        String sha256hex = org.apache.commons.codec.digest.DigestUtils.sha256Hex(stringForHash);
        //System.out.println(sha256hex);

        return sha256hex;
    }

    public void login(String email, String role, String authKey) throws IOException, InterruptedException {
        String hash = getHash(email, role, authKey);
        CookieStore cookieStore = null;

        HostnameVerifier hostnameVerifier = org.apache.http.conn.ssl.SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER;
        DefaultHttpClient client = new DefaultHttpClient();

        SchemeRegistry registry = new SchemeRegistry();
        SSLSocketFactory socketFactory = SSLSocketFactory.getSocketFactory();
        socketFactory.setHostnameVerifier((X509HostnameVerifier) hostnameVerifier);
        registry.register(new Scheme("https", socketFactory, 443));
        SingleClientConnManager mgr = new SingleClientConnManager(client.getParams(), registry);
        DefaultHttpClient httpClient = new DefaultHttpClient(mgr, client.getParams());

        // Set verifier
        HttpsURLConnection.setDefaultHostnameVerifier(hostnameVerifier);

        httpClient.setRedirectStrategy(new LaxRedirectStrategy());
        HttpPost httppost = new HttpPost(CRM_BASE_URL);

        // Request parameters and other properties.
        List<NameValuePair> params = new ArrayList<NameValuePair>(3);
        params.add(new BasicNameValuePair("email", email));
        params.add(new BasicNameValuePair("role", role));
        params.add(new BasicNameValuePair("hash", hash));
        httppost.setEntity(new UrlEncodedFormEntity(params, "UTF-8"));

        //Execute and get the response.
        HttpResponse response = httpClient.execute(httppost);
        HttpEntity entity = response.getEntity();
        //System.out.println(response.getStatusLine());
        if (entity != null) {
            InputStream instream = entity.getContent();
            try {
                // do something useful
            } finally {
                instream.close();
            }
        }

        cookieStore = httpClient.getCookieStore();
        //Выведем в консоль имена cookie с их значениями
        List<org.apache.http.cookie.Cookie> cookieList = cookieStore.getCookies();
        if(!cookieList.isEmpty()) {
            for(org.apache.http.cookie.Cookie cookie : cookieList) {
                //System.out.println(cookie.getName() + " = " +  cookie.getValue());
            }
        }
        Cookie cookie1 = new Cookie(cookieList.get(0).getName(), cookieList.get(0).getValue());
        Cookie cookie2 = new Cookie(cookieList.get(1).getName(), cookieList.get(1).getValue());
        driver.get("https://crm.stage.gortest.ru/site/login");
        driver.manage().addCookie(cookie2);
        driver.manage().addCookie(cookie1);
        driver.get("https://crm.stage.gortest.ru/");
    }

    private static final Logger log = Logger.getLogger(String.valueOf(MainPageSteps.class));

    public void loginByAdmin() throws IOException, InterruptedException {
        login(ADMINISTRATOR_EMAIL, ADMINISTRATOR_ROLE, ADMINISTRATOR_AUTH_KEY);
        log.info("I try login with this role: " + ADMINISTRATOR_ROLE);
    }

    public void loginBySupervisor() throws IOException, InterruptedException {
        login(SUPERVISOR_EMAIL, SUPERVISOR_ROLE, SUPERVISOR_AUTH_KEY);
        log.info("I try login with this role: " + SUPERVISOR_ROLE);
    }

    public void loginByMarketer() throws IOException, InterruptedException {
        login(MARKETER_EMAIL, MARKETER_ROLE, MARKETER_AUTH_KEY);
        log.info("I try login with this role: " + MARKETER_ROLE);
    }

    public void loginByAccountFinDep() throws IOException, InterruptedException {
        login(ACCOUNT_DEP_EMAIL, ACCOUNT_DEP_ROLE, ACCOUNT_DEP_AUTH_KEY);
        log.info("I try login with this role: " + ACCOUNT_DEP_ROLE);
    }

    public void loginByAccountOffice() throws IOException, InterruptedException {
        login(ACCOUNT_OFFICE_EMAIL, ACCOUNT_OFFICE_ROLE, ACCOUNT_OFFICE_AUTH_KEY);
        log.info("I try login with this role: " + ACCOUNT_OFFICE_ROLE);
    }

    public void loginBySenior() throws IOException, InterruptedException {
        login(SENIOR_MANAGER_EMAIL, SENIOR_MANAGER_ROLE, SENIOR_MANAGER_AUTH_KEY);
        log.info("I try login with this role: " + SENIOR_MANAGER_ROLE);
    }

    public void loginByManager() throws IOException, InterruptedException {
        login(MANAGER_EMAIL, MANAGER_ROLE, MANAGER_AUTH_KEY);
        log.info("I try login with this role: " + MANAGER_ROLE);
    }

    //@Step("Open client page")
    public void openClientPage() {
        click(getElementByXpath("//*[@id='main-nav-menu']/li/a[contains(.,'Клиенты')]"));
    }

    //@Step("Open client page")
    public void openUserPage() {
        click(getElementByXpath("//*[@id='main-nav-menu']/li/a[contains(.,'Пользователи')]"));
    }

    //@Step("Open office page")
    public void openOfficePage() {
        click(getElementByXpath("//*[@id='main-nav-menu']/li/a[contains(.,'Офисы')]"));
    }

    //@Step("Open provider page")
    public void openProviderPage() {
        click(getElementByXpath("//*[@id='main-nav-menu']/li/a[contains(.,'Поставщики')]"));
    }

    //@Step("Open request page")
    public void openRequestPage() {
        click(getElementByXpath("//*[@id='main-nav-menu']/li/a[contains(.,'Заявки')]"));
    }

    //@Step("Open request page")
    public void openFreeRequestPage() {
        click(getElementByXpath("//*[@id='main-nav-menu']/li/a[contains(.,'Свободные заявки')]"));
    }

    //@Step("Open deal page")
    public void openDealPage() {
        click(getElementByXpath("//*[@id='main-nav-menu']/li/a[contains(.,'Дела')]"));
    }

    //@Step("Open bank transfer page")
    public void openBankTransferPage() {
        click(getElementByXpath("//*[@id='main-nav-menu']/li/a[contains(.,'Приходы и расходы банк')]"));
    }

    //@Step("Open card transfer page")
    public void openCardTransferPage() throws InterruptedException {
        click(getElementByXpath("//*[@id='main-nav-menu']/li/a[contains(.,'Приходы и расходы карта')]"));
    }

    //@Step("Open cash transfer page")
    public void openCashTransferPage() {
        click(getElementByXpath("//*[@id='main-nav-menu']/li/a[contains(.,'Приходы и расходы cash')]"));
    }

    //@Step("Open salary transfer page")
    public void openSalaryTransferPage() {
        click(getElementByXpath("//*[@id='main-nav-menu']/li/a[contains(.,'Зарплата')]"));
    }

    //@Step("Open report transfer page")
    public void openReportPage() {
        click(getElementByXpath("//*[@id='main-nav-menu']/li/a[contains(.,'Отчеты')]"));
    }

    //@Step("Open payer page")
    public void openPayerPage() {
        click(getElementByXpath("//*[@id='main-nav-menu']/li/a[contains(.,'Плательщики')]"));
    }

    //@Step("Open close document page")
    public void openCloseDocumentPage() {
        click(getElementByXpath("//*[@id='main-nav-menu']//a[contains(.,'Закрывающие документы')]"));
    }

    //@Step("Open bank account page")
    public void openBankAccountPage() {
        moveToElement(getElementByXpath("//a[@title='Справочники']"));
        click(getElementByXpath("//a[@title='Расчетные счета']"));
    }

    //@Step("Open Task page")
    public void openTaskPage() {
        click(getElementByXpath("//a[contains(.,'Задачи')]"));
    }

    //@Step("Open bank page")
    public void openBankPage() {
        moveToElement(getElementByXpath("//a[@title='Справочники']"));
        click(getElementByXpath("//a[@title='Справочник банков']"));
    }

    //@Step("Open contract page")
    public void openContractPage() {
        moveToElement(getElementByXpath("//a[@title='Справочники']"));
        click(getElementByXpath("//a[@title='Договора']"));
    }

    //@Step("Open organization page")
    public void openOrganizationPage() {
        moveToElement(getElementByXpath("//a[@title='Справочники']"));
        click(getElementByXpath("//a[@title='Организации']"));
    }

    //@Step("Open layouts page")
    public void openLayoutsPage() {
        moveToElement(getElementByXpath("//ul[@id='main-nav-menu']/li/a[@href='/setting']"));
        click(getElementByXpath("//a[@title='Макеты печатных форм']"));
    }
}
