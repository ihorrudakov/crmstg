package steps;

import org.openqa.selenium.WebDriver;

import static org.testng.Assert.assertEquals;

public class ReportPageSteps extends BaseSteps {
    WebDriver driver;

    public ReportPageSteps(WebDriver driver) {
        super(driver);
        this.driver = driver;
    }

    public void openByOfficeDealTab() {
        click(getElementByXpath("//a[contains(.,'По делам офиса')]"));
    }

    public void clickSearchButton() {
        click(getElementByXpath("//button[contains(.,'Поиск')]"));
    }

    public void selectLeadFilters(String type, String format, String clientType) {
        click(getElementByID("plotform-type"));
        click(getElementByXpath("//*[@id='plotform-type']//option[contains(.,'" + type + "')]"));

        click(getElementByID("plotform-format"));
        click(getElementByXpath("//*[@id='plotform-format']//option[contains(.,'" + format + "')]"));

        click(getElementByID("plotform-clienttype"));
        click(getElementByXpath("//*[@id='plotform-clienttype']//option[contains(.,'" + clientType + "')]"));
    }

    public void clickBuildButton() {
        click(getElementByXpath("//button[contains(.,'Построить')]"));
    }

    public void checkChart() throws InterruptedException {
        Thread.sleep(5000);
        assertEquals(true, getElementByXpath("//div[@id='chart_div']").isDisplayed());
    }

    public void openLeadTab() {
        click(getElementByXpath("//a[contains(.,'Анализ заявок')]"));
    }

    public void openOfficeReportTab() {
        click(getElementByXpath("//a[contains(.,'Отчет офиса продаж')]"));
    }
}
