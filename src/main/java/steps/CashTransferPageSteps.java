package steps;

import org.openqa.selenium.WebDriver;

import static utils.Constants.SALARY_TRANSFER_SUMMARY;

public class CashTransferPageSteps extends BaseSteps {
    WebDriver driver;
    public CashTransferPageSteps(WebDriver driver) {
        super(driver);
        this.driver = driver;
    }

    //@Step("Click 'Create card transfer link by deal' button")
    public void clickCreateCashTransferLinkByDealButton() throws InterruptedException {
        click(getElementByID("create-cash-transfer-link-by-deal"));
        Thread.sleep(1000);
    }

    //@Step("Enter data into 'Create cash transfer link by deal' form")
    public void enterDataIntoCreateCashTransferLinkByDeal(String office, String incomeExpenses, String operationType, String operationCode, String dealNumber, String dealCost,
                                                          String dealPaid, String client, String provider, String worker, String description, String status, String operationDate,
                                                          String operationSummary, String role) throws InterruptedException {
        Thread.sleep(3000);
        if (role.equals("Admin") || role.equals("Supervisor")) {
            click(getElementByID("cashtransfermodel-officeid"));
            click(getElementByXpath("//*[@id='cashtransfermodel-officeid']/option[contains(.,'" + office + "')]"));
            Thread.sleep(3000);
        }

        click(getElementByID("cashtransfermodel-incomeexpenses"));
        click(getElementByXpath("//*[@id='cashtransfermodel-incomeexpenses']/option[contains(.,'" + incomeExpenses + "')]"));
        Thread.sleep(2000);
        click(getElementByID("cashtransfermodel-operationtype"));
        click(getElementByXpath("//*[@id='cashtransfermodel-operationtype']/option[contains(.,'" + operationType + "')]"));
        Thread.sleep(2000);
        click(getElementByID("cashtransfermodel-operationcode"));
        click(getElementByXpath("//*[@id='cashtransfermodel-operationcode']/option[contains(.,'" + operationCode + "')]"));

        moveToElement(getElementByXpath("//*[@id='container-cashtransfer-deal-']/div/table/tbody/tr/td[1]/div[2]/span/span[1]/span"));
        Thread.sleep(2000);
        click(getElementByXpath("//*[@id='container-cashtransfer-deal-']/div/table/tbody/tr/td[1]/div[2]/span/span[1]/span"));
        Thread.sleep(2000);
        clearAndType(getElementByXpath("html/body/span/span/span[1]/input"), dealNumber);
        Thread.sleep(2000);
        click(getElementByXpath("html/body/span/span/span[2]//li[contains(.,'" + dealNumber + "')]"));
            //clearAndType(getElementByID("cashtransferdealmodel-0-dealid"), dealNumber);
            Thread.sleep(2000);
            clearAndType(getElementByID("cashtransferdealmodel-0-dealcost"), dealCost);
            Thread.sleep(2000);
            clearAndType(getElementByID("cashtransferdealmodel-0-dealpaid"), dealPaid);
            Thread.sleep(2000);


        /*click(getElementByXpath("//*[@id='cashtransfer-form-']/div[3]/div[3]/div/div[3]/div[1]/span/span[1]/span"));
        clearAndType(getElementByXpath("html/body/span/span/span[1]/input"), worker);
        click(getElementByXpath("html/body/span/span/span[2]//li[contains(.,'" + worker + "')]"));*/
        click(getElementByXpath("//span[contains(@aria-labelledby, 'cashtransfermodel-clientid')]"));
        clearAndType(getElementByXpath("html/body/span/span/span[1]/input"), client);
        click(getElementByXpath("html/body/span/span/span[2]//li[contains(.,'" + client + "')]"));
        Thread.sleep(2000);
        click(getElementByXpath("//span[contains(@aria-labelledby, 'cashtransfermodel-providerid')]"));
        clearAndType(getElementByXpath("html/body/span/span/span[1]/input"), provider);
        click(getElementByXpath("html/body/span/span/span[2]//li[contains(.,'" + provider + "')]"));
        Thread.sleep(2000);
        Thread.sleep(2000);
        clearAndType(getElementByID("cashtransfermodel-description"), description);
        Thread.sleep(2000);
        //click(getElementByID("link-operation-cashTransactionId-"));
        /*Thread.sleep(2000);
        clearAndType(getElementByID("cashtransfermodel-operationdate"), operationDate);*/
        Thread.sleep(2000);
        //clearAndType(getElementByID("cashtransfermodel-operationsum-cash_by_deal"), operationSummary);
        Thread.sleep(2000);
        //uploadFile("//*[@id='cashtransfermodel-attachment-']", "doc");
    }

    //@Step("Click save button")
    public void clickSaveButton() throws InterruptedException {
        Thread.sleep(1000);
        click(getElementByXpath("//*[@id='cashtransfer-form-']//button[contains(.,'Сохранить')]"));
        Thread.sleep(2000);
    }

    //@Step("Click delete button")
    public void clickDeleteButton() throws InterruptedException {
        Thread.sleep(1000);
        moveToElement(getElementByXpath("//tr[1]//a[@title='удалить']"));
        click(getElementByXpath("//tr[1]//a[@title='удалить']"));
        Thread.sleep(2000);
        acceptAlert();
    }

    int startVal = 0;
    int endVal = 0;
    int startValMove = 0;
    int endValMove = 0;

    public void enterGeneralSearchQuery(String query) {
        clearAndType(getElementByXpath("//input[@id='all-search-input-visuals']"), query);
    }

    public void clickGeneralSearchButton() {
        click(getElementByXpath("//p[@class='search pull-left']/a"));
    }
}
