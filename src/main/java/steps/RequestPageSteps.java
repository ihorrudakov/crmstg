package steps;

import org.openqa.selenium.WebDriver;

import static org.testng.Assert.assertEquals;

public class RequestPageSteps extends BaseSteps {
    WebDriver driver;

    public RequestPageSteps(WebDriver driver) {
        super(driver);
        this.driver = driver;
    }

    //@Step("Click search button")
    public void clickSearchButton() throws InterruptedException {
        click(getElementByID("pagination-refresh"));
        Thread.sleep(3000);
    }

    //@Step("Click general search button")
    public void clickGeneralSearchButton() {
        click(getElementByXpath("//p[@class='search']//a[@onclick]"));
    }

    //@Step("Enter data into simple search field")
    public void enterDataIntoSimpleSearchField(String query) throws InterruptedException {
        clearAndType(getElementByID("all-search-input-visual"), query);
        Thread.sleep(1000);
    }

    //@Step("Click create request button")
    public void clickCreateRequestButton() {
        click(getElementByID("create-lead-link"));
    }

    //@Step("Click open extended filters link")
    public void clickOpenFilters() {
        click(getElementByXpath("//*[@id='wrapper']//span[@class='show-search-filter']"));
    }

    //@Step("Enter values")
    public void enterValues(String date, String responsiblePerson, String requestText, String office, String client, String country, String region, String city,
                            String status, String type, String site, String detailsByForm, String replyText, String notification,
                            String comments, String rejectionReason, String name, String phone, String email, String dob, String notes, String direction, String filter) throws InterruptedException {
        clearAndType(getElementByID("leadmodel-clientname"), client);

        click(getElementByID("leadmodel-status"));
        click(getElementByXpath("//*[@id='leadmodel-status']/option[contains(.,'"+ status +"')]"));

        if (filter.equals("Admin") || filter.equals("Supervisor")){
            click(getElementByID("leadmodel-officeid"));
            click(getElementByXpath("//*[@id='leadmodel-officeid']/option[contains(.,'" + office + "')]"));
        }

        if (filter.equals("Admin") || filter.equals("Supervisor")) {
            click(getElementByXpath("//span[@aria-labelledby='select2-leadmodel-userid-container']"));
            Thread.sleep(1000);
            clearAndType(getElementByXpath("html/body/span/span/span[1]/input"), responsiblePerson);
            Thread.sleep(2000);
            click(getElementByXpath("html/body/span/span/span[2]//li[contains(.,'" + responsiblePerson + "')]"));
        }
        Thread.sleep(5000);
        pressEnter(getElementByXpath("//input[contains(@name, 'ClientContactModel[0][name]')]"));
        Thread.sleep(3000);
        clearAndType(getElementByXpath("//input[contains(@name, 'ClientContactModel[0][name]')]"), name);
        clearAndType(getElementByXpath("//input[contains(@name, 'ClientContactModel[0][email]')]"), email);
        clearAndType(getElementByXpath("//input[contains(@name, 'ClientContactModel[0][phone]')]"), phone);
        clearAndType(getElementByXpath("//input[contains(@name, 'ClientContactModel[0][additional]')]"), dob);
        clearAndType(getElementByXpath("//input[contains(@name, 'ClientContactModel[0][notes]')]"), notes);
        click(getElementByID("clientcontactmodel-0-isbyphone"));
        click(getElementByID("clientcontactmodel-0-isbyemail"));

        click(getElementByXpath("//*[@id='client-change']/div[2]/span[2]/span[1]/span/ul"));
        Thread.sleep(1000);
        clearAndType(getElementByXpath("//*[@id='client-change']/div[2]/span[2]/span[1]/span/ul/li/input"), direction);
        Thread.sleep(2000);
        click(getElementByXpath("html/body/span/span/span[2]/ul/li[contains(.,'" + direction + "')]"));

        clearAndType(getElementByID("leadmodel-requesttext"), requestText);

        click(getElementByID("leadmodel-type"));
        click(getElementByXpath("//*[@id='leadmodel-type']/option[contains(.,'" + type + "')]"));

        clearAndType(getElementByID("leadmodel-siteurl"), site);

        clearAndType(getElementByID("leadmodel-detailsbyform"), detailsByForm);

        click(getElementByID("countryChange"));

        click(getElementByXpath("//span[@aria-labelledby='select2-leadmodel-cityid-container']"));
        Thread.sleep(1000);
        clearAndType(getElementByXpath("html/body/span/span/span[1]/input"), city);
        Thread.sleep(2000);
        click(getElementByXpath("html/body/span/span/span[2]//li[contains(.,'" + city + "')]"));

        click(getElementByXpath("//span[@aria-labelledby='select2-leadmodel-countryid-container']"));
        Thread.sleep(1000);
        clearAndType(getElementByXpath("html/body/span/span/span[1]/input"), country);
        Thread.sleep(2000);
        click(getElementByXpath("html/body/span/span/span[2]//li[contains(.,'" + country + "')]"));

        click(getElementByXpath("//span[@aria-labelledby='select2-leadmodel-regionid-container']"));
        Thread.sleep(1000);
        clearAndType(getElementByXpath("html/body/span/span/span[1]/input"), region);
        Thread.sleep(2000);
        click(getElementByXpath("html/body/span/span/span[2]//li[contains(.,'" + region + "')]"));

        clearAndType(getElementByID("leadmodel-leaddate"), date);

        clearAndType(getElementByID("leadmodel-replytext"), replyText);

        clearAndType(getElementByID("leadmodel-notification"), notification);

        clearAndType(getElementByID("leadmodel-comments"), comments);

        if (status.equals("Отказ")) {
            click(getElementByID("leadmodel-rejectiontype"));
            click(getElementByXpath("//*[@id='leadmodel-rejectiontype']/option[contains(.,'срок')]"));

            clearAndType(getElementByID("leadmodel-rejectionreason"), rejectionReason);
        }
    }

    //@Step("Click save request button")
    public void clickSaveButton() throws InterruptedException {
        click(getElementByXpath("//button[contains(.,'Сохранить')]"));
        Thread.sleep(2000);
    }

    //@Step("Check that request is found")
    public void checkRequest(String date, String office, String status, String responciblePerson, String type,
                             String site, String detailsByForm, String country, String region, String city, String client,
                             String direction, String text, String replyText, String comments, String notification, String rejectionReason,
                             String filter) {

        //moveToElement(getElementByXpath("//*[@id='wrapper']/div[2]/div/div/div[2]/div/div[3]/div[2]"));
        if (filter.equals("Admin") || filter.equals("Supervisor")) {
            assertEquals(office, getElementByXpath("//tr[1]/td[@data-office]").getText());
        }
    }

    //@Step("Delete created request")
    public void deleteCreatedRequest() throws InterruptedException {
        Thread.sleep(3000);
        moveToElement(getElementByXpath("//tr[1]//a[@title='удалить']"));
        click(getElementByXpath("//tr[1]//a[@title='удалить']"));
        acceptAlert();
    }

    //@Step("Click create deal button")
    public void clickCreateDealButton() throws InterruptedException {
        Thread.sleep(3000);
        moveToElement(getElementByXpath("//tr[1]//a[@title='открыть дело']"));
        click(getElementByXpath("//tr[1]//a[@title='открыть дело']"));
    }
}
