package steps;

import org.openqa.selenium.WebDriver;

public class CardTransferPageSteps extends BaseSteps {
    WebDriver driver;
    public CardTransferPageSteps(WebDriver driver) {
        super(driver);
        this.driver = driver;
    }

    //@Step("Enter data into search fields")
    public void enterDataIntoSearchFields(String type, String office, String dateFrom, String dateTo, String incomeExpence, String operationType,
                                          String operationCode, String summary, String billStatus, String status,
                                          String dealNumber, String client, String provider,
                                          String description, String infoName) throws InterruptedException {

        Thread.sleep(2000);
        click(getElementByID("cardtransfersearchmodel-typeid"));
        click(getElementByXpath("//*[@id='cardtransfersearchmodel-typeid']/option[contains(.,'" + type + "')]"));

        click(getElementByID("cardtransfersearchmodel-officeid"));
        click(getElementByXpath("//*[@id='cardtransfersearchmodel-officeid']/option[contains(.,'" + office + "')]"));

        clearAndType(getElementByID("cardtransfersearchmodel-createdatfrom"), dateFrom);

        clearAndType(getElementByID("cardtransfersearchmodel-createdatto"), dateTo);

        click(getElementByID("cardtransfersearchmodel-incomeexpensesid"));
        click(getElementByXpath("//*[@id='cardtransfersearchmodel-incomeexpensesid']/option[contains(.,'" + incomeExpence + "')]"));

        click(getElementByID("cardtransfersearchmodel-operationtypeid"));
        click(getElementByXpath(".//*[@id='cardtransfersearchmodel-operationtypeid']/option[contains(.,'" + operationType + "')]"));

        click(getElementByID("cardtransfersearchmodel-operationcodeid"));
        click(getElementByXpath("//*[@id='cardtransfersearchmodel-operationcodeid']/option[contains(.,'" + operationCode + "')]"));

        clearAndType(getElementByID("cardtransfersearchmodel-summary"), summary);

        click(getElementByID("cardtransfersearchmodel-statusbillid"));
        click(getElementByXpath("//*[@id='cardtransfersearchmodel-statusbillid']/option[contains(.,'" + billStatus + "')]"));

        click(getElementByID("cardtransfersearchmodel-status"));
        click(getElementByXpath("//*[@id='cardtransfersearchmodel-status']/option[@value=1]"));

        clearAndType(getElementByID("cardtransfersearchmodel-dealserialnumber"), dealNumber);

        /*click(getElementByID("cardtransfersearchmodel-clientid"));
        click(getElementByXpath("//*[@id='cardtransfersearchmodel-clientid']/option[contains(.,'" + client + "')]"));*/

        /*click(getElementByXpath("//*[@id='custom-filter-form']/div[3]/div[2]/div/span/span[1]/span"));
        clearAndType(getElementByXpath("html/body/span/span/span[1]/input"), provider);
        click(getElementByXpath("html/body/span/span/span[2]//li[contains(.,'" + provider + "')]"));*/

        clearAndType(getElementByID("cardtransfersearchmodel-description"), description);

        //clearAndType(getElementByID("cardtransfersearchmodel-infoname"), infoName);
    }

    //@Step("Open filters")
    public void openFilters() {
        click(getElementByXpath("//span[@class='show-search-filter']"));
    }

    //@Step("Click search button")
    public void clickSearchButton() {
        click(getElementByXpath("//button[@title='Поиск']"));
    }

    //@Step("Click 'Create card transfer link by deal' button")
    public void clickCreateCardTransferLinkByDealButton() {
        click(getElementByID("create-card-transfer-link-by-deal"));
    }

    //@Step("Enter data into 'Create card transfer link by deal' form")
    public void enterDataIntoCreateCardTransferLinkByDeal(String office, String incomeExpenses, String operationType, String operationCode, String dealNumber, String dealCost,
                                                          String dealPaid, String client, String provider, String statusBillID,
                                                          String description, String status, String filter) throws InterruptedException {

        if(filter.equals("Admin") || filter.equals("Supervisor")) {
            click(getElementByID("cardtransfermodel-officeid"));
            click(getElementByXpath("//*[@id='cardtransfermodel-officeid']/option[contains(.,'" + office + "')]"));
        }

        click(getElementByID("cardtransfermodel-incomeexpenses"));
        click(getElementByXpath("//*[@id='cardtransfermodel-incomeexpenses']/option[contains(.,'" + incomeExpenses + "')]"));

        click(getElementByID("cardtransfermodel-operationtype"));
        click(getElementByXpath("//*[@id='cardtransfermodel-operationtype']/option[contains(.,'" + operationType + "')]"));

        click(getElementByID("cardtransfermodel-operationcode"));
        click(getElementByXpath("//*[@id='cardtransfermodel-operationcode']/option[contains(.,'" + operationCode + "')]"));

        click(getElementByXpath("//*[@id='container-cardtransfer-deal-']/div/table/thead/tr/th[2]/button"));

        click(getElementByXpath("//*[@id='container-cardtransfer-deal-']//span[@dir]"));
        Thread.sleep(3000);
        clearAndType(getElementByXpath("html/body/span/span/span[1]/input"), dealNumber);
        Thread.sleep(5000);
        click(getElementByXpath("html/body/span/span/span[2]//li[contains(.,'" + dealNumber + "')]"));

        clearAndType(getElementByXpath("//*[@id='container-cardtransfer-deal-']/div//div[3]//input"), dealCost);

        clearAndType(getElementByXpath("//*[@id='container-cardtransfer-deal-']/div//div[4]//input"), dealPaid);

        click(getElementByXpath("//*[@id='cardtransfer-form-']/div[3]/div[3]/div/div[1]/div/span/span[1]/span"));
        Thread.sleep(3000);
        clearAndType(getElementByXpath("html/body/span/span/span[1]/input"), client);
        Thread.sleep(5000);
        click(getElementByXpath("html/body/span/span/span[2]//li[contains(.,'" + client + "')]"));

        /*click(getElementByXpath("//*[@id='cardtransfer-form-']/div[3]/div[3]/div/div[2]/div[1]/span/span[1]/span"));
        Thread.sleep(3000);
        clearAndType(getElementByXpath("html/body/span/span/span[1]/input"), provider);
        Thread.sleep(5000);
        click(getElementByXpath("html/body/span/span/span[2]//li[contains(.,'" + provider + "')]"));*/

        click(getElementByID("cardtransfermodel-statusbillid"));
        click(getElementByXpath("//*[@id='cardtransfermodel-statusbillid']/option[contains(.,'" + statusBillID + "')]"));

        clearAndType(getElementByID("cardtransfermodel-description"), description);

        //uploadFile("//*[@id='cardtransfermodel-attachment-']", "doc");

        scrollToBot("500");
    }

    //@Step("Click expand payment link")
    public void clickExpandPaymentLink() {
        click(getElementByID("link-charges-"));
    }

    //@Step("Enter data into payment block")
    public void enterDataIntoPaymentBlock(String card, String chargeDate, String chargeSum, String commission) throws InterruptedException {
        //uploadFile("//*[@id='cardtransfermodel-attachmentcharge-']", "doc");

        Thread.sleep(2000);
        click(getElementByID("cardtransferdealmodel-0-fromcard"));
        Thread.sleep(2000);
        click(getElementByXpath("//*[@id='cardtransferdealmodel-0-fromcard']/option[contains(.,'" + card + "')]"));

        clearAndType(getElementByID("cardtransfermodel-0-chargedate"), chargeDate);

        clearAndType(getElementByID("cardtransferdealmodel-0-chargesum"), chargeSum);

        clearAndType(getElementByID("cardtransferdealmodel-0-chargecommission"), commission);
    }

    //@Step("Click save button")
    public void clickSaveButton() {
        click(getElementByXpath("//*[@id='cardtransfer-form-']/div[4]/div/button[contains(.,'Сохранить')]"));
    }

    //@Step("Delete first item button")
    public void clickDeleteButton() throws InterruptedException {
        Thread.sleep(1000);
        moveToElement(getElementByXpath("//tr[1]//a[@title='удалить']"));
        click(getElementByXpath("//tr[1]//a[@title='удалить']"));
        acceptAlert();
        Thread.sleep(2000);
    }
}
