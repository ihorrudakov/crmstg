package steps;
import org.openqa.selenium.WebDriver;

public class BankTransferPageSteps extends BaseSteps {
    WebDriver driver;
    public BankTransferPageSteps(WebDriver driver) {
        super(driver);
        this.driver = driver;
    }

    public String getSum() {
        int quantity = Integer.valueOf(getElementByXpath("//input[contains(@name, 'DealServiceModel[0][0][quantity]')]").getAttribute("value"));
        int price = Integer.valueOf(getElementByXpath("//input[contains(@name, 'DealServiceModel[0][0][price]')]").getAttribute("value"));
        return String.valueOf(quantity * price);
    }

    //@Step("Enter data into search fields")
    public void enterDataIntoSearchFields(String type, String office, String incomeExpence, String operationType,
                                          String operationCode, String dateFrom, String dateTo, String billNumber, String summary, String billStatus, String isWorkDone,
                                          String isSent, String isScan, String isOriginalInOffice, String isOriginInFinanceDepartment, String dealSerialNumber, String payerID, String provider,
                                          String description, String organizationID, String status, String actList, String invoiceList, String actDateCloseFrom,
                                          String actDateCloseTo, String actNumber, String actInvoiceNumber, String legalName, String legalINN, String legalKPP,
                                          String number, String chargeDatePayment, String chargeDate, String paySum) throws InterruptedException {
        Thread.sleep(3000);
        moveToElement(getElementByID("banktransfersearchmodel-typeid"));
        click(getElementByID("banktransfersearchmodel-typeid"));
        click(getElementByXpath("//*[@id='banktransfersearchmodel-typeid']/option[contains(.,'" + type + "')]"));

        click(getElementByID("banktransfersearchmodel-officeid"));
        click(getElementByXpath("//*[@id='banktransfersearchmodel-officeid']/option[contains(.,'" + office + "')]"));

        click(getElementByID("banktransfersearchmodel-incomeexpensesid"));
        click(getElementByXpath("//*[@id='banktransfersearchmodel-incomeexpensesid']/option[contains(.,'" + incomeExpence + "')]"));

        click(getElementByID("banktransfersearchmodel-operationtypeid"));
        click(getElementByXpath(".//*[@id='banktransfersearchmodel-operationtypeid']/option[contains(.,'" + operationType + "')]"));

        click(getElementByID("banktransfersearchmodel-operationcodeid"));
        click(getElementByXpath("//*[@id='banktransfersearchmodel-operationcodeid']/option[contains(.,'" + operationCode + "')]"));

        clearAndType(getElementByID("banktransfersearchmodel-billnumber"), billNumber);

        clearAndType(getElementByID("banktransfersearchmodel-summary"), summary);

        click(getElementByID("banktransfersearchmodel-statusbillid"));
        click(getElementByXpath("//*[@id='banktransfersearchmodel-statusbillid']/option[contains(.,'" + billStatus + "')]"));

        Thread.sleep(2000);
        /*click(getElementByID("banktransfersearchmodel-isworkdone"));
        click(getElementByXpath("//*[@id='banktransfersearchmodel-isworkdone']/option[contains(.,'" + isWorkDone + "')]"));

        click(getElementByID("banktransfersearchmodel-issent"));
        click(getElementByXpath("//*[@id='banktransfersearchmodel-issent']/option[contains(.,'" + isSent + "')]"));

        click(getElementByID("banktransfersearchmodel-isoriginalinoffice"));
        click(getElementByXpath("//*[@id='banktransfersearchmodel-isoriginalinoffice']/option[contains(.,'" + isOriginalInOffice + "')]"));

        click(getElementByID("banktransfersearchmodel-isoriginfinancedepartment"));
        click(getElementByXpath("//*[@id='banktransfersearchmodel-isoriginfinancedepartment']/option[contains(.,'" + isOriginInFinanceDepartment + "')]"));*/
        click(getElementByID("banktransfersearchmodel-dealserialnumber"));
        clearAndType(getElementByID("banktransfersearchmodel-dealserialnumber"), dealSerialNumber);

        /*click(getElementByID("banktransfersearchmodel-payerid"));
        click(getElementByXpath("//*[@id='banktransfersearchmodel-payerid']/option[contains(.,'" + payerID + "')]"));

        click(getElementByXpath("//*[@id='custom-filter-form']/div[4]/div[2]/div/span/span[1]/span"));
        clearAndType(getElementByXpath("html/body/span/span/span[1]/input"), provider);
        click(getElementByXpath("html/body/span/span/span[2]//li[contains(.,'" + provider + "')]"));*/

        clearAndType(getElementByID("banktransfersearchmodel-description"), description);

        click(getElementByID("banktransfersearchmodel-organizationid"));
        click(getElementByXpath("//*[@id='banktransfersearchmodel-organizationid']/option[contains(.,'" + organizationID + "')]"));

        click(getElementByID("banktransfersearchmodel-status"));
        click(getElementByXpath("//*[@id='banktransfersearchmodel-status']/option[contains(.,'" + status + "')]"));

        clearAndType(getElementByID("banktransfersearchmodel-actdateclosefrom"), actDateCloseFrom);
        clearAndType(getElementByID("banktransfersearchmodel-actdatecloseto"), actDateCloseTo);
        clearAndType(getElementByID("banktransfersearchmodel-actlist"), actList);
        clearAndType(getElementByID("banktransfersearchmodel-invoicelist"), invoiceList);

        clearAndType(getElementByID("banktransfersearchmodel-legalname"), legalName);
        clearAndType(getElementByID("banktransfersearchmodel-legalinn"), legalINN);
        clearAndType(getElementByID("banktransfersearchmodel-legalkpp"), legalKPP);

        clearAndType(getElementByID("banktransfersearchmodel-chargenumber"), number);
        clearAndType(getElementByID("banktransfersearchmodel-chargedatepaymentfrom"), chargeDatePayment);
        clearAndType(getElementByID("banktransfersearchmodel-chargedatepaymentto"), chargeDatePayment);
        clearAndType(getElementByID("banktransfersearchmodel-chargedate"), chargeDate);
        clearAndType(getElementByID("banktransfersearchmodel-chargesum"), paySum);

        clearAndType(getElementByID("banktransfersearchmodel-billdatefrom"), dateFrom);

        clearAndType(getElementByID("banktransfersearchmodel-billdateto"), dateTo);
    }

    //@Step("Click search button")
    public void clickSearchButton() throws InterruptedException {
        click(getElementByXpath("//button[@class='btn btn-primary btn-circle pull-right']"));
        Thread.sleep(2000);
    }

    //@Step("Click 'Create bank transfer link by deal' button")
    public void clickCreateBankTransferByDealButton() throws InterruptedException {
        click(getElementByID("create-bank-transfer-link-by-deal"));
        Thread.sleep(2000);
    }

    //@Step("Click 'Expand filters' button")
    public void clickOpenFiltersButton() {
        click(getElementByXpath("//span[@class='show-search-filter']"));
    }

    //@Step("Enter data into 'Create bank transfer link by deal' form")
    public void enterDataIntoCreateBankTransferLinkByDeal(String office, String incomeExpenses, String operationType, String operationCode, String dealNumber, String dealCost,
                                                          String dealPaid, String service, String content, String quantity, String price, String vatPercent, String vat, String summ,
                                                          String payerID, String providerID, String organizationID, String billNumber, String billDate, String statusBillID,
                                                          String description, String status, String filter) throws InterruptedException {

        click(getElementByID("banktransfermodel-statusbillid"));
        Thread.sleep(1000);
        click(getElementByXpath("//*[@id='banktransfermodel-statusbillid']/option[contains(.,'" + statusBillID + "')]"));

        //clearAndType(getElementByID("banktransfermodel-billnumber"), billNumber);

        //clearAndType(getElementByID("banktransfermodel-billdate"), billDate);

        click(getElementByID("banktransfermodel-incomeexpenses"));
        click(getElementByXpath("//*[@id='banktransfermodel-incomeexpenses']/option[contains(.,'" + incomeExpenses + "')]"));

        /*click(getElementByID("banktransfermodel-operationtype"));
        click(getElementByXpath("//*[@id='banktransfermodel-operationtype']/option[contains(.,'" + operationType + "')]"));*/

        click(getElementByID("banktransfermodel-operationcode"));
        click(getElementByXpath("//*[@id='banktransfermodel-operationcode']/option[contains(.,'" + operationCode + "')]"));

        if (filter.equals("Admin") || filter.equals("Accountant office") || filter.equals("Supervisor") || filter.equals("Senior")) {
            Thread.sleep(1000);
            click(getElementByID("banktransfermodel-organizationid"));
            Thread.sleep(1000);
            click(getElementByXpath("//*[@id='banktransfermodel-organizationid']/option[contains(.,'" + organizationID + "')]"));
        }

        if (filter.equals("Admin") || filter.equals("Supervisor")) {
            click(getElementByID("banktransfermodel-officeid"));
            click(getElementByXpath("//*[@id='banktransfermodel-officeid']/option[contains(.,'" + office + "')]"));
        }

        click(getElementByID("banktransfermodel-isignoredby1c"));
        click(getElementByXpath("//*[@id='banktransfermodel-isignoredby1c']/option[contains(.,'Не выгружать в 1С')]"));

        if (filter.equals("Admin") || filter.equals("Supervisor")) {
            //click(getElementByXpath("//*[@id='container-banktransfer-deal-']/div/table/thead/tr/th[2]/button"));
        }

        click(getElementByXpath("//*[@id='container-banktransfer-deal-']/div/div/table/tbody/tr/td[1]/div[2]/span/span[1]/span"));
        Thread.sleep(1000);
        clearAndType(getElementByXpath("html/body/span/span/span[1]/input"), dealNumber);
        Thread.sleep(1000);
        click(getElementByXpath("html/body/span/span/span[2]//li[contains(.,'" + dealNumber + "')]"));
        Thread.sleep(1000);
        clearAndType(getElementByID("banktransferdealmodel-0-dealcost"), summ);
        Thread.sleep(1000);
        clearAndType(getElementByID("banktransferdealmodel-0-dealpaid"), dealPaid);

        //click(getElementByXpath("//button[contains(@class, 'add-banktransfer-deal-service')]"));

        //if (filter.equals("Admin") || filter.equals("Supervisor")) {
            click(getElementByXpath("//div[contains(@class, 'deal-service-item') and not(contains(@class,'hidden'))]//span[contains(@aria-labelledby, 'serviceid')]"));
            //click(getElementByXpath("//div[contains(@class, 'deal-service-item') and not(contains(@class,'hidden'))][2]//span[contains(@aria-labelledby, 'serviceid')]"));
            clearAndType(getElementByXpath("html/body/span/span/span[1]/input"), service);
            click(getElementByXpath("html/body/span/span/span[2]//li[contains(.,'" + service + "')]"));

            clearAndType(getElementByXpath("//input[contains(@name, 'DealServiceModel[0][0][content]')]"), content);
            clearAndType(getElementByXpath("//input[contains(@name, 'DealServiceModel[0][0][quantity]')]"), quantity);
            clearAndType(getElementByXpath("//input[contains(@name, 'DealServiceModel[0][0][price]')]"), price);
            //clearAndType(getElementByXpath("//input[contains(@name, 'DealServiceModel[0][0][vat_percent]')]"), vatPercent);
            //clearAndType(getElementByXpath("//input[contains(@name, 'DealServiceModel[0][0][vat]')]"), vat);
            //clearAndType(getElementByXpath("//input[contains(@name, 'DealServiceModel[0][0][summary]')]"), summ);
        /*} else {
            click(getElementByXpath("//div[contains(@class, 'deal-service-item') and not(contains(@class,'hidden'))]//span[contains(@aria-labelledby, 'serviceid')]"));
            clearAndType(getElementByXpath("html/body/span/span/span[1]/input"), service);
            click(getElementByXpath("html/body/span/span/span[2]//li[contains(.,'" + service + "')]"));

            clearAndType(getElementByXpath("//input[contains(@name, 'DealServiceModel[0][0][content]')]"), content);
            clearAndType(getElementByXpath("//input[contains(@name, 'DealServiceModel[0][0][quantity]')]"), quantity);
            clearAndType(getElementByXpath("//input[contains(@name, 'DealServiceModel[0][0][price]')]"), price);
            //clearAndType(getElementByXpath("//input[contains(@name, 'DealServiceModel[0][0][vat_percent]')]"), vatPercent);
            //clearAndType(getElementByXpath("//input[contains(@name, 'DealServiceModel[0][0][vat]')]"), vat);
            //clearAndType(getElementByXpath("//input[contains(@name, 'DealServiceModel[0][0][summary]')]"), summ);
        }*/
        if (incomeExpenses.equals("Счет клиенту") || incomeExpenses.equals("Возврат клиенту")) {
            click(getElementByID("banktransfermodel-payerid"));
            click(getElementByXpath("//*[@id='banktransfermodel-payerid']/option[contains(.,'" + payerID + "')]"));
        }

        if (!incomeExpenses.equals("Счет клиенту")) {
            click(getElementByXpath("//span[contains(@aria-labelledby, 'banktransfermodel-providerlegalid')]"));
            clearAndType(getElementByXpath("html/body/span/span/span[1]/input"), providerID);
            click(getElementByXpath("html/body/span/span/span[2]//li[contains(.,'" + providerID + "')]"));
        }
        click(getElementByID("banktransfermodel-createnewcontract"));
        clearAndType(getElementByID("banktransfermodel-description"), description);

        scrollToBot("250");

        //uploadFile("//*[@id='banktransfermodel-attachment-']", "doc");
    }

    //@Step("Click expand payment link")
    public void clickExpandPaymentLink() {
        click(getElementByID("link-charges-"));
    }

    //@Step("Enter data into payment block")
    public void enterDataIntoPaymentBlock(String number, String paymentDate, String chargeDate, String chargeSum, String commission) {
        //uploadFile("//*[@id='banktransfermodel-attachmentcharge-']", "doc");

        clearAndType(getElementByID("banktransferchargemodel-0-chargenumber"), number);

        clearAndType(getElementByID("banktransfermodel-0-chargedatepayment"), paymentDate);

        clearAndType(getElementByID("banktransfermodel_-0-chargedate"), chargeDate);

        clearAndType(getElementByID("banktransferchargemodel-0-chargesum"), chargeSum);

        clearAndType(getElementByID("banktransferdealmodel_-0-chargecommission"), commission);

        scrollToBot("300");
    }

    //@Step("Click save button")
    public void clickSaveButton() {
        click(getElementByXpath("//*[@id='banktransfer-form-']/div/div/button[contains(.,'Сохранить')]"));
    }

    //@Step("Delete first item button")
    public void clickDeleteButton() throws InterruptedException {
        Thread.sleep(3000);
        moveToElement(getElementByXpath("//tr[1]//a[@title='удалить']"));
        click(getElementByXpath("//tr[1]//a[@title='удалить']"));
        acceptAlert();
    }

    //@Step("Click expand first item button")
    public void clickEditButton() {
        scrollToBot("700");
        moveToElement(getElementByXpath("//tr[1]//a[@title='Редактировать']"));
        click(getElementByXpath("//tr[1]//a[@title='Редактировать']"));
    }

    //@Step("Click save update button")
    public void clickSaveUpdateButton() throws InterruptedException {
        Thread.sleep(2000);
        moveToElement(getElementByXpath("//div[@id='banktransfer-grid-pjax']//button[contains(.,'Сохранить')]"));
        click(getElementByXpath("//div[@id='banktransfer-grid-pjax']//button[contains(.,'Сохранить')]"));
    }

    public void enterGeneralSearchQuery(String query) {
        clearAndType(getElementByXpath("//input[@id='all-search-input-visual']"), query);
    }

    public void clickGeneralSearchButton() {
        click(getElementByXpath("//a[@onclick='setAllSearchInputAndDoSearch();']"));
    }

    //@Step("Click 'Open filters' button")
    public void enterDataIntoGeneralSearchField(String query) {
        clearAndType(getElementByID("all-search-input-visual"), query);
    }

    public void changeBillNumber(String billNumber) throws InterruptedException {
        Thread.sleep(3000);
        clearAndType(getElementByXpath("//div[@id='banktransfer-grid-pjax']//input[contains(@name, 'BankTransferModel[billNumber]')]"), billNumber);
    }

    public void changePayerToProvider(String incomeExpenses, String provider) throws InterruptedException {
        Thread.sleep(1000);
        click(getElementByXpath("//div[@id='banktransfer-grid-pjax']//select[contains(@id, 'banktransfermodel-incomeexpenses')]"));
        click(getElementByXpath("//div[@id='banktransfer-grid-pjax']//select[contains(@id, 'banktransfermodel-incomeexpenses')]/option[contains(.,'" + incomeExpenses + "')]"));
        Thread.sleep(1000);
        click(getElementByXpath("//div[@id='banktransfer-grid-pjax']//span[contains(@aria-labelledby, 'banktransfermodel-providerlegalid')]"));
        clearAndType(getElementByXpath("html/body/span/span/span[1]/input"), provider);
        click(getElementByXpath("html/body/span/span/span[2]//li[contains(.,'" + provider + "')]"));
    }
}
