package steps;

import org.openqa.selenium.WebDriver;

public class PayerPageSteps extends BaseSteps {
    WebDriver driver;
    public PayerPageSteps(WebDriver driver) {
        super(driver);
        this.driver = driver;
    }

    //@Step("Enter search data")
    public void enterSearchData(String name, String client, String inn, String status) throws InterruptedException {
        clearAndType(getElementByID("payersearchmodel-name"), name);

        click(getElementByXpath("//span[contains(@aria-labelledby, 'payersearchmodel-clientid')]"));
        Thread.sleep(1000);
        clearAndType(getElementByXpath("html/body/span/span/span[1]/input"), client);
        Thread.sleep(2000);
        click(getElementByXpath("html/body/span/span/span[2]//li"));

        clearAndType(getElementByID("payersearchmodel-inn"), inn);

        click(getElementByID("payersearchmodel-status"));
        if (status.equals("Удалено")) {
            click(getElementByXpath("//*[@id='payersearchmodel-status']/option[@value='0']"));
        } else {
            click(getElementByXpath("//*[@id='payersearchmodel-status']/option[@value='1']"));
        }

    }

    //@Step("Click 'Search' button")
    public void clickSearchButton() throws InterruptedException {
        click(getElementByXpath("//button[contains(.,'Поиск')]"));
        Thread.sleep(3000);
    }

    //@Step("Click 'Save' button")
    public void clickSaveButton() {
        click(getElementByXpath("//button[contains(.,'Обновить')]"));
    }

    //@Step("Click 'Edit' button")
    public void clickEditButton() throws InterruptedException {
        moveToElement(getElementByXpath("//tr[1]//a[@title='Редактировать']"));
        click(getElementByXpath("//tr[1]//a[@title='Редактировать']"));
        Thread.sleep(2000);
    }

    //@Step("Click 'Delete' button")
    public void clickDeleteButton() throws InterruptedException {
        moveToElement(getElementByXpath("//tr[1]//a[@title='удалить']"));
        click(getElementByXpath("//tr[1]//a[@title='удалить']"));
        acceptAlert();
        Thread.sleep(1000);
    }

    //@Step("Change client status")
    public void changeClientStatus(String client, String status) throws InterruptedException {
        click(getElementByXpath("//span[contains(@aria-labelledby, 'payermodel-clientid')]"));
        clearAndType(getElementByXpath("html/body/span/span/span[1]/input"), client);
        click(getElementByXpath("html/body/span/span/span[2]//li[contains(.,'" + client + "')]"));

        click(getElementByID("payermodel-status"));
        click(getElementByXpath("//*[@id='payermodel-status']/option[contains(.,'Активнo')]"));
    }
}
