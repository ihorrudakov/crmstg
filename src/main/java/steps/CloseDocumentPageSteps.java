package steps;

import org.openqa.selenium.WebDriver;

import static org.testng.Assert.assertEquals;

public class CloseDocumentPageSteps extends BaseSteps {
    WebDriver driver;

    public CloseDocumentPageSteps(WebDriver driver) {
        super(driver);
        this.driver = driver;
    }

    //@Step("Click 'Create Close Document for Client' button")
    public void clickCreateCloseDocumentForClientButton() {
        click(getElementByXpath("//div[@id='create-button-container']/a[1]"));
    }

    //@Step("Enter data")
    public void enterData(String dealNumber, String actNumber, String actDateTime, String content, String quantity,
                          String price, String nds, String sumNDS, String actSum, String actInvoiceNumber,
                          String dateInvoiceNumber, String description, String createdBy, String type) throws InterruptedException {
        Thread.sleep(3000);
        click(getElementByXpath("//span[contains(@aria-labelledby, 'banktransferdealmodel-0-dealid')]"));
        clearAndType(getElementByXpath("html/body/span/span/span[1]/input"), dealNumber);
        click(getElementByXpath("html/body/span/span/span[2]//li[contains(.,'" + dealNumber + "')]"));
        clearAndType(getElementByID("bankclosingmodel-description"), description);
    }

    //@Step("Enter updated data")
    public void enterUpdatedData(String dealNumber, String actNumber, String actDateTime, String content, String quantity,
                          String price, String nds, String sumNDS, String actSum, String actInvoiceNumber,
                          String dateInvoiceNumber, String description, String createdBy, String type) throws InterruptedException {
        Thread.sleep(3000);
        click(getElementByXpath("//div[contains(@class, 'skip-export kv-expanded-row')]//span[contains(@aria-labelledby, 'banktransferdealmodel-0-dealid')]"));
        clearAndType(getElementByXpath("html/body/span/span/span[1]/input"), dealNumber);
        click(getElementByXpath("html/body/span/span/span[2]//li[contains(.,'" + dealNumber + "')]"));
        clearAndType(getElementByXpath("//div[contains(@class, 'skip-export kv-expanded-row')]//input[@name='BankClosingModel[description]']"), description);

        //uploadFile("//*[@id='bankclosingmodel-attachmentdoc-']", "doc");
    }

    //@Step("Click 'Save' button")
    public void clickSaveButton() throws InterruptedException {
        click(getElementByXpath("//button[contains(.,'Сохранить')]"));
        Thread.sleep(2000);
    }

    //@Step("Click 'Update' button")
    public void clickUpdateButton() throws InterruptedException {
        click(getElementByXpath("//div[@id='bankclosing-grid-pjax']//button[contains(.,'Сохранить')]"));
        Thread.sleep(2000);
    }

    //@Step("Open search filter")
    public void openSearchFilter() throws InterruptedException {
        Thread.sleep(5000);
        click(getElementByXpath("//span[@class='show-search-filter']"));
        Thread.sleep(3000);
    }

    //@Step("Enter search data")
    public void enterSearchData(String type, String actNumber, String actDate, String actInvoiceNumber, String organization,
                                String billNumber, String legalClient, String legalProvider, String contract) throws InterruptedException {
        //click(getElementByID("bankclosingsearchmodel-typeid"));
        //click(getElementByXpath("//select[@id='bankclosingsearchmodel-typeid']/option[contains(.,'" + type + "')]"));
        Thread.sleep(3000);
        clearAndType(getElementByID("bankclosingsearchmodel-actnumber"), actNumber);
        Thread.sleep(3000);
        clearAndType(getElementByID("bankclosingsearchmodel-actdatetimefrom"), actDate);
        Thread.sleep(3000);
        clearAndType(getElementByID("bankclosingsearchmodel-actdatetimeto"), actDate);
        Thread.sleep(3000);
        clearAndType(getElementByID("bankclosingsearchmodel-actinvoicenumber"), actInvoiceNumber);
        Thread.sleep(3000);

        click(getElementByID("bankclosingsearchmodel-legalorganizationid"));
        click(getElementByXpath("//select[@id='bankclosingsearchmodel-legalorganizationid']/option[contains(.,'" + organization + "')]"));
        Thread.sleep(3000);
        clearAndType(getElementByID("bankclosingsearchmodel-billnumber"), billNumber);
        Thread.sleep(3000);
        clearAndType(getElementByID("bankclosingsearchmodel-legalclient"), legalClient);
        /*click(getElementByXpath("//span[contains(@aria-labelledby, 'bankclosingsearchmodel-legalprovider')]"));
        click(getElementByXpath("html/body/span/span/span[2]//li[contains(.,'" + legalProvider + "')]"));*/
        Thread.sleep(3000);
        clearAndType(getElementByID("bankclosingsearchmodel-contract"), contract);
    }

    //@Step("Click search button")
    public void clickSearchButton() throws InterruptedException {
        click(getElementByID("pagination-refresh"));
        Thread.sleep(3000);
    }

    //@Step("Click 'Edit' button")
    public void clickEditButton() {
        moveToElement(getElementByXpath("//tr[1]//a[@title='Редактировать']"));
        click(getElementByXpath("//tr[1]//a[@title='Редактировать']"));
    }

    //@Step("Click 'Delete' button")
    public void clickDeleteButton() throws InterruptedException {
        moveToElement(getElementByXpath("//tr[1]//a[@title='удалить']"));
        click(getElementByXpath("//tr[1]//a[@title='удалить']"));
        Thread.sleep(2000);
        acceptAlert();
    }

    public void checkChargeData(String number, String date, String summ) {
        assertEquals(number, getElementByXpath("//div[@id='bankclosing-grid-pjax']//div[@class='general-information']//div[contains(.,'Платежи')]//td[1]").getText());
        assertEquals(date, getElementByXpath("//div[@id='bankclosing-grid-pjax']//div[@class='general-information']//div[contains(.,'Платежи')]//td[2]").getText());
        assertEquals(summ, getElementByXpath("//div[@id='bankclosing-grid-pjax']//div[@class='general-information']//div[contains(.,'Платежи')]//td[3]").getText());
    }
}
