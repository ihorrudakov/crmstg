package steps;

import org.openqa.selenium.WebDriver;
import static org.testng.Assert.assertEquals;

public class DealPageSteps extends BaseSteps {
    WebDriver driver;

    double incomeV;

    public DealPageSteps(WebDriver driver) {
        super(driver);
        this.driver = driver;
    }

    public void enterValuesIntoSearchField(String number) {
        clearAndType(getElementByID("all-search-input-visual"), number);
    }

    public void clickSaveButton() throws InterruptedException {
        Thread.sleep(2000);
        click(getElementByXpath("//*[@id='deal-form-']//button[contains(.,'Сохранить')]"));
        Thread.sleep(7000);
    }

    public void enterValues(String number, String provider, String payer, String startDate, String endDate, String ratio, String comment, String status, String document,
                            String docQuantity, String docMeaning, String docNumber, String docEndDate, String okpd2, String tnved, String assignedCashSum, String assignedNoCashSum,
                            String assignedNoCashCurrency, String paidCashSum, String paidNoCashSum, String paidNoCashAccountNumber, String orderNumber, String courier,
                            String agentCashSum, String agentNoCashSum, String agentComment, String costCashSum, String costNoCashSum, String costNoCashSumCurrency, String incomeInvoiceNumber,
                            String officeRatio, String transfer, String taxpercent, String role) throws InterruptedException {

        //--------------
        click(getElementByXpath("//li//a[contains(.,'Дополнительно')]"));

        click(getElementByXpath("//*[@id='deal-form-']//span[contains(@aria-labelledby, 'dealmodel-couriername')]"));
        Thread.sleep(1000);
        clearAndType(getElementByXpath("html/body/span/span/span[1]/input"), courier);
        Thread.sleep(1000);
        click(getElementByXpath("html/body/span/span/span[2]//li[contains(.,'" + courier + "')]"));

        clearAndType(getElementByID("dealmodel-ordernumber"), orderNumber);

        clearAndType(getElementByID("dealmodel-agentcashsum"), agentCashSum);

        clearAndType(getElementByID("dealmodel-agentnocashsum"), agentNoCashSum);

        clearAndType(getElementByID("dealmodel-agentcomment"), agentComment);

        clearAndType(getElementByID("dealmodel-serialnumber"), number);

        clearAndType(getElementByID("dealmodel-ratio"), ratio);

        clearAndType(getElementByID("dealmodel-enddate"), endDate);

        if (role.equals("Admin")) {
            clearAndType(getElementByID("dealmodel-officeratio"), officeRatio);

            clearAndType(getElementByID("dealmodel-transfer"), transfer);

            clearAndType(getElementByID("dealmodel-taxpercent"), taxpercent);
        }

        Thread.sleep(5000);

        //-------------
        click(getElementByXpath("//li//a[contains(.,'Основное')]"));

        click(getElementByID("dealmodel-status"));
        click(getElementByXpath("//*[@id='dealmodel-status']/option[contains(.,'" + status + "')]"));

        //uploadFile("//*[@id='dealmodel-attachment-']", "doc");

        Thread.sleep(1000);

        click(getElementByXpath("//*[@id='deal-form-']//span[contains(@aria-labelledby, 'dealmodel-serviceid')]"));
        Thread.sleep(1000);
        clearAndType(getElementByXpath("html/body/span/span/span[1]/input"), document);
        Thread.sleep(2000);
        click(getElementByXpath("html/body/span/span/span[2]//li[contains(.,'" + document + "')]"));

        clearAndType(getElementByID("dealmodel-docquantity"), docQuantity);

        clearAndType(getElementByID("dealmodel-docenddate"), docEndDate);

        clearAndType(getElementByID("dealmodel-docmeaning"), docMeaning);

        clearAndType(getElementByID("dealmodel-docnumber"), docNumber);

        Thread.sleep(2000);

        click(getElementByXpath("//*[@id='deal-form-']//span[contains(@aria-labelledby, 'dealmodel-okpdNewlist')]"));
        Thread.sleep(1000);
        clearAndType(getElementByXpath("html/body/span/span/span[1]/input"), okpd2);
        Thread.sleep(1000);
        click(getElementByXpath("html/body/span/span/span[2]//li[contains(.,'" + okpd2 + "')]"));

        Thread.sleep(2000);

        click(getElementByXpath("//*[@id='deal-form-']//span[contains(@aria-labelledby, 'dealmodel-tnvedNewlist')]"));
        Thread.sleep(1000);
        clearAndType(getElementByXpath("html/body/span/span/span[1]/input"), tnved);
        Thread.sleep(1000);
        click(getElementByXpath("html/body/span/span/span[2]//li"));
        Thread.sleep(2000);

        click(getElementByID("dealmodel-payerid"));
        click(getElementByXpath("//*[@id='dealmodel-payerid']//option[contains(.,'" + payer + "')]"));

        clearAndType(getElementByID("dealmodel-assignedcashsum"), assignedCashSum);

        clearAndType(getElementByID("dealmodel-assignednocashsum"), assignedNoCashSum);

        click(getElementByID("dealmodel-assignednocashsumcurrency"));
        click(getElementByXpath("//*[@id='dealmodel-assignednocashsumcurrency']/option[contains(.,'" + assignedNoCashCurrency + "')]"));

        //clearAndType(getElementByID("dealmodel-paidcashsum"), paidCashSum);

        //clearAndType(getElementByID("dealmodel-paidnocashsum"), paidNoCashSum);

        clearAndType(getElementByID("dealmodel-paidnocashaccountnumber"), paidNoCashAccountNumber);

        click(getElementByXpath("//*[@id='deal-form-']//span[contains(@aria-labelledby, 'dealmodel-providerid')]"));
        Thread.sleep(1000);
        clearAndType(getElementByXpath("html/body/span/span/span[1]/input"), provider);
        Thread.sleep(1000);
        click(getElementByXpath("html/body/span/span/span[2]//li[contains(.,'" + provider + "')]"));

        clearAndType(getElementByID("dealmodel-costcashsum"), costCashSum);

        clearAndType(getElementByID("dealmodel-costnocashsum"), costNoCashSum);

        click(getElementByID("dealmodel-costnocashsumcurrency"));
        click(getElementByXpath("//*[@id='dealmodel-costnocashsumcurrency']/option[contains(.,'" + costNoCashSumCurrency + "')]"));

        clearAndType(getElementByID("dealmodel-incomeinvoicenumber"), incomeInvoiceNumber);

        scrollToBot("300");

        clearAndType(getElementByID("dealmodel-comment"), comment);

        if (role.equals("Admin")) {
            incomeV = getIncome(assignedCashSum, assignedNoCashSum, paidCashSum, paidNoCashSum,
                    agentCashSum, agentNoCashSum, costCashSum, costNoCashSum,
                    officeRatio, transfer, taxpercent, ratio);
        } else {
            incomeV = getIncome(assignedCashSum, assignedNoCashSum, paidCashSum, paidNoCashSum,
                    agentCashSum, agentNoCashSum, costCashSum, costNoCashSum,
                    "0.9", "0", "0.03", ratio);
        }

    }

    public void clickGeneralSearchButton() throws InterruptedException {
        Thread.sleep(1000);
        click(getElementByID("js-presss-search-enter"));
        Thread.sleep(2000);
    }

    public void clickExpandButton() throws InterruptedException {
        Thread.sleep(1000);
        moveToElement(getElementByXpath("//td[@data-mark]//a[@title='Редактировать']"));
        click(getElementByXpath("//td[@data-mark]//a[@title='Редактировать']"));
    }

    public void clickDeleteButton() throws InterruptedException {
        Thread.sleep(1000);
        moveToElement(getElementByXpath("//*[@id='w4-container']/table/tbody/tr[1]/td/a[@title = 'Удалить']"));
        click(getElementByXpath("//*[@id='w4-container']/table/tbody/tr[1]/td/a[@title = 'Удалить']"));
        acceptAlert();
    }

    public void openFinancialTab() throws InterruptedException {
        click(getElementByXpath("//li//a[contains(.,'Финансы')]"));
        Thread.sleep(2000);
    }

    public void checkFinTab(String bankType, String bankIncomeType, String bankBillNumber, String bankStatus, String bankDealPaid,
                            String payNumber, String paymentDate, String chargeDate, String chargeSum, String commission,
                            String cardType, String cardIncomeType, String cardStatus, String fromCard, String cardSum, String cardTax,
                            String cashIncomeType, String cashCode, String cashNote, String cashSum) {
        assertEquals(bankType, getElementByXpath("//table[@class='table table-bordered table-striped'][1]//tr[2]/td[1]").getText());
        //Assert.assertEquals(bankDate, getElementByXpath("//table[@class='table table-bordered table-striped'][1]//tr[2]/td[2]").getText());
        //Assert.assertEquals(bankBillNumber, getElementByXpath("//table[@class='table table-bordered table-striped'][1]//tr[2]/td[3]").getText());
        assertEquals(bankStatus, getElementByXpath("//table[@class='table table-bordered table-striped'][1]//tr[2]/td[5]").getText());
        assertEquals(bankDealPaid, getElementByXpath("//table[@class='table table-bordered table-striped'][1]//tr[2]/td[6]").getText());
        //Assert.assertEquals(chargeDate, getElementByXpath("//table[@class='table table-bordered table-striped'][1]//tr[2]/td[7]").getText());
        assertEquals(commission, getElementByXpath("//table[@class='table table-bordered table-striped'][1]//tr[2]/td[7]//tr[2]/td[2]").getText());
        assertEquals(chargeSum, getElementByXpath("//table[@class='table table-bordered table-striped'][1]//tr[2]/td[7]//tr[2]/td[3]").getText());
        //Assert.assertEquals(paymentDate, getElementByXpath("//table[@class='table table-bordered table-striped'][1]//tr[2]/td[7]//tr[2]/td[4]").getText());
        assertEquals(payNumber, getElementByXpath("//table[@class='table table-bordered table-striped'][1]//tr[2]/td[7]//tr[2]/td[5]").getText());

        assertEquals(cardType, getElementByXpath("//table[@class='table table-bordered table-striped tabs-table'][1]/tbody/tr[2]//td[1]").getText());
        assertEquals(cardIncomeType, getElementByXpath("//table[@class='table table-bordered table-striped tabs-table'][1]/tbody/tr[2]//td[2]").getText());
        assertEquals(cardStatus, getElementByXpath("//table[@class='table table-bordered table-striped tabs-table'][1]/tbody/tr[2]//td[3]").getText());
        //Assert.assertEquals(fromCard, getElementByXpath("//table[@class='table table-bordered table-striped tabs-table'][1]/tbody/tr[2]//td[5]//tr[2]//td[1]").getText());
        assertEquals(cardSum, getElementByXpath("//table[@class='table table-bordered table-striped tabs-table'][1]/tbody/tr[2]//td[5]//tr[2]//td[3]").getText());
        assertEquals(cardTax, getElementByXpath("//table[@class='table table-bordered table-striped tabs-table'][1]/tbody/tr[2]//td[5]//tr[2]//td[4]").getText());

        assertEquals(cashIncomeType, getElementByXpath("//table[@class='table table-bordered table-striped'][2]//tr[2]/td[2]").getText());
        assertEquals(cashCode, getElementByXpath("//table[@class='table table-bordered table-striped'][2]//tr[2]/td[3]").getText());
        assertEquals(cashNote, getElementByXpath("//table[@class='table table-bordered table-striped'][2]//tr[2]/td[4]").getText());
        assertEquals(true, getElementByXpath("//table[@class='table table-bordered table-striped'][2]//tr[2]/td[6][contains(.,'" + cashSum + "')]").isDisplayed());
    }

    public void checkPaidInfo(String type, String payerNumber, String payerPaid, String providerNumber, String paidSumCash) {
        if (type.equals("Payer")) {
            assertEquals(payerNumber, getElementByXpath("//div[@class='row border-b-block'][1]//div[@class='row border-b-block'][2]//div[contains(.,'№ счета')]//input[@disabled]").getAttribute("value"));
            assertEquals(payerPaid, getElementByXpath("//div[@class='row border-b-block'][1]//div[@class='row border-b-block'][2]//div[contains(.,'Оплачено')]//input[@disabled]").getAttribute("value").substring(0,3));
        } else {
            assertEquals(providerNumber, getElementByXpath("//div[@class='row border-b-block'][1]//div[contains(.,'№ счета')][2]//input[@disabled]").getAttribute("value"));
            assertEquals(paidSumCash, getElementByXpath("//div[@class='col-md-4'][contains(.,'Оплачено')][1]//input[@disabled]").getAttribute("value").substring(0, 5));
        }
    }
}
