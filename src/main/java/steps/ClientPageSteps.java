package steps;

import org.openqa.selenium.WebDriver;

import static utils.Constants.*;

public class ClientPageSteps extends BaseSteps {
    WebDriver driver;
    //private static final Logger LOG = LogManager.getLogger(PageObject.class.getSimpleName());

    public ClientPageSteps(WebDriver driver) {
        super(driver);
        this.driver = driver;
    }

    //@Step("Open filters")
    public void openFilters() {
        click(getElementByXpath("//span[@class='show-search-filter']"));
    }

    //@Step("Enter data into search fields")
    public void enterDataIntoSearchFields(String name, String contactName, String email, String phone, String manager, String status, String stage) throws InterruptedException {
        clearAndType(getElementByID("clientsearchmodel-name"), name);
        clearAndType(getElementByID("clientsearchmodel-contactname"), contactName);
        clearAndType(getElementByID("clientsearchmodel-contactemail"), email);
        clearAndType(getElementByID("clientsearchmodel-contactphone"), phone);
        click(getElementByID("clientsearchmodel-status"));
        click(getElementByXpath("//*[@id='clientsearchmodel-status']/option[contains(.,'" + status + "')]"));
        click(getElementByXpath("//span[contains(@aria-labelledby, 'clientsearchmodel-managerid')]"));
        Thread.sleep(1000);
        if (stage.equals("Create")) {
            clearAndType(getElementByXpath("html/body/span/span/span[1]/input"), manager);
            click(getElementByXpath("html/body/span/span/span[2]//li[contains(.,'" + manager + "')]"));
        } else {
            clearAndType(getElementByXpath("html/body/span/span/span[1]/input"), manager);
            click(getElementByXpath("html/body/span/span/span[2]//li[contains(.,'" + manager + "')]"));
        }

        //click(getElementByID("clientsearchmodel-withdeal"));
    }

    //@Step("Enter data into simple search field")
    public void enterDataIntoSimpleSearchField(String name) {
        clearAndType(getElementByID("all-search-input-visuals"), name);
    }

    //@Step("Enter data into simple search field")
    public void clickGeneralSearchButton() throws InterruptedException {
        click(getElementByXpath("//p[@class='search']//a"));
        Thread.sleep(2000);
    }

    //@Step("Click search button")
    public void clickSearchButton() throws InterruptedException {
        Thread.sleep(1000);
        click(getElementByXpath("//button[@title='Поиск']"));
        Thread.sleep(2000);
    }

    //@Step("Click expand client info button")
    public void clickExpandClientInfoButton() {
        moveToElement(getElementByXpath("//tr[1]//a[@title='Редактировать']"));
        click(getElementByXpath("//tr[1]//a[@title='Редактировать']"));
    }

    //@Step("Click save changes button")
    public void clickSaveChangesButton() throws InterruptedException {
        Thread.sleep(1000);
        click(getElementByXpath("//div[@id='client-grid-pjax']//button[contains(.,'Сохранить')]"));
        Thread.sleep(2000);
    }

    //@Step("Click delete button")
    public void clickDeleteButton() throws InterruptedException {
        Thread.sleep(1000);
        moveToElement(getElementByXpath("//tr[1]//a[@title='удалить']"));
        click(getElementByXpath("//tr[1]//a[@title='удалить']"));
        acceptAlert();
    }

    //@Step("Change status")
    public void changeStatus(String status) {
        click(getElementByXpath("//div[@id='client-grid-pjax']//select[@id='clientmodel-status']"));
        click(getElementByXpath("//div[@id='client-grid-pjax']//select[@id='clientmodel-status']/option[contains(.,'" + status + "')]"));
    }
}
