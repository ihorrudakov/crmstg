package steps;

import org.openqa.selenium.WebDriver;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import static org.testng.Assert.assertEquals;

public class DataBaseSteps extends BaseSteps{
    WebDriver driver;

    public DataBaseSteps(WebDriver driver) {
        super(driver);
        this.driver = driver;
    }

    Connection conn;

    Map<Integer, Integer> officeMap = new HashMap<Integer, Integer>();
    Map<Integer, Integer> employeeMap = new HashMap<Integer, Integer>();

    public int getRandomNumber(Map<Integer, Integer> map) {
        Random r = new Random();
        int randomNumber = 1;
        randomNumber = r.nextInt(map.size() - 1) + 1;
        return randomNumber;
    }

    int officeId = 0;

    public void getOffices(Connection conn) throws SQLException {
        Statement statement = conn.createStatement();
        ResultSet rs = statement.executeQuery("SELECT id, name FROM crm_stage.office where status != 0;");
        while (rs.next()) {
            officeMap.put(rs.getRow(), rs.getInt(1));
        }
        officeId = officeMap.get(getRandomNumber(officeMap));
        System.out.println("\nID выбранного офиса: " + officeId);
    }

    int employeeID = 0;

    public void getEmployee(Connection conn) throws SQLException {
        Statement statement = conn.createStatement();
        ResultSet rs = statement.executeQuery("SELECT distinct user.username, user.id FROM office " +
                "join user on (office.id = user.officeId and office.status <> 0) " +
                "join deal on user.id = deal.userId " +
                "where (user.role != 'worker' or user.role != 'marketer' or user.role != 'account_fin_dep') and office.id = " + officeId + " and deal.status = 13;");
        // " + getOffices(conn) + "
        while (rs.next()) {
            employeeMap.put(rs.getRow(), rs.getInt(2));
        }
        System.out.println(employeeMap);

        try {
            employeeID = employeeMap.get(getRandomNumber(employeeMap));
            //System.out.println(number);

        } catch (IllegalArgumentException e) {
            //System.out.println("В данном офисе отсутствуют сотрудники с закрытыми делами");
        }
    }

    double paidCashSum = 0;
    double paidNoCashSum = 0;
    double sumBillByClient = 0;
    double costCashSum = 0;
    double costNoCashSum = 0;
    double agentCashSum = 0;
    double agentNoCashSum = 0;
    double profit = 0;

    double totalPaidNoCashSum = 0;
    double totalPaidSum = 0;
    double totalCostSum = 0;
    double totalAgentSum = 0;

    public void getReportData(Connection conn) throws SQLException {
        this.conn = conn;
        getOffices(conn);
        getEmployee(conn);
        Statement statement = conn.createStatement();
        ResultSet rs = statement.executeQuery("SELECT paidCashSum, paidNoCashSum, sumBillByClient, costCashSum, costNoCashSum, agentCashSum, agentNoCashSum, profit " +
                "FROM deal where userId = " + employeeID + " and status = 13");

        while (rs.next()) {
            paidCashSum += rs.getDouble(1);
            paidNoCashSum += rs.getDouble(2);
            sumBillByClient += rs.getDouble(3);
            costCashSum += rs.getDouble(4);
            costNoCashSum += rs.getDouble(5);
            agentCashSum += rs.getDouble(6);
            agentNoCashSum += rs.getDouble(7);
            profit += rs.getDouble(8);
        }
        totalPaidNoCashSum = paidNoCashSum + sumBillByClient;
        totalPaidSum = paidCashSum + totalPaidNoCashSum;
        totalCostSum = costCashSum + costNoCashSum;
        totalAgentSum = agentCashSum + agentNoCashSum;

        System.out.println(paidCashSum + "   " + totalPaidNoCashSum + "   " + totalPaidSum + "   " + costCashSum + "   " + costNoCashSum + "   " + totalCostSum + "   " + agentCashSum + "   " + agentNoCashSum + "   " + totalAgentSum + "   " + profit);
    }

    String office;
    String username;

    public void selectOfficeAndEmployee() throws SQLException, InterruptedException {
        Statement st = conn.createStatement();
        ResultSet rs = st.executeQuery("SELECT name FROM office where id = " + officeId);
        while (rs.next()) {
            office = rs.getString(1);
        }
        rs.close();
        rs = st.executeQuery("SELECT username FROM user where id = " + employeeID);
        while (rs.next()) {
            username = rs.getString(1);
        }
        Thread.sleep(2000);
        click(getElementByID("reportsearchmodel-officeid"));
        Thread.sleep(3000);
        click(getElementByXpath("//*[@id='reportsearchmodel-officeid']//option[contains(.,'" + office + "')]"));
    }

    public void checkOfficeDealReport() throws InterruptedException {
        //I have to change type from int to double for this code using

        /*Assert.assertEquals(office, getElementByXpath("//tr[contains(.,'" + username + "')]//td[2]").getText());
        Assert.assertEquals(paidCashSum, getElementByXpath("//tr[contains(.,'" + username + "')]//td[3]").getText());
        Assert.assertEquals(paidNoCashSum, getElementByXpath("//tr[contains(.,'" + username + "')]//td[4]").getText());
        Assert.assertEquals(totalPaidSum, getElementByXpath("//tr[contains(.,'" + username + "')]//td[5]").getText());
        Assert.assertEquals(costCashSum, getElementByXpath("//tr[contains(.,'" + username + "')]//td[6]").getText());
        Assert.assertEquals(costNoCashSum, getElementByXpath("//tr[contains(.,'" + username + "')]//td[7]").getText());
        Assert.assertEquals(totalCostSum, getElementByXpath("//tr[contains(.,'" + username + "')]//td[8]").getText());
        Assert.assertEquals(agentCashSum, getElementByXpath("//tr[contains(.,'" + username + "')]//td[9]").getText());
        Assert.assertEquals(agentNoCashSum, getElementByXpath("//tr[contains(.,'" + username + "')]//td[10]").getText());
        Assert.assertEquals(totalAgentSum, getElementByXpath("//tr[contains(.,'" + username + "')]//td[11]").getText());
        Assert.assertEquals(profit, getElementByXpath("//tr[contains(.,'" + username + "')]//td[12]").getText());*/
        System.out.println(username);
        if (username != null) {
            Thread.sleep(3000);
            assertEquals(office, getElementByXpath("//tr[contains(.,'" + username + "')]//td[2]").getText());//except
            assertEquals(String.valueOf((int)paidCashSum), handleText(getElementByXpath("//tr[contains(.,'" + username + "')]//td[3]").getText()));
            assertEquals(String.valueOf((int)totalPaidNoCashSum), handleText(getElementByXpath("//tr[contains(.,'" + username + "')]//td[4]").getText()));
            assertEquals(String.valueOf((int)totalPaidSum), handleText(getElementByXpath("//tr[contains(.,'" + username + "')]//td[5]").getText()));
            assertEquals(String.valueOf((int)costCashSum), handleText(getElementByXpath("//tr[contains(.,'" + username + "')]//td[6]").getText()));
            assertEquals(String.valueOf((int)costNoCashSum), handleText(getElementByXpath("//tr[contains(.,'" + username + "')]//td[7]").getText()));
            assertEquals(String.valueOf((int)totalCostSum), handleText(getElementByXpath("//tr[contains(.,'" + username + "')]//td[8]").getText()));
            assertEquals(String.valueOf((int)agentCashSum), handleText(getElementByXpath("//tr[contains(.,'" + username + "')]//td[9]").getText()));
            assertEquals(String.valueOf((int)agentNoCashSum), handleText(getElementByXpath("//tr[contains(.,'" + username + "')]//td[10]").getText()));
            assertEquals(String.valueOf((int)totalAgentSum), handleText(getElementByXpath("//tr[contains(.,'" + username + "')]//td[11]").getText()));
            assertEquals(String.valueOf((int)profit), handleText(getElementByXpath("//tr[contains(.,'" + username + "')]//td[12]").getText()));

            System.out.println("Проверка прошла успешно\n");
        } else {
            System.out.println("Пользователи с закрытыми делами в данном офисе отсутствуют\n");
        }
        officeId = 0;
        officeMap.clear();
    }

    private static String handleText(String value) {
        return value.replace(" ", "").replace(",", ".").replaceAll("\\.\\d+", "");
    }

    int bank = 0;
    int card = 0;
    int cash = 0;

    public void getSummOfOfficeSales(Connection conn) throws SQLException {
        getOffices(conn);
        Statement st = conn.createStatement();
        ResultSet rs = st.executeQuery("SELECT chargeSum FROM bank_transfer join bank_transfer_charge on bank_transfer_charge.bankTransferId = bank_transfer.id " +
                "where bank_transfer.officeId = " + officeId + " and bank_transfer.status <> 0 and bank_transfer_charge.status <> 0 and (bank_transfer_charge.chargeDatePayment between '2018-03-01' and '2018-03-31') " +
                "and bank_transfer.operationTypeId = 1 and bank_transfer.operationCodeId = 1;");
        while (rs.next()) {
            bank += rs.getInt(1);
        }
        rs.close();

        rs = st.executeQuery("SELECT chargeSum FROM card_transfer join card_transfer_charge on card_transfer_charge.cardTransferId = card_transfer.id " +
                "where card_transfer.officeId = " + officeId + " and card_transfer.status <> 0 and card_transfer_charge.status <> 0 and (card_transfer_charge.chargeDate between '2018-03-01' and '2018-03-31') " +
                "and card_transfer.operationTypeId = 1 and card_transfer.operationCodeId = 1;");
        while (rs.next()) {
            card += rs.getInt(1);
        }
        rs.close();

        rs = st.executeQuery("SELECT operationSum FROM cash_transfer " +
                "where cash_transfer.officeId = " + officeId + " and cash_transfer.status <> 0 and (cash_transfer.operationDate between '2018-03-01' and '2018-03-31') " +
                "and cash_transfer.operationTypeId = 1 and cash_transfer.operationCodeId = 1;");
        while (rs.next()) {
            cash += rs.getInt(1);
        }
        rs.close();

        rs = st.executeQuery("SELECT name FROM office where id = " + officeId);
        while (rs.next()) {
            office = rs.getString(1);
        }

        System.out.println(bank + "   " + card + "   " + cash);
    }

    public void checkSummOfOfficeSales() {
        try {
            assertEquals(true, getElementByXpath("//tr[contains(.,'" + office + "')]//td[3][contains(.,'" + bank + "')]").isDisplayed());
            assertEquals(true, getElementByXpath("//tr[contains(.,'" + office + "')]//td[4][contains(.,'" + card + "')]").isDisplayed());
            assertEquals(true, getElementByXpath("//tr[contains(.,'" + office + "')]//td[5][contains(.,'" + cash + "')]").isDisplayed());
            System.out.println("Проверка прошла успешно\n");
        } catch (Exception e) {
            e.getMessage();
        }

    }
}
